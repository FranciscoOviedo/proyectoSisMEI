package vistas;

import java.util.Date;

public class V_Info_HerramientaCC {

	private int T_HerramientaCC_ID;
	public int getT_HerramientaCC_ID() {
		return T_HerramientaCC_ID;
	}
	public void setT_HerramientaCC_ID(int t_HerramientaCC_ID) {
		T_HerramientaCC_ID = t_HerramientaCC_ID;
	}
	public int getT_FamiliaKolping_ID() {
		return T_FamiliaKolping_ID;
	}
	public void setT_FamiliaKolping_ID(int t_FamiliaKolping_ID) {
		T_FamiliaKolping_ID = t_FamiliaKolping_ID;
	}
	public int getT_MicroProyecto_ID() {
		return T_MicroProyecto_ID;
	}
	public void setT_MicroProyecto_ID(int t_MicroProyecto_ID) {
		T_MicroProyecto_ID = t_MicroProyecto_ID;
	}
	public Date getFechaCreacion() {
		return FechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	public String getNombreFamiliaKolping() {
		return NombreFamiliaKolping;
	}
	public void setNombreFamiliaKolping(String nombreFamiliaKolping) {
		NombreFamiliaKolping = nombreFamiliaKolping;
	}
	public String getNombreMicroProyecto() {
		return NombreMicroProyecto;
	}
	public void setNombreMicroProyecto(String nombreMicroProyecto) {
		NombreMicroProyecto = nombreMicroProyecto;
	}
	private int T_FamiliaKolping_ID;
	private int T_MicroProyecto_ID;
	private Date FechaCreacion;
	private boolean Estado;
	private String NombreFamiliaKolping;
	private String NombreMicroProyecto;
	
}
