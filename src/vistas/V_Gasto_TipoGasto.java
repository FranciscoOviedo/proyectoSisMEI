package vistas;

public class V_Gasto_TipoGasto {
	private String NombreTipoGasto;
	private boolean EstadoTipoGasto;
	private int T_Gasto_ID;
	private String DescripcionGasto;
	private float Cantidad;
	private boolean EstadoGasto;
	private int T_TipoGasto_ID;
	private int T_Actividad_ID;
	
	public String getNombreTipoGasto() {
		return NombreTipoGasto;
	}
	public void setNombreTipoGasto(String nombreTipoGasto) {
		NombreTipoGasto = nombreTipoGasto;
	}
	public boolean isEstadoTipoGasto() {
		return EstadoTipoGasto;
	}
	public void setEstadoTipoGasto(boolean estadoTipoGasto) {
		EstadoTipoGasto = estadoTipoGasto;
	}
	public int getT_Gasto_ID() {
		return T_Gasto_ID;
	}
	public void setT_Gasto_ID(int t_Gasto_ID) {
		T_Gasto_ID = t_Gasto_ID;
	}
	public String getDescripcionGasto() {
		return DescripcionGasto;
	}
	public void setDescripcionGasto(String descripcionGasto) {
		DescripcionGasto = descripcionGasto;
	}
	public float getCantidad() {
		return Cantidad;
	}
	public void setCantidad(float cantidad) {
		Cantidad = cantidad;
	}
	public boolean isEstadoGasto() {
		return EstadoGasto;
	}
	public void setEstadoGasto(boolean estadoGasto) {
		EstadoGasto = estadoGasto;
	}
	public int getT_TipoGasto_ID() {
		return T_TipoGasto_ID;
	}
	public void setT_TipoGasto_ID(int t_TipoGasto_ID) {
		T_TipoGasto_ID = t_TipoGasto_ID;
	}
	public int getT_Actividad_ID() {
		return T_Actividad_ID;
	}
	public void setT_Actividad_ID(int t_Actividad_ID) {
		T_Actividad_ID = t_Actividad_ID;
	}
}
