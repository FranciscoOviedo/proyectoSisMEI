package vistas;

import java.util.Date;

public class V_ReunionDetalle {
	
	private int T_Reunion_ID;
	private Date Fecha;
	private String Tema;
	private String Descripcion;
	private String Observacion;
	private boolean Estado;
	private String NombreFormador;
	private String ApellidoFormador;
	private String TipoReunion;
	private String NombreFamilia;
	private String NombreMunicipio;
	private String NombreDepartamento;
	
	public int getT_Reunion_ID() {
		return T_Reunion_ID;
	}
	public void setT_Reunion_ID(int t_Reunion_ID) {
		T_Reunion_ID = t_Reunion_ID;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public String getTema() {
		return Tema;
	}
	public void setTema(String tema) {
		Tema = tema;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getObservacion() {
		return Observacion;
	}
	public void setObservacion(String observacion) {
		Observacion = observacion;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	public String getNombreFormador() {
		return NombreFormador;
	}
	public void setNombreFormador(String nombreFormador) {
		NombreFormador = nombreFormador;
	}
	public String getApellidoFormador() {
		return ApellidoFormador;
	}
	public void setApellidoFormador(String apellidoFormador) {
		ApellidoFormador = apellidoFormador;
	}
	public String getTipoReunion() {
		return TipoReunion;
	}
	public void setTipoReunion(String tipoReunion) {
		TipoReunion = tipoReunion;
	}
	public String getNombreFamilia() {
		return NombreFamilia;
	}
	public void setNombreFamilia(String nombreFamilia) {
		NombreFamilia = nombreFamilia;
	}
	public String getNombreMunicipio() {
		return NombreMunicipio;
	}
	public void setNombreMunicipio(String nombreMunicipio) {
		NombreMunicipio = nombreMunicipio;
	}
	public String getNombreDepartamento() {
		return NombreDepartamento;
	}
	public void setNombreDepartamento(String nombreDepartamento) {
		NombreDepartamento = nombreDepartamento;
	}
	
	

}
