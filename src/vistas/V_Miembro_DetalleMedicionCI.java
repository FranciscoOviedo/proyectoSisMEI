package vistas;

import java.util.ArrayList;

import entidades.DetalleMedicionCI;

public class V_Miembro_DetalleMedicionCI {
	private int T_Miembro_ID;
	private String Nombre;
	private String Apellido;
	private ArrayList<DetalleMedicionCI> Mediciones = new ArrayList<DetalleMedicionCI>();
	
	public int getT_Miembro_ID() {
		return T_Miembro_ID;
	}
	public void setT_Miembro_ID(int t_Miembro_ID) {
		T_Miembro_ID = t_Miembro_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public ArrayList<DetalleMedicionCI> getMediciones() {
		return Mediciones;
	}
	public void setMediciones(ArrayList<DetalleMedicionCI> mediciones) {
		Mediciones = mediciones;
	}
}
