package vistas;

import java.util.Date;

public class V_Info_Miembro {
	
	private int T_Miembro_ID;
	public int getT_Miembro_ID() {
		return T_Miembro_ID;
	}
	public void setT_Miembro_ID(int t_Miembro_ID) {
		T_Miembro_ID = t_Miembro_ID;
	}
	public int getT_Municipio_ID() {
		return T_Municipio_ID;
	}
	public void setT_Municipio_ID(int t_Municipio_ID) {
		T_Municipio_ID = t_Municipio_ID;
	}
	public int getT_Cargo_ID() {
		return T_Cargo_ID;
	}
	public void setT_Cargo_ID(int t_Cargo_ID) {
		T_Cargo_ID = t_Cargo_ID;
	}
	public int getT_TipoSocio_ID() {
		return T_TipoSocio_ID;
	}
	public void setT_TipoSocio_ID(int t_TipoSocio_ID) {
		T_TipoSocio_ID = t_TipoSocio_ID;
	}
	public int getT_FamiliaKolping_ID() {
		return T_FamiliaKolping_ID;
	}
	public void setT_FamiliaKolping_ID(int t_FamiliaKolping_ID) {
		T_FamiliaKolping_ID = t_FamiliaKolping_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public String getCedula() {
		return Cedula;
	}
	public void setCedula(String cedula) {
		Cedula = cedula;
	}
	public Date getFechaNacimiento() {
		return FechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		FechaNacimiento = fechaNacimiento;
	}
	public Date getFechaUnion() {
		return FechaUnion;
	}
	public void setFechaUnion(Date fechaUnion) {
		FechaUnion = fechaUnion;
	}
	public int getSexo() {
		return Sexo;
	}
	public void setSexo(int sexo) {
		Sexo = sexo;
	}
	public String getNivelAcademico() {
		return NivelAcademico;
	}
	public void setNivelAcademico(String nivelAcademico) {
		NivelAcademico = nivelAcademico;
	}
	public String getTelefono() {
		return Telefono;
	}
	public void setTelefono(String telefono) {
		Telefono = telefono;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	public String getNombreTipoSocio() {
		return NombreTipoSocio;
	}
	public void setNombreTipoSocio(String nombreTipoSocio) {
		NombreTipoSocio = nombreTipoSocio;
	}
	public String getNombreFamiliaKolping() {
		return NombreFamiliaKolping;
	}
	public void setNombreFamiliaKolping(String nombreFamiliaKolping) {
		NombreFamiliaKolping = nombreFamiliaKolping;
	}
	public String getNombreDepartamento() {
		return NombreDepartamento;
	}
	public void setNombreDepartamento(String nombreDepartamento) {
		NombreDepartamento = nombreDepartamento;
	}
	public String getNombreMunicipio() {
		return NombreMunicipio;
	}
	public void setNombreMunicipio(String nombreMunicipio) {
		NombreMunicipio = nombreMunicipio;
	}
	public String getNombreCargo() {
		return NombreCargo;
	}
	public void setNombreCargo(String nombreCargo) {
		NombreCargo = nombreCargo;
	}
	private int T_Municipio_ID;
	private int T_Cargo_ID;
	private int T_TipoSocio_ID;
	private int T_FamiliaKolping_ID;
	private String Nombre;
	private String Apellido;
	private String Cedula;
	private Date FechaNacimiento;
	private Date FechaUnion;
	private int Sexo;
	private String NivelAcademico;
	private String Telefono;
	private boolean Estado;
	private String NombreTipoSocio;
	private String NombreFamiliaKolping;
	private String NombreDepartamento;
	private String NombreMunicipio;
	private String NombreCargo;
}
