package entidades;

public class Diocesana {

	private int T_Diocesana_ID;
	private String Nombre;
	private boolean Estado;
	public int getT_Diocesana_ID() {
		return T_Diocesana_ID;
	}
	public void setT_Diocesana_ID(int t_Diocesana_ID) {
		T_Diocesana_ID = t_Diocesana_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
}
