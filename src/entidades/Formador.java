package entidades;

import java.util.Date;

public class Formador {
	private int T_Formador_ID;
	private int T_Municipio_ID;
	private String Nombre;
	private String Apellido;
	private String Cedula;
	private Date FechaNacimiento;
	private Date FechaUnion;
	private String Tipo;
	private int Sexo;
	private String Telefono;
	private boolean Estado;
	
	public int getT_Formador_ID() {
		return T_Formador_ID;
	}
	public void setT_Formador_ID(int t_Formador_ID) {
		T_Formador_ID = t_Formador_ID;
	}
	public int getT_Municipio_ID() {
		return T_Municipio_ID;
	}
	public void setT_Municipio_ID(int t_Municipio_ID) {
		T_Municipio_ID = t_Municipio_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getApellido() {
		return Apellido;
	}
	public void setApellido(String apellido) {
		Apellido = apellido;
	}
	public String getCedula() {
		return Cedula;
	}
	public void setCedula(String cedula) {
		Cedula = cedula;
	}
	public Date getFechaNacimiento() {
		return FechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		FechaNacimiento = fechaNacimiento;
	}
	public Date getFechaUnion() {
		return FechaUnion;
	}
	public void setFechaUnion(Date fechaUnion) {
		FechaUnion = fechaUnion;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	public int getSexo() {
		return Sexo;
	}
	public void setSexo(int sexo) {
		Sexo = sexo;
	}
	public String getTelefono() {
		return Telefono;
	}
	public void setTelefono(String telefono) {
		Telefono = telefono;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
