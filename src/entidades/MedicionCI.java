package entidades;

import java.util.Date;

public class MedicionCI {
	private int T_MedicionCI_ID;
	private int T_HerramientaCI_ID;
	private Date Fecha;
	private boolean Estado;
	
	public int getT_MedicionCI_ID() {
		return T_MedicionCI_ID;
	}
	public void setT_MedicionCI_ID(int t_MedicionCI_ID) {
		T_MedicionCI_ID = t_MedicionCI_ID;
	}
	public int getT_HerramientaCI_ID() {
		return T_HerramientaCI_ID;
	}
	public void setT_HerramientaCI_ID(int t_HerramientaCI_ID) {
		T_HerramientaCI_ID = t_HerramientaCI_ID;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
