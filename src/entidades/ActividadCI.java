package entidades;

public class ActividadCI {
	private int T_ActividadCI_ID;
	private int T_ObjetivoCI_ID;
	private String Nombre;
	private boolean Estado;
	
	public int getT_ActividadCI_ID() {
		return T_ActividadCI_ID;
	}
	public void setT_ActividadCI_ID(int t_ActividadCI_ID) {
		T_ActividadCI_ID = t_ActividadCI_ID;
	}
	public int getT_ObjetivoCI_ID() {
		return T_ObjetivoCI_ID;
	}
	public void setT_ObjetivoCI_ID(int t_ObjetivoCI_ID) {
		T_ObjetivoCI_ID = t_ObjetivoCI_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
