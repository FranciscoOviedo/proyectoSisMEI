package entidades;

import java.util.Date;

public class MedicionMacroIndicador {

	private int T_MedicionMacroIndicador_ID;
	private int T_MacroIndicador_ID;
	private int Cantidad;
	private String Comentario;
	private Date Fecha;
	private String Estado;
	private int ID_User_Registra;
	private int ID_User_Modifica;
	private int ID_User_Elimina;
	private Date FechaRegistra;
	private Date FechaModifica;
	private Date FechaElimina;
	public int getT_MedicionMacroIndicador_ID() {
		return T_MedicionMacroIndicador_ID;
	}
	public void setT_MedicionMacroIndicador_ID(int t_MedicionMacroIndicador_ID) {
		T_MedicionMacroIndicador_ID = t_MedicionMacroIndicador_ID;
	}
	public int getT_MacroIndicador_ID() {
		return T_MacroIndicador_ID;
	}
	public void setT_MacroIndicador_ID(int t_MacroIndicador_ID) {
		T_MacroIndicador_ID = t_MacroIndicador_ID;
	}
	public int getCantidad() {
		return Cantidad;
	}
	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}
	public String getComentario() {
		return Comentario;
	}
	public void setComentario(String comentario) {
		Comentario = comentario;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public String getEstado() {
		return Estado;
	}
	public void setEstado(String estado) {
		Estado = estado;
	}
	public int getID_User_Registra() {
		return ID_User_Registra;
	}
	public void setID_User_Registra(int iD_User_Registra) {
		ID_User_Registra = iD_User_Registra;
	}
	public int getID_User_Modifica() {
		return ID_User_Modifica;
	}
	public void setID_User_Modifica(int iD_User_Modifica) {
		ID_User_Modifica = iD_User_Modifica;
	}
	public int getID_User_Elimina() {
		return ID_User_Elimina;
	}
	public void setID_User_Elimina(int iD_User_Elimina) {
		ID_User_Elimina = iD_User_Elimina;
	}
	public Date getFechaRegistra() {
		return FechaRegistra;
	}
	public void setFechaRegistra(Date fechaRegistra) {
		FechaRegistra = fechaRegistra;
	}
	public Date getFechaModifica() {
		return FechaModifica;
	}
	public void setFechaModifica(Date fechaModifica) {
		FechaModifica = fechaModifica;
	}
	public Date getFechaElimina() {
		return FechaElimina;
	}
	public void setFechaElimina(Date fechaElimina) {
		FechaElimina = fechaElimina;
	}
	
	
	
}
