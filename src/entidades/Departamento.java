package entidades;

public class Departamento {
	private int T_Departamento_ID;
	private String Nombre;
	private boolean Estado;
	public int getT_Departamento_ID() {
		return T_Departamento_ID;
	}
	public void setT_Departamento_ID(int t_Departamento_ID) {
		T_Departamento_ID = t_Departamento_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
