package entidades;

public class TipoSocio {
	private String Nombre;
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	
	public int getT_TipoSocio_ID() {
		return T_TipoSocio_ID;
	}
	public void setT_TipoSocio_ID(int t_TipoSocio_ID) {
		T_TipoSocio_ID = t_TipoSocio_ID;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	private boolean Estado;
	private int T_TipoSocio_ID;
}
