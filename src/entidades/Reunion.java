package entidades;
import java.util.Date;
public class Reunion {
	
	private int T_Reunion_ID;
	private int T_FamiliaKolping_ID;
	private int T_Formador_ID;
	private int T_TipoReunion_ID;
	private Date Fecha;
	private String Tema;
	private String Descripcion;
	private String Observacion;
	private boolean Estado;
	
	public int getT_Reunion_ID() {
		return T_Reunion_ID;
	}
	public void setT_Reunion_ID(int t_Reunion_ID) {
		T_Reunion_ID = t_Reunion_ID;
	}
	public int getT_FamiliaKolping_ID() {
		return T_FamiliaKolping_ID;
	}
	public void setT_FamiliaKolping_ID(int t_FamiliaKolping_ID) {
		T_FamiliaKolping_ID = t_FamiliaKolping_ID;
	}
	public int getT_Formador_ID() {
		return T_Formador_ID;
	}
	public void setT_Formador_ID(int t_Formador_ID) {
		T_Formador_ID = t_Formador_ID;
	}
	
	public int getT_TipoReunion_ID() {
		return T_TipoReunion_ID;
	}
	public void setT_TipoReunion_ID(int t_TipoReunion_ID) {
		T_TipoReunion_ID = t_TipoReunion_ID;
	}
	
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public String getTema() {
		return Tema;
	}
	public void setTema(String tema) {
		Tema = tema;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getObservacion() {
		return Observacion;
	}
	public void setObservacion(String observacion) {
		Observacion = observacion;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}

}

