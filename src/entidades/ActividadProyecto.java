package entidades;

import java.util.Date;

public class ActividadProyecto {
	private int T_Actividad_ID;
	private int T_MicroProyecto_ID;
	private int T_Formador_ID;
	private Date Fecha;
	private String Nombre;
	private String Descripcion;
	private float Estimado;
	private boolean Estado;
	public int getT_Actividad_ID() {
		return T_Actividad_ID;
	}
	public void setT_Actividad_ID(int t_Actividad_ID) {
		T_Actividad_ID = t_Actividad_ID;
	}
	public int getT_MicroProyecto_ID() {
		return T_MicroProyecto_ID;
	}
	public void setT_MicroProyecto_ID(int t_MicroProyecto_ID) {
		T_MicroProyecto_ID = t_MicroProyecto_ID;
	}
	public int getT_Formador_ID() {
		return T_Formador_ID;
	}
	public void setT_Formador_ID(int t_Formador_ID) {
		T_Formador_ID = t_Formador_ID;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public float getEstimado() {
		return Estimado;
	}
	public void setEstimado(float estimado) {
		Estimado = estimado;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
}
