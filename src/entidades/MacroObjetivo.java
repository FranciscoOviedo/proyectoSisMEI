package entidades;

import java.sql.Timestamp;

public class MacroObjetivo {

	private int T_MacroObjetivo_ID;
	private int T_MacroProyecto_ID;
	private String Definicion;
	private int Estado;
	private int ID_User_Registra;
	private int ID_User_Modifica;
	private int ID_User_Elimina;
	private Timestamp Fecha_Registra;
	private Timestamp Fecha_Modifica;
	private Timestamp Fecha_Elimina;
	
	public int getT_MacroObjetivo_ID() {
		return T_MacroObjetivo_ID;
	}
	public void setT_MacroObjetivo_ID(int t_MacroObjetivo_ID) {
		T_MacroObjetivo_ID = t_MacroObjetivo_ID;
	}
	public int getT_MacroProyecto_ID() {
		return T_MacroProyecto_ID;
	}
	public void setT_MacroProyecto_ID(int t_MacroProyecto_ID) {
		T_MacroProyecto_ID = t_MacroProyecto_ID;
	}
	public String getDefinicion() {
		return Definicion;
	}
	public void setDefinicion(String definicion) {
		Definicion = definicion;
	}
	public int getEstado() {
		return Estado;
	}
	public void setEstado(int estado) {
		Estado = estado;
	}
	public int getID_User_Registra() {
		return ID_User_Registra;
	}
	public void setID_User_Registra(int iD_User_Registra) {
		ID_User_Registra = iD_User_Registra;
	}
	public int getID_User_Modifica() {
		return ID_User_Modifica;
	}
	public void setID_User_Modifica(int iD_User_Modifica) {
		ID_User_Modifica = iD_User_Modifica;
	}
	public int getID_User_Elimina() {
		return ID_User_Elimina;
	}
	public void setID_User_Elimina(int iD_User_Elimina) {
		ID_User_Elimina = iD_User_Elimina;
	}
	public Timestamp getFecha_Registra() {
		return Fecha_Registra;
	}
	public void setFecha_Registra(Timestamp fechaRegistra) {
		Fecha_Registra = fechaRegistra;
	}
	public Timestamp getFecha_Modifica() {
		return Fecha_Modifica;
	}
	public void setFecha_Modifica(Timestamp fechaModifica) {
		Fecha_Modifica = fechaModifica;
	}
	public Timestamp getFecha_Elimina() {
		return Fecha_Elimina;
	}
	public void setFecha_Elimina(Timestamp fechaElimina) {
		Fecha_Elimina = fechaElimina;
	}
	
}
