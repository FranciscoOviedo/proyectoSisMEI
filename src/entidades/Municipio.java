package entidades;

public class Municipio {
	private int T_Municipio_ID;
	private int T_Departamento_ID;
	private String Nombre;
	private boolean Estado;
	public int getT_Municipio_ID() {
		return T_Municipio_ID;
	}
	public void setT_Municipio_ID(int t_Municipio_ID) {
		T_Municipio_ID = t_Municipio_ID;
	}
	public int getT_Departamento_ID() {
		return T_Departamento_ID;
	}
	public void setT_Departamento_ID(int t_Departamento_ID) {
		T_Departamento_ID = t_Departamento_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
