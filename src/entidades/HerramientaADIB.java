package entidades;

import java.util.Date;

public class HerramientaADIB {

	private int T_HerramientaADIB_ID;
	private int T_FamiliaKolping_ID;
	private int T_MacroProyecto_ID;
	private Date FechaCreacion;
	private boolean Estado;
	
	public int getT_HerramientaADIB_ID() {
		return T_HerramientaADIB_ID;
	}
	public void setT_HerramientaADIB_ID(int t_HerramientaADIB_ID) {
		T_HerramientaADIB_ID = t_HerramientaADIB_ID;
	}
	public int getT_FamiliaKolping_ID() {
		return T_FamiliaKolping_ID;
	}
	public void setT_FamiliaKolping_ID(int t_FamiliaKolping_ID) {
		T_FamiliaKolping_ID = t_FamiliaKolping_ID;
	}
	public int getT_MacroProyecto_ID() {
		return T_MacroProyecto_ID;
	}
	public void setT_MacroProyecto_ID(int t_MacroProyecto_ID) {
		T_MacroProyecto_ID = t_MacroProyecto_ID;
	}
	public Date getFechaCreacion() {
		return FechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		FechaCreacion = fechaCreacion;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	
}
