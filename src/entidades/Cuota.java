package entidades;

public class Cuota {

	private int Anio;
	private boolean Cancelado;
	private boolean Estado;
	private int T_Miembro_ID;
	private int T_CuotaMiembro_ID;
	
	public int getT_Miembro_ID() {
		return T_Miembro_ID;
	}
	public void setT_Miembro_ID(int t_Miembro_ID) {
		T_Miembro_ID = t_Miembro_ID;
	}
	
	public int getAnio() {
		return Anio;
	}
	public void setAnio(int anio) {
		Anio = anio;
	}
	public boolean isCancelado() {
		return Cancelado;
	}
	public void setCancelado(boolean cancelado) {
		Cancelado = cancelado;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	public int getT_CuotaMiembro_ID() {
		return T_CuotaMiembro_ID;
	}
	public void setT_CuotaMiembro_ID(int t_CuotaMiembro_ID) {
		T_CuotaMiembro_ID = t_CuotaMiembro_ID;
	}
	
	
}
