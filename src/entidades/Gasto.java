package entidades;

public class Gasto {
	private int T_Gasto_ID;
	private int T_Actividad_ID;
	private int T_TipoGasto_ID;
	private String Descripcion;
	private float Cantidad;
	private boolean Estado;
	
	public int getT_Gasto_ID() {
		return T_Gasto_ID;
	}
	public void setT_Gasto_ID(int t_Gasto_ID) {
		T_Gasto_ID = t_Gasto_ID;
	}
	public int getT_Actividad_ID() {
		return T_Actividad_ID;
	}
	public void setT_Actividad_ID(int t_Actividad_ID) {
		T_Actividad_ID = t_Actividad_ID;
	}
	public int getT_TipoGasto_ID() {
		return T_TipoGasto_ID;
	}
	public void setT_TipoGasto_ID(int t_TipoGasto_ID) {
		T_TipoGasto_ID = t_TipoGasto_ID;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public float getCantidad() {
		return Cantidad;
	}
	public void setCantidad(float cantidad) {
		Cantidad = cantidad;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
