package entidades;

public class ObjetivoMiembro {
	private int T_ObjetivoMiembro_ID;
	private int T_Miembro_ID;
	private boolean Estado;
	
	public int getT_ObjetivoCIMiembro_ID() {
		return T_ObjetivoMiembro_ID;
	}
	public void setT_ObjetivoCIMiembro_ID(int t_ObjetivoCIMiembro_ID) {
		T_ObjetivoMiembro_ID = t_ObjetivoCIMiembro_ID;
	}
	public int getT_Miembro_ID() {
		return T_Miembro_ID;
	}
	public void setT_Miembro_ID(int t_Miembro_ID) {
		T_Miembro_ID = t_Miembro_ID;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
