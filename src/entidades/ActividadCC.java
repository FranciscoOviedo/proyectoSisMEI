package entidades;

public class ActividadCC {
	
	private int T_ActividadCC_ID;
	public int getT_ActividadCC_ID() {
		return T_ActividadCC_ID;
	}
	public void setT_ActividadCC_ID(int t_ActividadCC_ID) {
		T_ActividadCC_ID = t_ActividadCC_ID;
	}
	public int getT_ObjetivoCC_ID() {
		return T_ObjetivoCC_ID;
	}
	public void setT_ObjetivoCC_ID(int t_ObjetivoCC_ID) {
		T_ObjetivoCC_ID = t_ObjetivoCC_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	private int T_ObjetivoCC_ID;
	private String Nombre;
	private boolean Estado;
	
}
