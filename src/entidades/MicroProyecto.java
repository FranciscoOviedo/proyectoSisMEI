package entidades;

import java.sql.Timestamp;
import java.util.Date;

public class MicroProyecto {
	
	private int T_MicroProyecto_ID;
	private int T_FamiliaKolping_ID;
	private String Nombre;
	private String ObjetivoGeneral;
	private Date FechaInicio;
	private Date FechaFin;
	private String Tipo;
	private int Estado;
	private int ID_User_Registra;
	private int ID_User_Modifica;
	private int ID_User_Elimina;
	private Timestamp Fecha_Registra;
	private Timestamp Fecha_Modifica;
	private Timestamp Fecha_Elimina;
	
	public int getT_MicroProyecto_ID() {
		return T_MicroProyecto_ID;
	}
	public void setT_MicroProyecto_ID(int t_MicroProyecto_ID) {
		T_MicroProyecto_ID = t_MicroProyecto_ID;
	}
	public int getT_FamiliaKolping_ID() {
		return T_FamiliaKolping_ID;
	}
	public void setT_FamiliaKolping_ID(int t_FamiliaKolping_ID) {
		T_FamiliaKolping_ID = t_FamiliaKolping_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getObjetivoGeneral() {
		return ObjetivoGeneral;
	}
	public void setObjetivoGeneral(String objetivoGeneral) {
		ObjetivoGeneral = objetivoGeneral;
	}
	public Date getFechaInicio() {
		return FechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		FechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return FechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		FechaFin = fechaFin;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	public int getEstado() {
		return Estado;
	}
	public void setEstado(int estado) {
		Estado = estado;
	}
	public int getID_User_Registra() {
		return ID_User_Registra;
	}
	public void setID_User_Registra(int iD_User_Registra) {
		ID_User_Registra = iD_User_Registra;
	}
	public int getID_User_Modifica() {
		return ID_User_Modifica;
	}
	public void setID_User_Modifica(int iD_User_Modifica) {
		ID_User_Modifica = iD_User_Modifica;
	}
	public int getID_User_Elimina() {
		return ID_User_Elimina;
	}
	public void setID_User_Elimina(int iD_User_Elimina) {
		ID_User_Elimina = iD_User_Elimina;
	}
	public Timestamp getFecha_Registra() {
		return Fecha_Registra;
	}
	public void setFecha_Registra(Timestamp fecha_Registra) {
		Fecha_Registra = fecha_Registra;
	}
	public Timestamp getFecha_Modifica() {
		return Fecha_Modifica;
	}
	public void setFecha_Modifica(Timestamp fecha_Modifica) {
		Fecha_Modifica = fecha_Modifica;
	}
	public Timestamp getFecha_Elimina() {
		return Fecha_Elimina;
	}
	public void setFecha_Elimina(Timestamp fecha_Elimina) {
		Fecha_Elimina = fecha_Elimina;
	}
	
}
