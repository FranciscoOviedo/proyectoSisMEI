package entidades;

public class TipoGasto {
	private int T_TipoGasto_ID;
	private String Nombre;
	private boolean Estado;
	
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	public int getT_TipoGasto_ID() {
		return T_TipoGasto_ID;
	}
	public void setT_TipoGasto_ID(int t_TipoGasto_ID) {
		T_TipoGasto_ID = t_TipoGasto_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
}
