package entidades;

public class TareaCI {
	private int T_TareaCI_ID;
	private int T_ActividadCI_ID;
	private String Nombre;
	private String Definicion;
	private boolean Completado;
	private boolean Estado;
	
	public int getT_TareaCI_ID() {
		return T_TareaCI_ID;
	}
	public void setT_TareaCI_ID(int t_TareaCI_ID) {
		T_TareaCI_ID = t_TareaCI_ID;
	}
	public int getT_ActividadCI_ID() {
		return T_ActividadCI_ID;
	}
	public void setT_ActividadCI_ID(int t_ActividadCI_ID) {
		T_ActividadCI_ID = t_ActividadCI_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDefinicion() {
		return Definicion;
	}
	public void setDefinicion(String definicion) {
		Definicion = definicion;
	}
	public boolean isCompletado() {
		return Completado;
	}
	public void setCompletado(boolean completado) {
		Completado = completado;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
