package entidades;

public class ObjetivoCI {
	private int T_ObjetivoCI_ID;
	private int T_HerramientaCI_ID;
	private int T_Categoria_ID;
	private String Nombre;
	private String Criterio1;
	private String Criterio2;
	private String Criterio3;
	private String Criterio4;
	private String Criterio5;
	private boolean Estado;
	
	public int getT_ObjetivoCI_ID() {
		return T_ObjetivoCI_ID;
	}
	public void setT_ObjetivoCI_ID(int t_ObjetivoCI_ID) {
		T_ObjetivoCI_ID = t_ObjetivoCI_ID;
	}
	public int getT_HerramientaCI_ID() {
		return T_HerramientaCI_ID;
	}
	public void setT_HerramientaCI_ID(int t_HerramientaCI_ID) {
		T_HerramientaCI_ID = t_HerramientaCI_ID;
	}
	public int getT_Categoria_ID() {
		return T_Categoria_ID;
	}
	public void setT_Categoria_ID(int t_Categoria_ID) {
		T_Categoria_ID = t_Categoria_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getCriterio1() {
		return Criterio1;
	}
	public void setCriterio1(String criterio1) {
		Criterio1 = criterio1;
	}
	public String getCriterio2() {
		return Criterio2;
	}
	public void setCriterio2(String criterio2) {
		Criterio2 = criterio2;
	}
	public String getCriterio3() {
		return Criterio3;
	}
	public void setCriterio3(String criterio3) {
		Criterio3 = criterio3;
	}
	public String getCriterio4() {
		return Criterio4;
	}
	public void setCriterio4(String criterio4) {
		Criterio4 = criterio4;
	}
	public String getCriterio5() {
		return Criterio5;
	}
	public void setCriterio5(String criterio5) {
		Criterio5 = criterio5;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
