package entidades;


public class Familia {
	
	private int T_FamiliaKolping_ID;
	private int T_Diocesana_ID;
	private int T_Municipio_ID;
	private String Nombre;
	private int Distancia;
	private float TiempoViaje;
	private String Tipo;
	private boolean Estado;
	
	
	public int getT_FamiliaKolping_ID() {
		return T_FamiliaKolping_ID;
	}
	public void setT_FamiliaKolping_ID(int t_FamiliaKolping_ID) {
		T_FamiliaKolping_ID = t_FamiliaKolping_ID;
	}
	public int getT_Diocesana_ID() {
		return T_Diocesana_ID;
	}
	public void setT_Diocesana_ID(int t_Diocesana_ID) {
		T_Diocesana_ID = t_Diocesana_ID;
	}
	public int getT_Municipio_ID() {
		return T_Municipio_ID;
	}
	public void setT_Municipio_ID(int t_Municipio_ID) {
		T_Municipio_ID = t_Municipio_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public int getDistancia() {
		return Distancia;
	}
	public void setDistancia(int distancia) {
		Distancia = distancia;
	}
	public float getTiempoViaje() {
		return TiempoViaje;
	}
	public void setTiempoViaje(float tiempoViaje) {
		TiempoViaje = tiempoViaje;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
