package entidades;

public class DetalleMedicionCI {
	private int T_DetalleMedicionCI_ID;
	private int T_ObjetivoCI_ID;
	private int T_MedicionCI_ID;
	private int T_Miembro_ID;
	private int Valor;
	private boolean Estado;
	
	public int getT_DetalleMedicionCI_ID() {
		return T_DetalleMedicionCI_ID;
	}
	public void setT_DetalleMedicionCI_ID(int t_DetalleMedicionCI_ID) {
		T_DetalleMedicionCI_ID = t_DetalleMedicionCI_ID;
	}
	public int getT_ObjetivoCI_ID() {
		return T_ObjetivoCI_ID;
	}
	public void setT_ObjetivoCI_ID(int t_ObjetivoCI_ID) {
		T_ObjetivoCI_ID = t_ObjetivoCI_ID;
	}
	public int getT_MedicionCI_ID() {
		return T_MedicionCI_ID;
	}
	public void setT_MedicionCI_ID(int t_MedicionCI_ID) {
		T_MedicionCI_ID = t_MedicionCI_ID;
	}
	public int getT_Miembro_ID() {
		return T_Miembro_ID;
	}
	public void setT_Miembro_ID(int t_Miembro_ID) {
		T_Miembro_ID = t_Miembro_ID;
	}
	public int getValor() {
		return Valor;
	}
	public void setValor(int valor) {
		Valor = valor;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
