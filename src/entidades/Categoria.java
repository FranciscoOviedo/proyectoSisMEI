package entidades;

public class Categoria {
	private int T_Categoria_ID;
	private String Nombre;
	private String Objetivo;
	private boolean Estado;
	
	public int getT_Categoria_ID() {
		return T_Categoria_ID;
	}
	public void setT_Categoria_ID(int t_Categoria_ID) {
		T_Categoria_ID = t_Categoria_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getObjetivo() {
		return Objetivo;
	}
	public void setObjetivo(String objetivo) {
		Objetivo = objetivo;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
}
