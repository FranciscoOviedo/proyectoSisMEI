package entidades;

public class TareaCC {
	
	private int T_TareaCC_ID;
	public int getT_TareaCC_ID() {
		return T_TareaCC_ID;
	}
	public void setT_TareaCC_ID(int t_TareaCC_ID) {
		T_TareaCC_ID = t_TareaCC_ID;
	}
	public int getT_ActividadCC_ID() {
		return T_ActividadCC_ID;
	}
	public void setT_ActividadCC_ID(int t_ActividadCC_ID) {
		T_ActividadCC_ID = t_ActividadCC_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDefinicion() {
		return Definicion;
	}
	public void setDefinicion(String definicion) {
		Definicion = definicion;
	}
	public boolean isCompletado() {
		return Completado;
	}
	public void setCompletado(boolean completado) {
		Completado = completado;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	private int T_ActividadCC_ID;
	private String Nombre;
	private String Definicion;
	private boolean Completado;
	private boolean Estado;
	
}
