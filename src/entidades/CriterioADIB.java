package entidades;

public class CriterioADIB {
	
	private String Nombre;
	private String Descripcion;
	private String Categoria1;
	private String Categoria2;
	private String Categoria3;
	private String Categoria4;
	private String Categoria5;
	private boolean Estado;
	
	
	private int T_CriterioBienestar_ID;
	public int getT_CriterioBienestar_ID() {
		return T_CriterioBienestar_ID;
	}
	public void setT_CriterioBienestar_ID(int t_CriterioBienestar_ID) {
		T_CriterioBienestar_ID = t_CriterioBienestar_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getCategoria1() {
		return Categoria1;
	}
	public void setCategoria1(String categoria1) {
		Categoria1 = categoria1;
	}
	public String getCategoria2() {
		return Categoria2;
	}
	public void setCategoria2(String categoria2) {
		Categoria2 = categoria2;
	}
	public String getCategoria3() {
		return Categoria3;
	}
	public void setCategoria3(String categoria3) {
		Categoria3 = categoria3;
	}
	public String getCategoria4() {
		return Categoria4;
	}
	public void setCategoria4(String categoria4) {
		Categoria4 = categoria4;
	}
	public String getCategoria5() {
		return Categoria5;
	}
	public void setCategoria5(String categoria5) {
		Categoria5 = categoria5;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}

	

}
