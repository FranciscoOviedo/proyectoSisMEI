package entidades;

import java.util.Date;

public class MedicionCC {

	private int T_MedicionCC_ID;
	public int getT_MedicionCC_ID() {
		return T_MedicionCC_ID;
	}
	public void setT_MedicionCC_ID(int t_MedicionCC_ID) {
		T_MedicionCC_ID = t_MedicionCC_ID;
	}
	public int getT_HerramientaCC_ID() {
		return T_HerramientaCC_ID;
	}
	public void setT_HerramientaCC_ID(int t_HerramientaCC_ID) {
		T_HerramientaCC_ID = t_HerramientaCC_ID;
	}
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	private int T_HerramientaCC_ID;
	private Date Fecha;
	private boolean Estado;
}
