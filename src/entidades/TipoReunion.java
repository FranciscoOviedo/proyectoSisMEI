package entidades;

public class TipoReunion {
	private int T_TipoReunion_ID;
	private String Nombre;
	private boolean Estado;
	
	public int getT_TipoReunion_ID() {
		return T_TipoReunion_ID;
	}
	public void setT_TipoReunion_ID(int t_TipoReunion_ID) {
		T_TipoReunion_ID = t_TipoReunion_ID;
	}
	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String nombre) {
		Nombre = nombre;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	
	

}
