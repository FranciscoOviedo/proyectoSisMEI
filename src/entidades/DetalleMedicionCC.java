package entidades;

public class DetalleMedicionCC {

	private int T_DetalleMedicionCC_ID;
	public int getT_DetalleMedicionCC_ID() {
		return T_DetalleMedicionCC_ID;
	}
	public void setT_DetalleMedicionCC_ID(int t_DetalleMedicionCC_ID) {
		T_DetalleMedicionCC_ID = t_DetalleMedicionCC_ID;
	}
	public int getT_ObjetivoCC_ID() {
		return T_ObjetivoCC_ID;
	}
	public void setT_ObjetivoCC_ID(int t_ObjetivoCC_ID) {
		T_ObjetivoCC_ID = t_ObjetivoCC_ID;
	}
	public int getT_MedicionCC_ID() {
		return T_MedicionCC_ID;
	}
	public void setT_MedicionCC_ID(int t_MedicionCC_ID) {
		T_MedicionCC_ID = t_MedicionCC_ID;
	}
	public int getValor() {
		return Valor;
	}
	public void setValor(int valor) {
		Valor = valor;
	}
	public boolean isEstado() {
		return Estado;
	}
	public void setEstado(boolean estado) {
		Estado = estado;
	}
	private int T_ObjetivoCC_ID;
	private int T_MedicionCC_ID;
	private int Valor;
	private boolean Estado;
	
}
