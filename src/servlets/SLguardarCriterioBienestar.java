package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTCriterioADIB;
import entidades.CriterioADIB;

/**
 * Servlet implementation class SLguardarCriterio
 */
@WebServlet("/SLguardarCriterioBienestar")
public class SLguardarCriterioBienestar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarCriterioBienestar() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTCriterioADIB dtc = new DTCriterioADIB();
			CriterioADIB c = new CriterioADIB();
			
			String nombre, descripcion, categoria1, categoria2, categoria3, categoria4, categoria5;
			Boolean estado;
			
			nombre = request.getParameter("Nombre");
			descripcion = request.getParameter("Descripcion");
			categoria1 = request.getParameter("Categoria1");
			categoria2 = request.getParameter("Categoria2");
			categoria3 = request.getParameter("Categoria3");
			categoria4 = request.getParameter("Categoria4");
			categoria5 = request.getParameter("Categoria5");
			estado = true;
			
			c.setNombre(nombre);
			c.setDescripcion(descripcion);
			c.setCategoria1(categoria1);
			c.setCategoria2(categoria2);
			c.setCategoria3(categoria3);
			c.setCategoria4(categoria4);
			c.setCategoria5(categoria5);
			c.setEstado(estado);
			
			if(dtc.guardarCriterioADIB(c))
			{
				response.sendRedirect("view/herramientaADIB/listaCriterioBienestar.jsp");
			}
			else
			{
				response.sendRedirect("view/herramientaADIB/listaCriterioBienestar.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
	}

}
