package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTFormador;
import entidades.Formador;

/**
 * Servlet implementation class SLrestaurarFormador
 */
@WebServlet("/SLrestaurarFormador")
public class SLrestaurarFormador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarFormador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idFormador = "";
		idFormador = request.getParameter("id");
		idFormador = idFormador==null?"0":idFormador;
		try 
		{
			DTFormador dtf = new DTFormador();
			Formador f = new Formador();
			f.setT_Formador_ID(Integer.parseInt(idFormador));
			
			if(dtf.restaurarFormador(f))
			{
				response.sendRedirect("view/formador/listaFormador.jsp");
			}
			else
			{
				response.sendRedirect("view/formador/listaFormador.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
