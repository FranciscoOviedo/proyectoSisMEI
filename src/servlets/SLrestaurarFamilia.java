package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTFamilia;
import entidades.Familia;

/**
 * Servlet implementation class SLrestaurarFamilia
 */
@WebServlet("/SLrestaurarFamilia")
public class SLrestaurarFamilia extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarFamilia() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idFamilia = "";
		idFamilia = request.getParameter("id");
		idFamilia= idFamilia==null?"0":idFamilia;
		try 
		{
			DTFamilia dtfa = new DTFamilia();
			Familia fa = new Familia();
			fa.setT_FamiliaKolping_ID(Integer.parseInt(idFamilia));
			
			if(dtfa.restaurarFamilia(fa))
			{
				response.sendRedirect("view/Familias/listadoFamilias.jsp");
			}
			else
			{
				response.sendRedirect("view/Familias/listadoFamilias.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
