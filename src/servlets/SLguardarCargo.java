package servlets;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTCargo;
import entidades.Cargo;

/**
 * Servlet implementation class SLguardarCargo
 */
@WebServlet("/SLguardarCargo")
public class SLguardarCargo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarCargo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTCargo dtcar = new DTCargo();
			Cargo car = new Cargo();
			
			String nombre;
			Boolean estado;
			
			nombre = request.getParameter("Nombre");
			estado = true;
			
			
			car.setNombre(nombre);
			car.setEstado(estado);
			
			if(dtcar.guardarCargo(car))
			{
				response.sendRedirect("view/cargos/listaCargo.jsp");
			}
			else
			{
				response.sendRedirect("view/cargos/crearCargo.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
		
	}

}