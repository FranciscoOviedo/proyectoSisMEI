package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTTipoSocio;
import entidades.TipoSocio;

/**
 * Servlet implementation class SLguardatTipoSocio
 */
@WebServlet("/SLguardarTipoSocio")
public class SLguardarTipoSocio extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarTipoSocio() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTTipoSocio dtsocio = new DTTipoSocio();
			TipoSocio soc = new TipoSocio();
			
			String nombre;
			Boolean estado;
			
			nombre = request.getParameter("Nombre");
			estado = true;
			
			
			soc.setNombre(nombre);
			soc.setEstado(estado);
			
			if(dtsocio.guardarSocio(soc))
			{
				response.sendRedirect("view/TipoSocio/listaSocio.jsp");
			}
			else
			{
				response.sendRedirect("view/TipoSocio/crearTipoSocio.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
	}

}
