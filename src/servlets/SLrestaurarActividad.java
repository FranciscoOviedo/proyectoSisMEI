package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTActividadProyecto;
import entidades.ActividadProyecto;

/**
 * Servlet implementation class SLrestaurarActividad
 */
@WebServlet("/SLrestaurarActividad")
public class SLrestaurarActividad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarActividad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idActividad = "";
		idActividad = request.getParameter("id");
		idActividad = idActividad==null?"0":idActividad;
		try 
		{
			DTActividadProyecto dta = new DTActividadProyecto();
			ActividadProyecto a = new ActividadProyecto();
			a.setT_Actividad_ID(Integer.parseInt(idActividad));
			
			if(dta.restaurarActividadProyecto(a))
			{
				response.sendRedirect("view/actividad/listaActividad.jsp");
			}
			else
			{
				response.sendRedirect("view/actividad/listaActividad.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
