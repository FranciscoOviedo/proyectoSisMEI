package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import datos.DTObjetivoCC;
import entidades.ObjetivoCC;

/**
 * Servlet implementation class SLeditarObjetivoCC
 */
@WebServlet("/SLeditarObjetivoCC")
public class SLeditarObjetivoCC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarObjetivoCC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String t_ObjetivoCC_ID = "";
		t_ObjetivoCC_ID = request.getParameter("T_ObjetivoCC_ID");
		t_ObjetivoCC_ID = t_ObjetivoCC_ID==null?"0":t_ObjetivoCC_ID;

		String t_HerramientaCC_ID, t_Categoria_ID, nombre, criterio1, criterio2, criterio3, criterio4, criterio5;
		Boolean estado;
		
		try 
		{
			DTObjetivoCC dtoc = new DTObjetivoCC();
			ObjetivoCC oc = new ObjetivoCC();
			
			t_HerramientaCC_ID = request.getParameter("T_HerramientaCC_ID");
			t_Categoria_ID = request.getParameter("T_Categoria_ID");
			nombre = request.getParameter("Nombre");
			criterio1 = request.getParameter("Criterio1");
			criterio2 = request.getParameter("Criterio2");
			criterio3 = request.getParameter("Criterio3");
			criterio4 = request.getParameter("Criterio4");
			criterio5 = request.getParameter("Criterio5");
			estado = true;
			
			oc.setT_ObjetivoCC_ID(Integer.valueOf(t_ObjetivoCC_ID));
			oc.setT_HerramientaCC_ID(Integer.valueOf(t_HerramientaCC_ID));
			oc.setT_Categoria_ID(Integer.valueOf(t_Categoria_ID));
			oc.setNombre(nombre);
			oc.setCriterio1(criterio1);
			oc.setCriterio2(criterio2);
			oc.setCriterio3(criterio3);
			oc.setCriterio4(criterio4);
			oc.setCriterio5(criterio5);
			oc.setEstado(estado);
			
			if(dtoc.modificarObjetivoCC(oc))
			{
				response.sendRedirect("view/herramientaCC/detalleHerramientaCC.jsp?id="+oc.getT_HerramientaCC_ID());
			}
			else
			{
				response.sendRedirect("view/herramientaCC/detalleHerramientaCC.jsp?id="+oc.getT_HerramientaCC_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
