package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroProyecto;
import entidades.MacroProyecto;

/**
 * Servlet implementation class SLguardarMacroProyecto
 */
@WebServlet("/SLguardarMacroProyecto")
public class SLguardarMacroProyecto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMacroProyecto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMacroProyecto dtmacro = new DTMacroProyecto();
			MacroProyecto macro = new MacroProyecto();
			
			String nombre, objetivoGeneral, ID_User_Registra;
			
			//Para trabajar con fechas
			Date fechaInicio, fechaFin;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			nombre = request.getParameter("Nombre");
			objetivoGeneral = request.getParameter("ObjetivoGeneral");
			//Para trabajar con fechas
			fechaInicio = format.parse(request.getParameter("FechaInicio"));
			fechaFin = format.parse(request.getParameter("FechaFin"));
			ID_User_Registra = "1"; //TODO: Por ahora solo se pueden crear Macroproyectos con el primer usuario en la base de datos
			
			
			
			macro.setNombre(nombre);
			macro.setObjetivoGeneral(objetivoGeneral);
			macro.setFechaInicio(fechaInicio);
			macro.setFechaFin(fechaFin);
			macro.setEstado(1);
			macro.setID_User_Registra(Integer.valueOf(ID_User_Registra));
			macro.setFecha_Registra(new Timestamp(new Date().getTime()));
			
			if(dtmacro.guardarMacroProyecto(macro))
			{
				response.sendRedirect("view/proyectos/listaMacroProyecto.jsp");
			}
			else
			{
				response.sendRedirect("view/proyectos/listaMacroProyecto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� PROYECTO: " +e.getMessage());
		}
		
	}

}
