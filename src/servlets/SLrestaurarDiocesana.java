package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTDiocesana;
import entidades.Diocesana;

/**
 * Servlet implementation class SLrestaurarDiocesana
 */
@WebServlet("/SLrestaurarDiocesana")
public class SLrestaurarDiocesana extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarDiocesana() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idDiocesana = "";
		idDiocesana = request.getParameter("id");
		idDiocesana= idDiocesana==null?"0":idDiocesana;
		try 
		{
			DTDiocesana dtdio = new DTDiocesana();
			Diocesana dio = new Diocesana();
			dio.setT_Diocesana_ID(Integer.parseInt(idDiocesana));
			
			if(dtdio.restaurarDiocesana(dio))
			{
				response.sendRedirect("view/Familias/listadoDiocesanas.jsp");
			}
			else
			{
				response.sendRedirect("view/Familias/listadoDiocesanas.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
