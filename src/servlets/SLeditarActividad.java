package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTActividadProyecto;
import entidades.ActividadProyecto;

/**
 * Servlet implementation class SLeditarActividad
 */
@WebServlet("/SLeditarActividad")
public class SLeditarActividad extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarActividad() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String t_Actividad_ID = "";
		t_Actividad_ID = request.getParameter("T_Actividad_ID");
		t_Actividad_ID = t_Actividad_ID==null?"0":t_Actividad_ID;

		String t_MicroProyecto_ID, t_Formador_ID, nombre, descripcion, estimado;
		Boolean estado;
		//Para trabajar con fechas
		Date fecha;
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		try 
		{
			DTActividadProyecto dtap = new DTActividadProyecto();
			ActividadProyecto ap = new ActividadProyecto();
			
			t_MicroProyecto_ID = request.getParameter("T_MicroProyecto_ID");
			t_Formador_ID = request.getParameter("T_Formador_ID");
			fecha = format.parse(request.getParameter("Fecha"));
			nombre = request.getParameter("Nombre");
			descripcion = request.getParameter("Descripcion");
			estimado = request.getParameter("Estimado");
			estado = true;
			
			ap.setT_Actividad_ID(Integer.valueOf(t_Actividad_ID));
			
			ap.setT_MicroProyecto_ID(Integer.valueOf(t_MicroProyecto_ID));
			ap.setT_Formador_ID(Integer.valueOf(t_Formador_ID));
			ap.setFecha(fecha);
			ap.setNombre(nombre);
			ap.setDescripcion(descripcion);
			ap.setEstimado(Float.valueOf(estimado.replaceAll(",", "").toString()));
			ap.setEstado(estado);
			
			if(dtap.modificarActividadProyecto(ap))
			{
				response.sendRedirect("view/actividad/listaActividad.jsp");
			}
			else
			{
				response.sendRedirect("view/actividad/editarActividad.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
