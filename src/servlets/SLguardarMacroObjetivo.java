package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroObjetivo;
import entidades.MacroObjetivo;

/**
 * Servlet implementation class SLguardarGasto
 */
@WebServlet("/SLguardarMacroObjetivo")
public class SLguardarMacroObjetivo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMacroObjetivo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMacroObjetivo dto = new DTMacroObjetivo();
			MacroObjetivo o = new MacroObjetivo();
			
			String t_MacroProyecto_ID, definicion, ID_User_Registra;
			int estado;
			
			t_MacroProyecto_ID = request.getParameter("T_MacroProyecto_ID");
			definicion = request.getParameter("Definicion");
			estado = 0;
			ID_User_Registra = "1"; //TODO: Por ahora solo se pueden crear Macroproyectos con el primer usuario en la base de datos
			
			o.setT_MacroProyecto_ID(Integer.valueOf(t_MacroProyecto_ID));
			o.setDefinicion(definicion);
			o.setEstado(estado);
			o.setFecha_Registra(new Timestamp(new Date().getTime()));
			o.setID_User_Registra(Integer.valueOf(ID_User_Registra));
			
			if(dto.guardarMacroObjetivo(o))
			{
				response.sendRedirect("view/proyectos/detalleMacroProyecto.jsp?id="+o.getT_MacroProyecto_ID());
			}
			else
			{
				response.sendRedirect("view/proyectos/detalleMacroProyecto.jsp?id="+o.getT_MacroProyecto_ID());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� OBJETIVO: " +e.getMessage());
		}
	}

}
