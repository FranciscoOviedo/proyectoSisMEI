package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTReunion;
import entidades.Reunion;

/**
 * Servlet implementation class SLrestaurarReunion
 */
@WebServlet("/SLrestaurarReunion")
public class SLrestaurarReunion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarReunion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idReunion = "";
		idReunion = request.getParameter("id");
		idReunion = idReunion==null?"0":idReunion;
		try 
		{
			DTReunion dtr = new DTReunion();
			Reunion r = new Reunion();
			r.setT_Reunion_ID(Integer.parseInt(idReunion));
			
			if(dtr.restaurarReunion(r))
			{
				response.sendRedirect("view/Reuniones/listaReunion.jsp");
			}
			else
			{
				response.sendRedirect("view/Reuniones/listaReunion.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
