package servlets;

	import java.io.IOException;


	import javax.servlet.ServletException;
	import javax.servlet.annotation.WebServlet;
	import javax.servlet.http.HttpServlet;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import datos.DTDiocesana;
	import entidades.Diocesana;



	
	/**
	 * Servlet implementation class SLguardarDiocesana
	 */
	@WebServlet("/SLguardarDiocesana")
	public class SLguardarDiocesana extends HttpServlet {
		private static final long serialVersionUID = 1L;
	       
	    /**
	     * @see HttpServlet#HttpServlet()
	     */
	    public SLguardarDiocesana() {
	        super();
	        // TODO Auto-generated constructor stub
	    }

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			response.getWriter().append("Served at: ").append(request.getContextPath());
		}

		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			try 
			{
				DTDiocesana dtdio = new DTDiocesana();
				Diocesana dio = new Diocesana();
				
				String nombre;
				Boolean estado;
				
				nombre = request.getParameter("Nombre");
				estado = true;
				
				
				dio.setNombre(nombre);
				dio.setEstado(estado);
				
				if(dtdio.guardarDiocesana(dio))
				{
					response.sendRedirect("view/Familias/listadoDiocesanas.jsp");
				}
				else
				{
					response.sendRedirect("view/Familias/crearDiocesana.jsp?error");
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
				System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
			}
			
		}

	}

