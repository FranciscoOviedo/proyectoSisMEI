package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMiembro;
import entidades.Miembro;

/**
 * Servlet implementation class SLguardarMiembro
 */
@WebServlet("/SLguardarMiembro")
public class SLguardarMiembro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMiembro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMiembro dtm = new DTMiembro();
			Miembro m = new Miembro();
			
			String t_FamiliaKolping_ID, t_Cargo_ID, t_Municipio_ID, t_TipoSocio_ID, nombre, apellido, cedula, nivelAcademico, sexo, telefono;
			Boolean estado;
			
			//Para trabajar con fechas
			Date fechaNacimiento, fechaUnion;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_FamiliaKolping_ID = request.getParameter("T_FamiliaKolping_ID");
			t_Cargo_ID = request.getParameter("T_Cargo_ID");
			t_Municipio_ID = request.getParameter("T_Municipio_ID");
			t_TipoSocio_ID = request.getParameter("T_TipoSocio_ID");
			nombre = request.getParameter("Nombre");
			apellido = request.getParameter("Apellido");
			cedula = request.getParameter("Cedula");
			nivelAcademico = request.getParameter("NivelAcademico");
			//Para trabajar con fechas
			fechaNacimiento = format.parse(request.getParameter("FechaNacimiento"));
			fechaUnion = format.parse(request.getParameter("FechaUnion"));
			
			sexo = request.getParameter("Sexo");
			telefono = request.getParameter("Telefono");
			estado = true;
			
						
			m.setT_FamiliaKolping_ID(Integer.valueOf(t_FamiliaKolping_ID));
			m.setT_Cargo_ID(Integer.valueOf(t_Cargo_ID));
			m.setT_Municipio_ID(Integer.valueOf(t_Municipio_ID));
			m.setT_TipoSocio_ID(Integer.valueOf(t_TipoSocio_ID));
			m.setNombre(nombre);
			m.setApellido(apellido);
			m.setCedula(cedula);
			m.setFechaNacimiento(fechaNacimiento);
			m.setFechaUnion(fechaUnion);
			m.setNivelAcademico(nivelAcademico);
			m.setSexo(Integer.valueOf(sexo));
			m.setTelefono(telefono);
			m.setEstado(estado);
			
			if(dtm.guardarMiembro(m))
			{
				response.sendRedirect("view/miembros/listaMiembro.jsp");
			}
			else
			{
				response.sendRedirect("view/miembros/crearMiembro.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� MIEMBRO: " +e.getMessage());
		}
		
	}

}
