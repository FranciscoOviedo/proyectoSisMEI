package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroProyecto;
import datos.DTMicroProyecto;
import entidades.MacroProyecto;
import entidades.MicroProyecto;

/**
 * Servlet implementation class SLguardarMicroProyecto
 */
@WebServlet("/SLguardarMicroProyecto")
public class SLguardarMicroProyecto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMicroProyecto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMicroProyecto dtmicro = new DTMicroProyecto();
			MicroProyecto micro = new MicroProyecto();
			
			String t_FamiliaKolping_ID, nombre, objetivoGeneral, tipo, ID_User_Registra;
			
			//Para trabajar con fechas
			Date fechaInicio, fechaFin;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_FamiliaKolping_ID = request.getParameter("T_FamiliaKolping_ID");
			nombre = request.getParameter("Nombre");
			objetivoGeneral = request.getParameter("ObjetivoGeneral");
			//Para trabajar con fechas
			fechaInicio = format.parse(request.getParameter("FechaInicio"));
			fechaFin = format.parse(request.getParameter("FechaFin"));
			tipo = request.getParameter("Tipo");
			ID_User_Registra = "1"; //TODO: Por ahora solo se pueden crear Macroproyectos con el primer usuario en la base de datos
			
			
			micro.setT_FamiliaKolping_ID(Integer.valueOf(t_FamiliaKolping_ID));
			micro.setNombre(nombre);
			micro.setObjetivoGeneral(objetivoGeneral);
			micro.setEstado(1);
			micro.setFechaInicio(fechaInicio);
			micro.setFechaFin(fechaFin);
			micro.setTipo(tipo);
			micro.setID_User_Registra(Integer.valueOf(ID_User_Registra));
			micro.setFecha_Registra(new Timestamp(new Date().getTime()));
			
			if(dtmicro.guardarMicroProyecto(micro))
			{
				response.sendRedirect("view/proyectos/listaMicroProyecto.jsp");
			}
			else
			{
				response.sendRedirect("view/proyectos/editarMicroProyecto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� PROYECTO: " +e.getMessage());
		}

	}

}
