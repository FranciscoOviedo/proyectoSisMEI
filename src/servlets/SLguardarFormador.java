package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTFormador;
import entidades.Formador;

/**
 * Servlet implementation class SLguardarFormador
 */
@WebServlet("/SLguardarFormador")
public class SLguardarFormador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarFormador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTFormador dtf = new DTFormador();
			Formador f = new Formador();
			
			String t_Municipio_ID, nombre, apellido, cedula, tipo, sexo, telefono;
			Boolean estado;
			
			//Para trabajar con fechas
			Date fechaNacimiento, fechaUnion;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_Municipio_ID = request.getParameter("T_Municipio_ID");
			nombre = request.getParameter("Nombre");
			apellido = request.getParameter("Apellido");
			cedula = request.getParameter("Cedula");
			//Para trabajar con fechas
			fechaNacimiento = format.parse(request.getParameter("FechaNacimiento"));
			fechaUnion = format.parse(request.getParameter("FechaUnion"));
			tipo = request.getParameter("Tipo");
			sexo = request.getParameter("Sexo");
			telefono = request.getParameter("Telefono");
			estado = true;
			
			f.setT_Municipio_ID(Integer.valueOf(t_Municipio_ID));
			f.setNombre(nombre);
			f.setApellido(apellido);
			f.setCedula(cedula);
			f.setFechaNacimiento(fechaNacimiento);
			f.setFechaUnion(fechaUnion);
			f.setTipo(tipo);
			f.setSexo(Integer.valueOf(sexo));
			f.setTelefono(telefono);
			f.setEstado(estado);
			
			if(dtf.guardarFormador(f))
			{
				response.sendRedirect("view/formador/listaFormador.jsp");
			}
			else
			{
				response.sendRedirect("view/formador/crearFormador.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
		
	}

}
