package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import datos.DTCuotaMiembro;
import entidades.Cuota;

/**
 * Servlet implementation class SleditarCuota
 */
@WebServlet("/SleditarCuota")
public class SleditarCuota extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SleditarCuota() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String T_CuotaMiembro_ID = "";
		T_CuotaMiembro_ID = request.getParameter("T_CuotaMiembro_ID");
		T_CuotaMiembro_ID = T_CuotaMiembro_ID==null?"0":T_CuotaMiembro_ID;

		String nombre;
		Boolean estado;
		
		
		try 
		{
			DTCuotaMiembro dtcuo = new DTCuotaMiembro();
			Cuota cuo = new Cuota();
			
			
			Anio = request.getParameter("Anio");
			Cancelado = request.getParameter("Cancelado");
			estado = true;
			
			cuo.setT_Miembro_ID(t_Miembro_ID);
			cuo.setAnio(Anio);
			cuo.setCancelado(Cancelado);
			cuo.setEstado(estado);
			
			if(dtcuo.modificarCuota(cuo))
			{
				response.sendRedirect("view/cuotas/listaCuota.jsp");
			}
			else
			{
				response.sendRedirect("view/cuotas/crearCuota.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}

	}

}
