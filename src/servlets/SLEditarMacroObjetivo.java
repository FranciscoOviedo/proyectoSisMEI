package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroObjetivo;
import entidades.MacroObjetivo;

/**
 * Servlet implementation class SLeditarActividad
 */
@WebServlet("/SLeditarMacroObjetivo")
public class SLEditarMacroObjetivo extends HttpServlet{
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLEditarMacroObjetivo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idMacroObjetivo = "";
		idMacroObjetivo = request.getParameter("id");
		idMacroObjetivo = idMacroObjetivo==null?"0":idMacroObjetivo;

		String t_MacroObjetivo_ID, t_MacroProyecto_ID, definicion, ID_User_Registra ;
		int estado;
		
		try 
		{
			DTMacroObjetivo dtobj = new DTMacroObjetivo();
			MacroObjetivo obj = new MacroObjetivo();
			
			t_MacroProyecto_ID = request.getParameter("T_MacroProyecto_ID");
			t_MacroObjetivo_ID = request.getParameter("T_MacroObjetivo_ID");
			definicion = request.getParameter("Definicion");
			ID_User_Registra = "1"; //TODO: Por ahora solo se pueden crear MacroObjetivos con el primer usuario en la base de datos
			
			obj.setT_MacroObjetivo_ID(Integer.valueOf(t_MacroObjetivo_ID));
			
			obj.setT_MacroProyecto_ID(Integer.valueOf(t_MacroProyecto_ID));
			obj.setDefinicion(definicion);
			obj.setEstado(1);
			obj.setFecha_Registra(new Timestamp(new Date().getTime()));
			
			if(dtobj.modificarMacroObjetivo(obj))
			{
				response.sendRedirect("view/proyectos/detalleMacroProyecto.jsp");
			}
			else
			{
				response.sendRedirect("view/proyectos/detalleMacroProyecto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}
}
