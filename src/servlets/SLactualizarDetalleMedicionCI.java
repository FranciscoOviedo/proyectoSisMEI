package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTDetalleMedicionCI;
import entidades.DetalleMedicionCI;

/**
 * Servlet implementation class SLactualizarDetalleMedicionCI
 */
@WebServlet("/SLactualizarDetalleMedicionCI")
public class SLactualizarDetalleMedicionCI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLactualizarDetalleMedicionCI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String valor[] = request.getParameterValues("Valor");
		String detalleMedicionCI_ID[] = request.getParameterValues("DetalleMedicionCI_ID");
		String medicionCI_ID = request.getParameter("T_MedicionCI_ID");
		String miembro_ID[] = request.getParameterValues("Miembro_ID");
		String objetivoCI_ID[] = request.getParameterValues("ObjetivoCI_ID");
		
		DTDetalleMedicionCI ddmc = new DTDetalleMedicionCI();
		ArrayList<DetalleMedicionCI> detalleEliminar = new ArrayList<DetalleMedicionCI>();
		ArrayList<DetalleMedicionCI> detalleCrear = new ArrayList<DetalleMedicionCI>();
		ArrayList<DetalleMedicionCI> detalleActualizar = new ArrayList<DetalleMedicionCI>();
		
		for(int i = 0; i<valor.length; i++){
			if(Integer.valueOf(valor[i])==0){
				if(Integer.valueOf(detalleMedicionCI_ID[i])!=-1){
					//Eliminar
					DetalleMedicionCI temp = new DetalleMedicionCI();
					temp.setT_DetalleMedicionCI_ID(Integer.valueOf(detalleMedicionCI_ID[i]));
					detalleEliminar.add(temp);
				}
			} else {
				if(Integer.valueOf(detalleMedicionCI_ID[i])==-1){
					//Crear
					DetalleMedicionCI temp = new DetalleMedicionCI();
					temp.setValor(Integer.valueOf(valor[i]));
					temp.setT_MedicionCI_ID(Integer.valueOf(medicionCI_ID));
					temp.setT_Miembro_ID(Integer.valueOf(miembro_ID[i]));
					temp.setT_ObjetivoCI_ID(Integer.valueOf(objetivoCI_ID[i]));
					detalleCrear.add(temp);
				} else {
					//Actualizar
					DetalleMedicionCI temp = new DetalleMedicionCI();
					temp.setValor(Integer.valueOf(valor[i]));
					temp.setT_DetalleMedicionCI_ID(Integer.valueOf(detalleMedicionCI_ID[i]));
					temp.setT_MedicionCI_ID(Integer.valueOf(medicionCI_ID));
					temp.setT_Miembro_ID(Integer.valueOf(miembro_ID[i]));
					temp.setT_ObjetivoCI_ID(Integer.valueOf(objetivoCI_ID[i]));
					detalleActualizar.add(temp);
				}
			}
		}
		boolean error = true;
		if(ddmc.guardarListaDetalleMedicionCI(detalleCrear)){
			if(ddmc.modificarListaMedicionCI(detalleActualizar)){
				if(ddmc.eliminarListaDetalleMedicionCI(detalleEliminar)){
					error = false;
				}
			}
		}
		if(!error){
			response.sendRedirect("view/herramientaCI/detalleMedicionCI.jsp?idMedicion="+medicionCI_ID+"&success");
		}
		else
		{
			response.sendRedirect("view/herramientaCI/detalleMedicionCI.jsp?idMedicion="+medicionCI_ID+"&error");
		}
	}

}
