package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTReunion;
import entidades.Reunion;

/**
 * Servlet implementation class SLguardarReunion
 */

@WebServlet("/SLguardarReunion")
public class SLguardarReunion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
    public SLguardarReunion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try 
		{
			DTReunion dtr = new DTReunion();
			Reunion r = new Reunion();
			
			String t_FamiliaKolping_ID, t_Formador_ID, t_TipoReunion_ID, tema, descripcion, observacion;
			Boolean estado;
			
			//Para trabajar con fechas
			Date fecha;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_FamiliaKolping_ID = request.getParameter("T_FamiliaKolping_ID");
			t_Formador_ID = request.getParameter("T_Formador_ID");
			t_TipoReunion_ID = request.getParameter("T_TipoReunion_ID");
			tema = request.getParameter("Tema");
			descripcion = request.getParameter("Descripcion");
			observacion = request.getParameter("Observacion");
			
			//Para trabajar con fechas
			fecha = format.parse(request.getParameter("Fecha"));
			estado = true;
			
			r.setT_FamiliaKolping_ID(Integer.valueOf(t_FamiliaKolping_ID));
			r.setT_Formador_ID(Integer.valueOf(t_Formador_ID));
			r.setT_TipoReunion_ID(Integer.valueOf(t_TipoReunion_ID));
			r.setTema(tema);
			r.setDescripcion(descripcion);
			r.setObservacion(observacion);
			r.setFecha(fecha);
			r.setEstado(estado);
			
			if(dtr.guardarReunion(r))
			{
				response.sendRedirect("view/Reuniones/listaReunion.jsp");
			}
			else
			{
				response.sendRedirect("view/Reuniones/crearReunion.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� LA REUNI�N: " +e.getMessage());
		}
		
	}

}