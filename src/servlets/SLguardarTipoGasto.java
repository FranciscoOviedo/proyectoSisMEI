package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTTipoGasto;
import entidades.TipoGasto;

/**
 * Servlet implementation class SLguardarTipoGasto
 */
@WebServlet("/SLguardarTipoGasto")
public class SLguardarTipoGasto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarTipoGasto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTTipoGasto dttg = new DTTipoGasto();
			TipoGasto tg = new TipoGasto();
			
			String nombre;
			Boolean estado;
			
			nombre = request.getParameter("Nombre");
			estado = true;
			
			tg.setNombre(nombre);
			tg.setEstado(estado);
			
			if(dttg.guardarTipoGasto(tg))
			{
				response.sendRedirect("view/tipoGasto/listaTipoGasto.jsp");
			}
			else
			{
				response.sendRedirect("view/tipoGasto/crearTipoGasto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
	}

}
