package servlets;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTFamilia;
import entidades.Familia;


/**
 * Servlet implementation class SLeditarFamilia
 */
@WebServlet("/SLeditarFamilia")
public class SLeditarFamilia extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

	public SLeditarFamilia() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String t_FamiliaKolping_ID = "";
		t_FamiliaKolping_ID = request.getParameter("T_FamiliaKolping_ID");
		t_FamiliaKolping_ID = t_FamiliaKolping_ID==null?"0":t_FamiliaKolping_ID;

		String t_Diocesana_ID,t_Municipio_ID, nombre, tipo;
		int distancia;
		float tiempoviaje;
		Boolean estado;
	
		try 
		{
			DTFamilia dtfa = new DTFamilia();
			Familia fa = new Familia();
			
			fa.setT_FamiliaKolping_ID(Integer.parseInt(t_FamiliaKolping_ID));
			
			
			t_Diocesana_ID= request.getParameter("T_Diocesana_ID");
			t_Municipio_ID = request.getParameter("T_Municipio_ID");
			nombre = request.getParameter("Nombre");
			distancia =Integer.parseInt(request.getParameter("Distancia"));
			tiempoviaje= Float.parseFloat(request.getParameter("TiempoViaje"));
			tipo = request.getParameter("Tipo");
			estado = true;
			
			fa.setT_FamiliaKolping_ID(Integer.valueOf(t_FamiliaKolping_ID));
			fa.setT_Diocesana_ID(Integer.valueOf(t_Diocesana_ID));
			fa.setT_Municipio_ID(Integer.valueOf(t_Municipio_ID));
			fa.setNombre(nombre);
			fa.setDistancia(distancia);
		    fa.setTiempoViaje(tiempoviaje);
			fa.setTipo(tipo);
			fa.setEstado(estado);
			
			
			if(dtfa.modificarFamilia(fa))
			{
				response.sendRedirect("view/Familias/listadoFamilias.jsp");
			}
			else
			{
				response.sendRedirect("view/Familias/crearFamilia.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
