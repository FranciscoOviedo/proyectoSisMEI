package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTTipoSocio;
import entidades.TipoSocio;

/**
 * Servlet implementation class SleditarTipoSocio
 */
@WebServlet("/SLeditarTipoSocio")
public class SleditarTipoSocio extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SleditarTipoSocio() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String T_TipoSocio_ID = "";
		T_TipoSocio_ID = request.getParameter("T_TipoSocio_ID");
		T_TipoSocio_ID = T_TipoSocio_ID==null?"0":T_TipoSocio_ID;
		
		String Nombre;
		Boolean estado;
		
		
		try 
		{
			DTTipoSocio dtsoc = new DTTipoSocio();
			TipoSocio soc = new TipoSocio();
			soc.setT_TipoSocio_ID(Integer.parseInt(T_TipoSocio_ID));
			
			Nombre = request.getParameter("Nombre");
			estado = true;
			
			soc.setNombre(Nombre);
			soc.setEstado(estado);
			
			if(dtsoc.modificarSocio(soc))
			{
				response.sendRedirect("view/TipoSocio/listaSocio.jsp");
			}
			else
			{
				response.sendRedirect("view/TipoSocio/editarTipoSocio.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
