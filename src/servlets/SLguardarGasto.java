package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTGasto;
import entidades.Gasto;

/**
 * Servlet implementation class SLguardarGasto
 */
@WebServlet("/SLguardarGasto")
public class SLguardarGasto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarGasto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTGasto dtg = new DTGasto();
			Gasto g = new Gasto();
			
			String t_Actividad_ID, t_TipoGasto_ID, descripcion, cantidad;
			Boolean estado;
			
			t_Actividad_ID = request.getParameter("T_Actividad_ID");
			t_TipoGasto_ID = request.getParameter("T_TipoGasto_ID");
			descripcion = request.getParameter("Descripcion");
			cantidad = request.getParameter("Cantidad");
			estado = true;
			
			g.setT_Actividad_ID(Integer.valueOf(t_Actividad_ID));
			g.setT_TipoGasto_ID(Integer.valueOf(t_TipoGasto_ID));
			g.setDescripcion(descripcion);
			g.setCantidad(Float.valueOf(cantidad.replaceAll(",", "").toString()));
			g.setEstado(estado);
			
			if(dtg.guardarGasto(g))
			{
				response.sendRedirect("view/actividad/detalleActividad.jsp?id="+g.getT_Actividad_ID());
			}
			else
			{
				response.sendRedirect("view/actividad/detalleActividad.jsp?id="+g.getT_Actividad_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
	}

}
