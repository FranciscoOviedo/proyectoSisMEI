package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTCategoria;
import entidades.Categoria;

/**
 * Servlet implementation class SLeditarCategoria
 */
@WebServlet("/SLeditarCategoria")
public class SLeditarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarCategoria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String t_Categoria_ID = "";
		t_Categoria_ID = request.getParameter("T_Categoria_ID");
		t_Categoria_ID = t_Categoria_ID==null?"0":t_Categoria_ID;

		String nombre, objetivo;
		Boolean estado;
		//Para trabajar con fechas
		try 
		{
			DTCategoria dtc = new DTCategoria();
			Categoria c = new Categoria();
			
			nombre = request.getParameter("Nombre");
			objetivo = request.getParameter("Objetivo");
			estado = true;
			
			c.setT_Categoria_ID(Integer.valueOf(t_Categoria_ID));
			c.setNombre(nombre);
			c.setObjetivo(objetivo);
			c.setEstado(estado);
			
			if(dtc.modificarTipoGasto(c))
			{
				response.sendRedirect("view/categoriaCICC/listaCategoriaCICC.jsp");
			}
			else
			{
				response.sendRedirect("view/categoriaCICC/editarTipoGasto.jsp?id="+c.getT_Categoria_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
