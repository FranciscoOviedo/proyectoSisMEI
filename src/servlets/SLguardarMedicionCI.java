package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMedicionCI;
import entidades.MedicionCI;

/**
 * Servlet implementation class SLguardarMedicionCI
 */
@WebServlet("/SLguardarMedicionCI")
public class SLguardarMedicionCI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMedicionCI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMedicionCI dtmc = new DTMedicionCI();
			MedicionCI mc = new MedicionCI();
			
			String t_HerramientaCI_ID;
			Boolean estado;
			
			//Para trabajar con fechas
			Date fecha;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_HerramientaCI_ID = request.getParameter("T_HerramientaCI_ID");
			fecha = format.parse(request.getParameter("Fecha"));
			estado = true;
			
			mc.setT_HerramientaCI_ID(Integer.valueOf(t_HerramientaCI_ID));
			mc.setFecha(fecha);
			mc.setEstado(estado);
			
			if(dtmc.guardarMedicionCI(mc))
			{
				response.sendRedirect("view/herramientaCI/detalleHerramientaCI.jsp?id="+mc.getT_HerramientaCI_ID());
			}
			else
			{
				response.sendRedirect("view/herramientaCI/detalleHerramientaCI.jsp?id="+mc.getT_HerramientaCI_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� MEDICION CI: " +e.getMessage());
		}
	}

}
