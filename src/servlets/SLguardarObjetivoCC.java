package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTObjetivoCC;
import entidades.ObjetivoCC;

/**
 * Servlet implementation class SLguardarObjetivoCC
 */
@WebServlet("/SLguardarObjetivoCC")
public class SLguardarObjetivoCC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarObjetivoCC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


		try 
		{
			DTObjetivoCC dtoc = new DTObjetivoCC();
			ObjetivoCC oc = new ObjetivoCC();
			
			String t_HerramientaCC_ID, t_Categoria_ID, nombre;
			Boolean estado;
			
			t_HerramientaCC_ID = request.getParameter("T_HerramientaCC_ID");
			t_Categoria_ID = request.getParameter("T_Categoria_ID");
			nombre = request.getParameter("Nombre");
			estado = true;
			
			oc.setT_HerramientaCC_ID(Integer.valueOf(t_HerramientaCC_ID));
			oc.setT_Categoria_ID(Integer.valueOf(t_Categoria_ID));
			oc.setNombre(nombre);
			oc.setEstado(estado);
			
			if(dtoc.guardarObjetivoCC(oc))
			{
				response.sendRedirect("view/herramientaCC/detalleHerramientaCC.jsp?id="+oc.getT_HerramientaCC_ID());
			}
			else
			{
				response.sendRedirect("herramientaCC/detalleHerramientaCC.jsp?id="+oc.getT_HerramientaCC_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� OBJETIVO CC: " +e.getMessage());
		}
		
		
	}
	

}
