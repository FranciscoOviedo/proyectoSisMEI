package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTHerramientaCI;
import entidades.HerramientaCI;

/**
 * Servlet implementation class SLguardarHerramientaCI
 */
@WebServlet("/SLguardarHerramientaCI")
public class SLguardarHerramientaCI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarHerramientaCI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTHerramientaCI dhc = new DTHerramientaCI();
			HerramientaCI hc = new HerramientaCI();
			
			String t_FamiliaKolping_ID, t_MicroProyecto_ID;
			Boolean estado;
			
			//Para trabajar con fechas
			Date fechaCreacion;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_FamiliaKolping_ID = request.getParameter("T_FamiliaKolping_ID");
			t_MicroProyecto_ID = request.getParameter("T_MicroProyecto_ID");
			fechaCreacion = format.parse(request.getParameter("FechaCreacion"));
			
			
			System.out.println(t_FamiliaKolping_ID);
			System.out.println(t_MicroProyecto_ID);
			estado = true;
			
			hc.setT_FamiliaKolping_ID(Integer.valueOf(t_FamiliaKolping_ID));
			hc.setT_MicroProyecto_ID(Integer.valueOf(t_MicroProyecto_ID));
			hc.setFechaCreacion(fechaCreacion);
			hc.setEstado(estado);
			
			if(dhc.guardarHerramientaCI(hc))
			{
				response.sendRedirect("view/herramientaCI/listaHerramientaCI.jsp");
			}
			else
			{
				response.sendRedirect("view/herramientaCI/listaHerramientaCI.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� HERRAMIENTA CI: " +e.getMessage());
		}
	}

}
