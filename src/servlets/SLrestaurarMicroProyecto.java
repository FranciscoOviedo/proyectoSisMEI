package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMicroProyecto;
import entidades.MicroProyecto;

/**
 * Servlet implementation class SLrestaurarMicroProyecto
 */
@WebServlet("/SLrestaurarMicroProyecto")
public class SLrestaurarMicroProyecto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarMicroProyecto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idMicroProyecto = "";
		idMicroProyecto = request.getParameter("id");
		idMicroProyecto = idMicroProyecto==null?"0":idMicroProyecto;
		System.out.println(idMicroProyecto);
		try 
		{
			DTMicroProyecto dtmicro = new DTMicroProyecto();
			MicroProyecto micro = new MicroProyecto();
			micro.setT_MicroProyecto_ID(Integer.parseInt(idMicroProyecto));
			
			System.out.println(micro.getT_MicroProyecto_ID());
			
			if(dtmicro.restaurarMicroProyecto(micro))
			{
				response.sendRedirect("view/proyectos/listaMicroProyecto.jsp");
			}
			else
			{
				response.sendRedirect("view/proyectos/listaMicroProyecto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
