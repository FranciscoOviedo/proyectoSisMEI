package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroProyecto;
import entidades.MacroProyecto;

/**
 * Servlet implementation class SLrestaurarMacroProyecto
 */
@WebServlet("/SLrestaurarMacroProyecto")
public class SLrestaurarMacroProyecto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarMacroProyecto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idMacroProyecto = "";
		idMacroProyecto = request.getParameter("id");
		idMacroProyecto = idMacroProyecto==null?"0":idMacroProyecto;
		try 
		{
			DTMacroProyecto dtmacro = new DTMacroProyecto();
			MacroProyecto macro = new MacroProyecto();
			macro.setT_MacroProyecto_ID(Integer.parseInt(idMacroProyecto));
			
			System.out.println(macro.getT_MacroProyecto_ID());
			
			if(dtmacro.restaurarMacroProyecto(macro))
			{
				response.sendRedirect("view/proyectos/listaMacroProyecto.jsp");
			}
			else
			{
				response.sendRedirect("view/proyectos/listaMacroProyecto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
