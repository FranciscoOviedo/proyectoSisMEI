package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import entidades.Miembro;
import datos.DTMiembro;

/**
 * Servlet implementation class SLrestaurarMiembro
 */
@WebServlet("/SLrestaurarMiembro")
public class SLrestaurarMiembro extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLrestaurarMiembro() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idMiembro = "";
		idMiembro = request.getParameter("id");
		idMiembro = idMiembro==null?"0":idMiembro;
		try 
		{
			DTMiembro dtm= new DTMiembro();
			Miembro m = new Miembro();
			m.setT_Miembro_ID(Integer.parseInt(idMiembro));
			
			if(dtm.restaurarMiembro(m))
			{
				response.sendRedirect("view/miembros/listaMiembro.jsp");
			}
			else
			{
				response.sendRedirect("view/miembros/listaMiembro.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE RESTAURO: " +e.getMessage());
		}
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
