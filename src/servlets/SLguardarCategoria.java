package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTCategoria;
import entidades.Categoria;

/**
 * Servlet implementation class SLguardarCategoria
 */
@WebServlet("/SLguardarCategoria")
public class SLguardarCategoria extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarCategoria() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTCategoria dtc = new DTCategoria();
			Categoria c = new Categoria();
			
			String nombre, objetivo;
			Boolean estado;
			
			nombre = request.getParameter("Nombre");
			objetivo = request.getParameter("Objetivo");
			estado = true;
			
			c.setNombre(nombre);
			c.setObjetivo(objetivo);
			c.setEstado(estado);
			
			if(dtc.guardarCategoria(c))
			{
				response.sendRedirect("view/categoriaCICC/listaCategoriaCICC.jsp");
			}
			else
			{
				response.sendRedirect("view/categoriaCICC/crearCategoria.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� ROL: " +e.getMessage());
		}
	}

}
