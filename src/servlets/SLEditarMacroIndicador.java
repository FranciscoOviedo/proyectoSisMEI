package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroIndicador;
import entidades.MacroIndicador;

/**
 * Servlet implementation class SLeditarIndicador
 */
@WebServlet("/SLeditarMacroIndicador")
public class SLEditarMacroIndicador extends HttpServlet{
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLEditarMacroIndicador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idMacroIndicador = "";
		idMacroIndicador = request.getParameter("id");
		idMacroIndicador = idMacroIndicador==null?"0":idMacroIndicador ;

		String t_MacroIndicador_ID, t_MacroObjetivo_ID, definicion, tipoMedicion, ID_User_Modifica ;
		int estado;
		
		try 
		{
			DTMacroIndicador dtind = new DTMacroIndicador();
			MacroIndicador ind = new MacroIndicador();
			
			t_MacroIndicador_ID = request.getParameter("T_MacroIndicador_ID");
			t_MacroObjetivo_ID = request.getParameter("T_MacroObjetivo_ID");
			definicion = request.getParameter("Definicion");
			tipoMedicion = request.getParameter("TipoMedicion");
			ID_User_Modifica = "1"; //TODO: Por ahora solo se pueden modificar MacroIndicadores con el primer usuario en la base de datos
			
			ind.setT_MacroObjetivo_ID(Integer.valueOf(t_MacroObjetivo_ID));
			
			ind.setT_MacroIndicador_ID(Integer.valueOf(t_MacroIndicador_ID));
			ind.setDefinicion(definicion);
			ind.setTipoMedicion(tipoMedicion);
			ind.setEstado(1);
			ind.setFecha_Registra(new Timestamp(new Date().getTime()));
			
			if(dtind.modificarMacroIndicador(ind))
			{
				response.sendRedirect("view/proyectos/editarMacroProyecto.jsp");
			}
			else
			{
				response.sendRedirect("view/proyectos/editarMacroProyecto.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}
}
