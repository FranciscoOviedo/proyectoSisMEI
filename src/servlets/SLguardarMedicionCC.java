package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMedicionCC;
import entidades.MedicionCC;

/**
 * Servlet implementation class SLguardarMedicionCC
 */
@WebServlet("/SLguardarMedicionCC")
public class SLguardarMedicionCC extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMedicionCC() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMedicionCC dtmc = new DTMedicionCC();
			MedicionCC mc = new MedicionCC();
			
			String t_HerramientaCC_ID;
			Boolean estado;
		System.out.println("Probando");
			//Para trabajar con fechas
			Date fecha;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_HerramientaCC_ID = request.getParameter("T_HerramientaCC_ID");
			fecha = format.parse(request.getParameter("Fecha"));
			estado = true;
			
			mc.setT_HerramientaCC_ID(Integer.valueOf(t_HerramientaCC_ID));
			mc.setFecha(fecha);
			mc.setEstado(estado);
			
			if(dtmc.guardarMedicionCC(mc))
			{
				response.sendRedirect("view/herramientaCC/detalleHerramientaCC.jsp?id="+mc.getT_HerramientaCC_ID());
			}
			else
			{
				response.sendRedirect("view/herramientaCC/detalleHerramientaCC.jsp?id="+mc.getT_HerramientaCC_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� MEDICION CC: " +e.getMessage());
		}
	}

}
