package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTCargo;
import entidades.Cargo;

/**
 * Servlet implementation class SLeditarCargo
 */
@WebServlet("/SLeditarCargo")
public class SLeditarCargo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarCargo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String t_Cargo_ID = "";
		t_Cargo_ID = request.getParameter("T_Cargo_ID");
		t_Cargo_ID = t_Cargo_ID==null?"0":t_Cargo_ID;

		String nombre;
		Boolean estado;
		
		
		try 
		{
			DTCargo dtcar = new DTCargo();
			Cargo car = new Cargo();
			car.setT_Cargo_ID(Integer.parseInt(t_Cargo_ID));
			
			
			
			nombre = request.getParameter("Nombre");
			estado = true;
			
			car.setNombre(nombre);
			car.setEstado(estado);
			
			if(dtcar.modificarCargo(car))
			{
				response.sendRedirect("view/cargos/listaCargo.jsp");
			}
			else
			{
				response.sendRedirect("view/cargos/crearCargo.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}