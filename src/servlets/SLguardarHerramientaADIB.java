package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTHerramientaADIB;
import entidades.HerramientaADIB;

/**
 * Servlet implementation class SLguardarHerramientaADIB
 */
@WebServlet("/SLguardarHerramientaADIB")
public class SLguardarHerramientaADIB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarHerramientaADIB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTHerramientaADIB dha = new DTHerramientaADIB();
			HerramientaADIB ha = new HerramientaADIB();
			
			String t_FamiliaKolping_ID, t_MacroProyecto_ID;
			Boolean estado;
			
			//Para trabajar con fechas
			Date fechaCreacion;
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			
			t_FamiliaKolping_ID = request.getParameter("T_FamiliaKolping_ID");
			t_MacroProyecto_ID = request.getParameter("T_MacroProyecto_ID");
			fechaCreacion = format.parse(request.getParameter("FechaCreacion"));
			
			
			System.out.println(t_FamiliaKolping_ID);
			System.out.println(t_MacroProyecto_ID);

			System.out.println(fechaCreacion);
			estado = true;
			
			ha.setT_FamiliaKolping_ID(Integer.valueOf(t_FamiliaKolping_ID));
			ha.setT_MacroProyecto_ID(Integer.valueOf(t_MacroProyecto_ID));
			ha.setFechaCreacion(fechaCreacion);
			ha.setEstado(estado);
			
			if(dha.guardarHerramientaADIB(ha))
			{
				response.sendRedirect("view/herramientaADIB/listaHerramientaADIB.jsp");
			}
			else
			{
				response.sendRedirect("view/herramientaADIB/listaHerramientaADIB.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� HERRAMIENTA ADIB: " +e.getMessage());
		}
	}

}
