package servlets;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTDiocesana;
import entidades.Diocesana;

/**
 * Servlet implementation class SLeditarDiocesana
 */
@WebServlet("/SLeditarDiocesana")
public class SLeditarDiocesana extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarDiocesana() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String t_Diocesana_ID = "";
		t_Diocesana_ID = request.getParameter("T_Diocesana_ID");
		t_Diocesana_ID = t_Diocesana_ID==null?"0":t_Diocesana_ID;

		String nombre;
		Boolean estado;
		
		
		try 
		{
			DTDiocesana dtdio = new DTDiocesana();
		    Diocesana dio = new Diocesana();
		    
			dio.setT_Diocesana_ID(Integer.parseInt(t_Diocesana_ID));
			
			
			
			nombre = request.getParameter("Nombre");
			estado = true;
			
			dio.setT_Diocesana_ID(Integer.valueOf(t_Diocesana_ID));
			dio.setNombre(nombre);
			dio.setEstado(estado);
			
			if(dtdio.modificarDiocesana(dio))
			{
				response.sendRedirect("view/Familias/listadoDiocesanas.jsp");
			}
			else
			{
				response.sendRedirect("view/Familias/crearDiocesana.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}