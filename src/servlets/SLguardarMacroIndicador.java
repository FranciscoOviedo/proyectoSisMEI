package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTMacroIndicador;
import entidades.MacroIndicador;

/**
 * Servlet implementation class SLguardarMacroIndicador
 */
@WebServlet("/SLguardarMacroIndicador")
public class SLguardarMacroIndicador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarMacroIndicador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTMacroIndicador dti = new DTMacroIndicador();
			MacroIndicador i = new MacroIndicador();
			
			String t_MacroObjetivo_ID, definicion, ID_User_Registra;
			int estado;
			
			t_MacroObjetivo_ID = request.getParameter("T_MacroObjetivo_ID");
			definicion = request.getParameter("Definicion");
			estado = 0;
			ID_User_Registra = "1"; //TODO: Por ahora solo se pueden crear Macroindicadores con el primer usuario en la base de datos
			
			i.setT_MacroObjetivo_ID(Integer.valueOf(t_MacroObjetivo_ID));
			i.setDefinicion(definicion);
			i.setEstado(estado);
			i.setFecha_Registra(new Timestamp(new Date().getTime()));
			i.setID_User_Registra(Integer.valueOf(ID_User_Registra));
			
			if(dti.guardarMacroIndicador(i))
			{
				response.sendRedirect("view/proyectos/detalleMacroObjetivo.jsp?id="+i.getT_MacroObjetivo_ID());
			}
			else
			{
				response.sendRedirect("view/proyectos/detalleMacroObjetivo.jsp?id="+i.getT_MacroObjetivo_ID());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� INDICADOR: " +e.getMessage());
		}
	}

}
