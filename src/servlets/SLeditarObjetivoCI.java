package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTObjetivoCI;
import entidades.ObjetivoCI;


/**
 * Servlet implementation class SLeditarObjetivoCI
 */
@WebServlet("/SLeditarObjetivoCI")
public class SLeditarObjetivoCI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarObjetivoCI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String t_ObjetivoCI_ID = "";
		t_ObjetivoCI_ID = request.getParameter("T_ObjetivoCI_ID");
		t_ObjetivoCI_ID = t_ObjetivoCI_ID==null?"0":t_ObjetivoCI_ID;

		String t_HerramientaCI_ID, t_Categoria_ID, nombre, criterio1, criterio2, criterio3, criterio4, criterio5;
		Boolean estado;
		
		try 
		{
			DTObjetivoCI dtoc = new DTObjetivoCI();
			ObjetivoCI oc = new ObjetivoCI();
			
			t_HerramientaCI_ID = request.getParameter("T_HerramientaCI_ID");
			t_Categoria_ID = request.getParameter("T_Categoria_ID");
			nombre = request.getParameter("Nombre");
			criterio1 = request.getParameter("Criterio1");
			criterio2 = request.getParameter("Criterio2");
			criterio3 = request.getParameter("Criterio3");
			criterio4 = request.getParameter("Criterio4");
			criterio5 = request.getParameter("Criterio5");
			estado = true;
			
			oc.setT_ObjetivoCI_ID(Integer.valueOf(t_ObjetivoCI_ID));
			oc.setT_HerramientaCI_ID(Integer.valueOf(t_HerramientaCI_ID));
			oc.setT_Categoria_ID(Integer.valueOf(t_Categoria_ID));
			oc.setNombre(nombre);
			oc.setCriterio1(criterio1);
			oc.setCriterio2(criterio2);
			oc.setCriterio3(criterio3);
			oc.setCriterio4(criterio4);
			oc.setCriterio5(criterio5);
			oc.setEstado(estado);
			
			if(dtoc.modificarObjetivoCI(oc))
			{
				response.sendRedirect("view/herramientaCI/detalleHerramientaCI.jsp?id="+oc.getT_HerramientaCI_ID());
			}
			else
			{
				response.sendRedirect("view/herramientaCI/detalleHerramientaCI.jsp?id="+oc.getT_HerramientaCI_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
