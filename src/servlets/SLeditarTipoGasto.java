package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTTipoGasto;
import entidades.TipoGasto;

/**
 * Servlet implementation class SLeditarTipoGasto
 */
@WebServlet("/SLeditarTipoGasto")
public class SLeditarTipoGasto extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLeditarTipoGasto() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String t_TipoGasto_ID = "";
		t_TipoGasto_ID = request.getParameter("T_TipoGasto_ID");
		t_TipoGasto_ID = t_TipoGasto_ID==null?"0":t_TipoGasto_ID;

		String nombre;
		Boolean estado;
		//Para trabajar con fechas
		try 
		{
			DTTipoGasto dttg = new DTTipoGasto();
			TipoGasto tg = new TipoGasto();
			
			nombre = request.getParameter("Nombre");
			estado = true;
			
			tg.setT_TipoGasto_ID(Integer.valueOf(t_TipoGasto_ID));
			tg.setNombre(nombre);
			tg.setEstado(estado);
			
			if(dttg.modificarTipoGasto(tg))
			{
				response.sendRedirect("view/categoriaCICC/listaCategoriaCICC.jsp");
			}
			else
			{
				response.sendRedirect("view/categoriaCICC/editarCategoria.jsp?error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE MODIFIC�: " +e.getMessage());
		}
	}

}
