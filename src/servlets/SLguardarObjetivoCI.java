package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datos.DTObjetivoCI;
import entidades.ObjetivoCI;

/**
 * Servlet implementation class SLguardarObjetivoCI
 */
@WebServlet("/SLguardarObjetivoCI")
public class SLguardarObjetivoCI extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SLguardarObjetivoCI() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try 
		{
			DTObjetivoCI dtoc = new DTObjetivoCI();
			ObjetivoCI oc = new ObjetivoCI();
			
			String t_HerramientaCI_ID, t_Categoria_ID, nombre;
			Boolean estado;
			
			t_HerramientaCI_ID = request.getParameter("T_HerramientaCI_ID");
			t_Categoria_ID = request.getParameter("T_Categoria_ID");
			nombre = request.getParameter("Nombre");
			estado = true;
			
			oc.setT_HerramientaCI_ID(Integer.valueOf(t_HerramientaCI_ID));
			oc.setT_Categoria_ID(Integer.valueOf(t_Categoria_ID));
			oc.setNombre(nombre);
			oc.setEstado(estado);
			
			if(dtoc.guardarObjetivoCI(oc))
			{
				response.sendRedirect("view/herramientaCI/detalleHerramientaCI.jsp?id="+oc.getT_HerramientaCI_ID());
			}
			else
			{
				response.sendRedirect("herramientaCI/detalleHerramientaCI.jsp?id="+oc.getT_HerramientaCI_ID()+"&error");
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.err.println("SL ERROR: NO SE GUARD� OBJETIVO CI: " +e.getMessage());
		}
	}

}
