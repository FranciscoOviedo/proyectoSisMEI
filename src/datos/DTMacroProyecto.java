package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.MacroProyecto;
import entidades.MicroProyecto;

public class DTMacroProyecto {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<MacroProyecto> listaMacroProyectoInactivos()
	{
		ArrayList<MacroProyecto> macroProyecto = new ArrayList<MacroProyecto>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MacroProyecto where estado = 3");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroProyecto macro = new MacroProyecto();
				macro.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				macro.setNombre(rs.getString("Nombre"));
				macro.setObjetivoGeneral(rs.getString("ObjetivoGeneral"));
				macro.setFechaInicio(rs.getDate("FechaInicio"));
				macro.setFechaFin(rs.getDate("FechaFin"));
				macro.setEstado(rs.getInt("Estado"));
				macro.setID_User_Registra(rs.getInt("ID_User_Registra"));
				macro.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				macro.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				macro.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				macro.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				macro.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				macroProyecto.add(macro);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MACROPROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return macroProyecto;
	}
	
	public ArrayList<MacroProyecto> listarMacroProyecto()
	{
		ArrayList<MacroProyecto> macroProyecto = new ArrayList<MacroProyecto>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MacroProyecto where estado <> 3");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroProyecto macro = new MacroProyecto();
				macro.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				macro.setNombre(rs.getString("Nombre"));
				macro.setObjetivoGeneral(rs.getString("ObjetivoGeneral"));
				macro.setFechaInicio(rs.getDate("FechaInicio"));
				macro.setFechaFin(rs.getDate("FechaFin"));
				macro.setEstado(rs.getInt("Estado"));
				macro.setID_User_Registra(rs.getInt("ID_User_Registra"));
				macro.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				macro.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				macro.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				macro.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				macro.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				macroProyecto.add(macro);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MACROPROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return macroProyecto;
	}
	
	public boolean guardarMacroProyecto(MacroProyecto macro)
	{
		boolean guardado = false;
		try 
		{
			this.listarMacroProyecto();
			rs.moveToInsertRow();
			rs.updateString("Nombre", macro.getNombre());
			rs.updateString("ObjetivoGeneral", macro.getObjetivoGeneral());
			rs.updateDate("FechaInicio", new java.sql.Date(macro.getFechaInicio().getTime()));
			rs.updateDate("FechaFin", new java.sql.Date(macro.getFechaFin().getTime()));
			rs.updateInt("Estado", macro.getEstado());
			rs.updateInt("ID_User_Registra", macro.getID_User_Registra());
			rs.updateTimestamp("Fecha_Registra",macro.getFecha_Registra());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MACROPROYECTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMacroProyecto(MacroProyecto macro) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_MacroProyecto set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MacroProyecto set Estado = 3, ID_User_Elimina=?, Fecha_Elimina=? where T_MacroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, macro.getID_User_Elimina());
			ps.setTimestamp(2, macro.getFecha_Elimina());
			ps.setInt(3, macro.getT_MacroProyecto_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR MACROPROYECTO"+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarMacroProyecto(MacroProyecto macro) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MacroProyecto set estado = 1 where T_MacroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, macro.getT_MacroProyecto_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMacroProyecto(MacroProyecto macro)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_MacroProyecto set estado = 2, .......");
		String sql = ("UPDATE T_MacroProyecto set Estado = 2, Nombre = ?, ObjetivoGeneral=?, FechaInicio=?, FechaFin = ?, Estado=?, ID_User_Modifica=?, Fecha_Modifica=? where T_MicroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, macro.getNombre());
			ps.setString(	2, macro.getObjetivoGeneral());
			ps.setDate(		3, new java.sql.Date(macro.getFechaInicio().getTime()));
			ps.setDate(		4, new java.sql.Date(macro.getFechaFin().getTime()));
			ps.setInt(		5, macro.getEstado());
			ps.setInt(		6, macro.getID_User_Modifica());
			ps.setTimestamp(7, macro.getFecha_Modifica());
			ps.setInt(		8, macro.getT_MacroProyecto_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR MACROPROYECTO "+e.getMessage());
			
		}
		return modificado;
	}
}
