package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Categoria;

public class DTCategoria {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Categoria> listarCategoriasInactivas()
	{
		ArrayList<Categoria> categorias = new ArrayList<Categoria>();
		String sql = ("SELECT * FROM T_Categoria where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Categoria tc = new Categoria();
				tc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				tc.setNombre(rs.getString("Nombre"));
				tc.setObjetivo(rs.getString("Objetivo"));
				categorias.add(tc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS TIPOS DE GASTO "+ e.getMessage());
			e.printStackTrace();
		}
		return categorias;
	}
	
	public ArrayList<Categoria> listarCategorias()
	{
		ArrayList<Categoria> categorias = new ArrayList<Categoria>();
		String sql = ("SELECT * FROM T_Categoria where estado = 1");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Categoria tc = new Categoria();
				tc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				tc.setNombre(rs.getString("Nombre"));
				tc.setObjetivo(rs.getString("Objetivo"));
				categorias.add(tc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS TIPOS DE GASTO "+ e.getMessage());
			e.printStackTrace();
		}
		return categorias;
	}
	
	public boolean guardarCategoria(Categoria tc)
	{
		boolean guardado = false;
		try 
		{
			this.listarCategorias();
			rs.moveToInsertRow();
			rs.updateString("Nombre", tc.getNombre());
			rs.updateString("Objetivo", tc.getObjetivo());
			rs.updateBoolean("Estado", tc.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR TIPO DE GASTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarCategoria(Categoria tc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_Categoria set estado = 0 where T_Categoria_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, tc.getT_Categoria_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarTipoGasto(Categoria tc) throws SQLException
	{
		boolean restaurado = false;
		PreparedStatement ps;
		String sql = ("Update T_Categoria set estado = 1 where T_Categoria_ID = ?");
		try 
		
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, tc.getT_Categoria_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarTipoGasto(Categoria tc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_Categoria set Nombre=?, Objetivo=? where T_Categoria_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, tc.getNombre());
			ps.setString(	2, tc.getObjetivo());
			ps.setInt(		3, tc.getT_Categoria_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
