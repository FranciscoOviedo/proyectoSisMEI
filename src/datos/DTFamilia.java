package datos;

	import java.sql.Connection;
	import java.sql.PreparedStatement;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.util.ArrayList;

	import entidades.Familia;
import entidades.Formador;
	public class DTFamilia
	{
		Conexion c = Conexion.getInstance();
		Connection cn = Conexion.getConnection();
		ResultSet rs = null;
		public ArrayList<Familia> listarFamiliasInactivas()
		{
			ArrayList<Familia> familias = new ArrayList<Familia>();
			//Si su tabla tiene auitoria usar: where estado <> 3
			//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
			String sql = ("SELECT * FROM T_FamiliaKolping where estado = 0");
			
			try 
			{
				PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
				rs = ps.executeQuery();
				
				while(rs.next())
				{
					Familia fa = new Familia();
					fa.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
					fa.setT_Diocesana_ID(rs.getInt("T_Diocesana_ID"));
					fa.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
					fa.setNombre(rs.getString("Nombre"));
					fa.setDistancia(rs.getInt("Distancia"));
					fa.setTiempoViaje(rs.getFloat("TiempoViaje"));
					fa.setTipo(rs.getString("Tipo"));
					
					
					familias.add(fa);
				}
			} 
			catch (Exception e) 
			{
				System.out.println("DATOS: ERROR AL OBTENER LOS FAMILIAS "+ e.getMessage());
				e.printStackTrace();
			}
			return familias;
		}
		
		public ArrayList<Familia> listarFamilias()
		{
			ArrayList<Familia> familias = new ArrayList<Familia>();
			//Si su tabla tiene auitoria usar: where estado <> 3
			//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
			String sql = ("SELECT * FROM T_FamiliaKolping where estado <> 0");
			
			try 
			{
				PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
				rs = ps.executeQuery();
				
				while(rs.next())
					
				{
					
					Familia fa = new Familia();
					fa.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
					fa.setT_Diocesana_ID(rs.getInt("T_Diocesana_ID"));
					fa.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
					fa.setNombre(rs.getString("Nombre"));
					fa.setDistancia(rs.getInt("Distancia"));
					fa.setTiempoViaje(rs.getFloat("TiempoViaje"));
					fa.setTipo(rs.getString("Tipo"));
					familias.add(fa);
				}
			} 
			catch (Exception e) 
			{
				System.out.println("DATOS: ERROR AL OBTENER LAS FAMILIAS "+ e.getMessage());
				e.printStackTrace();
			}
			return familias;
		}
		
		public boolean guardarFamilia(Familia fa)
		{
			boolean guardado = false;
			try 
			{
				this.listarFamilias();
				rs.moveToInsertRow();
				if(fa.getT_Diocesana_ID()==0){
					rs.updateNull("T_Diocesana_ID");
				}else{
					rs.updateInt("T_Diocesana_ID", fa.getT_Diocesana_ID());
				}
				
			   
				//System.out.println(fa.getT_Diocesana_ID());
				//System.out.println(fa.getT_Municipio_ID());
				//System.out.println(fa.getNombre());
				//rs.updateInt("T_Diocesana_ID", fa.getT_Diocesana_ID());
				rs.updateInt("T_Municipio_ID", fa.getT_Municipio_ID());
				rs.updateString("Nombre", fa.getNombre());
				rs.updateInt("Distancia", fa.getDistancia());
				rs.updateFloat("TiempoViaje", fa.getTiempoViaje());
				rs.updateString("Tipo", fa.getTipo());
				rs.updateBoolean("Estado", fa.isEstado());
				rs.insertRow();
				rs.moveToCurrentRow();
				guardado = true;
			} 
			catch (Exception e) 
			{
				System.err.println("ERROR AL GUARDAR FAMILIA "+e.getMessage());
				e.printStackTrace();
			}
			
			return guardado;
		}
		
		public boolean eliminarFamilia(Familia fa) throws SQLException
		{
			/*
			 * Estados para tablas con auditoria
			 * 1 = agregado
			 * 2 = modificado
			 * 3 = eliminado
			 *  */
			boolean eliminado = false;
			PreparedStatement ps;
			//Si su tabla tiene auditoria usar: set estado = 3
			//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
			String sql = ("Update T_FamiliaKolping set estado = 0 where T_FamiliaKolping_ID = ?");
			try 
			{
				ps = cn.prepareStatement(sql);
				ps.setInt(1, fa.getT_FamiliaKolping_ID());
				ps.executeUpdate();
				eliminado = true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.err.println("ERROR AL ELIMINAR "+e.getMessage());
			}
			return eliminado;
		}
		
		public boolean restaurarFamilia(Familia fa) throws SQLException
		{
			/*
			 * Estados para tablas con auditoria
			 * 1 = agregado
			 * 2 = modificado
			 * 3 = eliminado
			 *  */
			boolean restaurado = false;
			PreparedStatement ps;
			//Si su tabla tiene auditoria usar: set estado = 3
			//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
			String sql = ("Update T_FamiliaKolping set estado = 1 where T_FamiliaKolping_ID = ?");
			try 
			{
				ps = cn.prepareStatement(sql);
				ps.setInt(1, fa.getT_FamiliaKolping_ID());
				ps.executeUpdate();
				restaurado = true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.err.println("ERROR AL RESTAURAR "+e.getMessage());
			}
			return restaurado;
		}
		
		
		public boolean modificarFamilia(Familia fa)
		{
			boolean modificado = false;
			PreparedStatement ps;
			//Para los que tengan auditoria tienen que agregar set estado = 2
			//String sql = ("UPDATE T_Formador set estado = 2, .......");
			String sql = ("UPDATE T_FamiliaKolping set T_Diocesana_ID=?, T_Municipio_ID=?,Nombre = ?, Distancia=?, TiempoViaje = ?, Tipo=?  where T_FamiliaKolping_ID = ?");
			try 
			{
				
				ps = cn.prepareStatement(sql);
				if(fa.getT_Diocesana_ID()==0){
					ps.setNull(		1, java.sql.Types.INTEGER);
				}else{
					ps.setInt(		1, fa.getT_Diocesana_ID());
				}
				//ps = cn.prepareStatement(sql);
				//ps.setInt(		1, fa.getT_Diocesana_ID());
				ps.setInt(		2, fa.getT_Municipio_ID());
				ps.setString(	3, fa.getNombre());
				ps.setInt(	    4, fa.getDistancia());
				ps.setFloat(    5, fa.getTiempoViaje());
				ps.setString(   6, fa.getTipo());
				ps.setInt(		7, fa.getT_FamiliaKolping_ID());
				ps.executeUpdate();
				modificado = true;
				
				
				System.out.println(fa.getNombre());
				System.out.println(fa.getDistancia());
				System.out.println(fa.getTiempoViaje());
				System.out.println(fa.getTipo());
				
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.err.println("ERROR AL MODIFICAR "+e.getMessage());
				
			}
			return modificado;
		}
	}


