package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Departamento;

public class DTDepartamento {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Departamento> listarDepartamentos()
	{
		ArrayList<Departamento> departamentos = new ArrayList<Departamento>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Departamento where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Departamento d = new Departamento();
				d.setT_Departamento_ID(rs.getInt("T_Departamento_ID"));
				d.setNombre(rs.getString("Nombre"));
				d.setEstado(rs.getBoolean("Estado"));
				departamentos.add(d);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS DEPARTAMENTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return departamentos;
	}
	
	public boolean guardarDepartamento(Departamento d)
	{
		boolean guardado = false;
		try 
		{
			this.listarDepartamentos();
			rs.moveToInsertRow();
			rs.updateString("Nombre", d.getNombre());
			rs.updateBoolean("Estado", d.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR DEPARTAMENTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarDepartamento(Departamento d) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Departamento set estado = 0 where T_Departamento_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, d.getT_Departamento_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarDepartamento(Departamento d)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_Departamento set Nombre=?, Estado = ? where T_Departamento_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, d.getNombre());
			ps.setBoolean(	2, d.isEstado());
			ps.setInt(		3, d.getT_Departamento_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
