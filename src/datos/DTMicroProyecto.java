package datos;

import entidades.Formador;
import entidades.MacroProyecto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.MicroProyecto;

public class DTMicroProyecto {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<MicroProyecto> listaMicroProyectoInactivos()
	{
		ArrayList<MicroProyecto> microProyecto = new ArrayList<MicroProyecto>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MicroProyecto where estado = 3");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MicroProyecto micro = new MicroProyecto();
				micro.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				micro.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				micro.setNombre(rs.getString("Nombre"));
				micro.setObjetivoGeneral(rs.getString("ObjetivoGeneral"));
				micro.setFechaInicio(rs.getDate("FechaInicio"));
				micro.setFechaFin(rs.getDate("FechaFin"));
				micro.setTipo(rs.getString("Tipo"));				
				micro.setEstado(rs.getInt("Estado"));
				micro.setID_User_Registra(rs.getInt("ID_User_Registra"));
				micro.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				micro.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				micro.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				micro.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				micro.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				microProyecto.add(micro);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MICROPROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return microProyecto;
	}
	
	public ArrayList<MicroProyecto> listaMicroProyecto()
	{
		ArrayList<MicroProyecto> microProyecto = new ArrayList<MicroProyecto>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MicroProyecto where estado <> 3");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MicroProyecto micro = new MicroProyecto();
				micro.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				micro.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				micro.setNombre(rs.getString("Nombre"));
				micro.setObjetivoGeneral(rs.getString("ObjetivoGeneral"));
				micro.setFechaInicio(rs.getDate("FechaInicio"));
				micro.setFechaFin(rs.getDate("FechaFin"));
				micro.setTipo(rs.getString("Tipo"));				
				micro.setEstado(rs.getInt("Estado"));
				micro.setID_User_Registra(rs.getInt("ID_User_Registra"));
				micro.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				micro.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				micro.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				micro.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				micro.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				microProyecto.add(micro);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MICROPROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return microProyecto;
	}
	
	public boolean guardarMicroProyecto(MicroProyecto micro)
	{
		boolean guardado = false;
		try 
		{
			this.listaMicroProyecto();
			rs.moveToInsertRow();
			rs.updateInt("T_FamiliaKolping_ID", micro.getT_FamiliaKolping_ID());
			rs.updateString("Nombre", micro.getNombre());
			rs.updateString("ObjetivoGeneral", micro.getObjetivoGeneral());
			rs.updateDate("FechaInicio", new java.sql.Date(micro.getFechaInicio().getTime()));
			rs.updateDate("FechaFin", new java.sql.Date(micro.getFechaFin().getTime()));
			rs.updateString("Tipo", "Temp"); //TODO: Averiguar que hacer con la columna tipo... si se va a dejar o no...
			rs.updateInt("Estado", micro.getEstado());
			rs.updateInt("ID_User_Registra", micro.getID_User_Registra());
			rs.updateTimestamp("Fecha_Registra",micro.getFecha_Registra());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MICROPROYECTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMicroProyecto(MicroProyecto micro) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_MicroProyecto set estado = 3 where T_MicroProyecto_ID = ?");
		String sql = ("Update T_MicroProyecto set estado = 3, ID_User_Elimina =?, Fecha_Elimina =? where T_MicroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, micro.getID_User_Elimina());
			ps.setTimestamp(2, micro.getFecha_Elimina());
			ps.setInt(3, micro.getT_MicroProyecto_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR MICROPROYECTO"+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarMicroProyecto(MicroProyecto micro) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MicroProyecto set estado = 1 where T_MicroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, micro.getT_MicroProyecto_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMicroProyecto(MicroProyecto micro)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_MacroProyecto set estado = 2, .......");
		String sql = ("UPDATE T_MacroProyecto set Nombre = ?, ObjetivoGeneral=?, FechaInicio=?, FechaFin = ?, Estado=?, ID_User_Registra = ?, ID_User_Modifica=?, ID_User_Elimina=?, FechaRegistra=?, FechaModifica=?, FechaElimina=? where T_MicroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, micro.getNombre());
			ps.setString(	2, micro.getObjetivoGeneral());
			ps.setDate(		3, new java.sql.Date(micro.getFechaInicio().getTime()));
			ps.setDate(		4, new java.sql.Date(micro.getFechaFin().getTime()));
			ps.setString(	5, micro.getTipo());
			ps.setInt(		6, micro.getEstado());
			ps.setInt(		6, micro.getID_User_Modifica());
			ps.setTimestamp(7, micro.getFecha_Modifica());
			ps.setInt(		8, micro.getT_FamiliaKolping_ID());
			ps.setInt(		9, micro.getT_MicroProyecto_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR MICROPROYECTO "+e.getMessage());
			
		}
		return modificado;
	}
}
