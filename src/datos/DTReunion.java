package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Reunion;
import vistas.V_ReunionDetalle;
public class DTReunion {
	
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Reunion> listarReuniones(){
		
		ArrayList<Reunion> reuniones = new ArrayList<Reunion>();
		
		//Si su tabla tiene auitoria usar: where estado <> 3 
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		
		String sql = ("SELECT * FROM T_Reunion where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Reunion r = new Reunion();
				r.setT_Reunion_ID(rs.getInt("T_Reunion_ID"));
				r.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				r.setT_Formador_ID(rs.getInt("T_Formador_ID"));
			    r.setT_TipoReunion_ID(rs.getInt("T_TipoReunion_ID"));
				r.setFecha(rs.getDate("Fecha"));
				r.setTema(rs.getString("Tema"));
				r.setDescripcion(rs.getString("Descripcion"));
				r.setObservacion(rs.getString("Observacion"));
				reuniones.add(r);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS REUNIONES "+ e.getMessage());
			e.printStackTrace();
		}
		return reuniones;
	}
	
public ArrayList<V_ReunionDetalle> listarReunionesDetalle(){
		
		ArrayList<V_ReunionDetalle> reunionesDetalle = new ArrayList<V_ReunionDetalle>();
		
		//Si su tabla tiene auitoria usar: where estado <> 3 
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		
		String sql = ("SELECT * FROM V_ReunionDetalle where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_ReunionDetalle rd = new V_ReunionDetalle();
				rd.setT_Reunion_ID(rs.getInt("T_Reunion_ID"));
				rd.setFecha(rs.getDate("Fecha"));
				rd.setTema(rs.getString("Tema"));
				rd.setDescripcion(rs.getString("Descripcion"));
				rd.setObservacion(rs.getString("Observacion"));
				rd.setNombreFormador(rs.getString("NombreFormador"));
				rd.setApellidoFormador(rs.getString("ApellidoFormador"));
				rd.setTipoReunion(rs.getString("TipoReunion"));
				rd.setNombreFamilia (rs.getString("NombreFamilia"));
				rd.setNombreMunicipio (rs.getString("NombreMunicipio"));
				rd.setNombreDepartamento (rs.getString("NombreDepartamento"));
				
				reunionesDetalle.add(rd);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS REUNIONES "+ e.getMessage());
			e.printStackTrace();
		}
		return reunionesDetalle;
	}
	
public ArrayList<V_ReunionDetalle> listarReunionesDetalleInactivos(){
	
	ArrayList<V_ReunionDetalle> reunionesDetalle = new ArrayList<V_ReunionDetalle>();
	
	//Si su tabla tiene auitoria usar: where estado <> 3 
	//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
	
	String sql = ("SELECT * FROM V_ReunionDetalle where estado = 0");
	
	try 
	{
		PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
		rs = ps.executeQuery();
		
		while(rs.next())
		{
			V_ReunionDetalle rd = new V_ReunionDetalle();
			rd.setT_Reunion_ID(rs.getInt("T_Reunion_ID"));
			rd.setFecha(rs.getDate("Fecha"));
			rd.setTema(rs.getString("Tema"));
			rd.setDescripcion(rs.getString("Descripcion"));
			rd.setObservacion(rs.getString("Observacion"));
			rd.setNombreFormador(rs.getString("NombreFormador"));
			rd.setApellidoFormador(rs.getString("ApellidoFormador"));
			rd.setTipoReunion(rs.getString("TipoReunion"));
			rd.setNombreFamilia (rs.getString("NombreFamilia"));
			rd.setNombreMunicipio (rs.getString("NombreMunicipio"));
			rd.setNombreDepartamento (rs.getString("NombreDepartamento"));
			
			reunionesDetalle.add(rd);
		}
	} 
	catch (Exception e) 
	{
		System.out.println("DATOS: ERROR AL OBTENER LAS REUNIONES "+ e.getMessage());
		e.printStackTrace();
	}
	return reunionesDetalle;
}

public ArrayList<Reunion> listarReunionesInactivos(){
		
		ArrayList<Reunion> reuniones = new ArrayList<Reunion>();
		
		//Si su tabla tiene auitoria usar: where estado <> 3 
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		
		String sql = ("SELECT * FROM T_Reunion where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Reunion r = new Reunion();
				r.setT_Reunion_ID(rs.getInt("T_Reunion_ID"));
				r.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				r.setT_Formador_ID(rs.getInt("T_Formador_ID"));
			    r.setT_TipoReunion_ID(rs.getInt("T_TipoReunion_ID"));
				r.setFecha(rs.getDate("Fecha"));
				r.setTema(rs.getString("Tema"));
				r.setDescripcion(rs.getString("Descripcion"));
				r.setObservacion(rs.getString("Observacion"));
				reuniones.add(r);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS REUNIONES "+ e.getMessage());
			e.printStackTrace();
		}
		return reuniones;
	}
	public boolean guardarReunion(Reunion r){
		
		boolean guardado = false;
		try 
		{
			this.listarReuniones();
			rs.moveToInsertRow();
			rs.updateInt("T_FamiliaKolping_ID", r.getT_FamiliaKolping_ID());
			rs.updateInt("T_Formador_ID", r.getT_Formador_ID());
			rs.updateInt("T_TipoReunion_ID", r.getT_TipoReunion_ID());
			rs.updateDate("Fecha", new java.sql.Date(r.getFecha().getTime()));
			rs.updateString("Tema", r.getTema());
			rs.updateString("Descripcion", r.getDescripcion());
			rs.updateString("Observacion", r.getObservacion());
			rs.updateBoolean("Estado", r.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR REUNION "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarReunion(Reunion r) throws SQLException{
		
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Reunion set estado = 0 where T_Reunion_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, r.getT_Reunion_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
public boolean restaurarReunion(Reunion r) throws SQLException{
		
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Reunion set estado = 1 where T_Reunion_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, r.getT_Reunion_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL RESTAURAR "+e.getMessage());
		}
		return restaurado;
	}
	public boolean modificarReunion(Reunion r)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_Reunion set T_FamiliaKolping_ID=?, "
					+ "T_Formador_ID = ?, "
					+ "T_TipoReunion_ID=?, "
					+ "Fecha = ?, "
					+ "Tema=?, "
					+ "Descripcion= ?, "
					+ "Observacion=?  "
					+ "where T_Reunion_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, r.getT_FamiliaKolping_ID());
			ps.setInt(		2, r.getT_Formador_ID());
			ps.setInt(		3, r.getT_TipoReunion_ID());
			ps.setDate(		4, new java.sql.Date(r.getFecha().getTime()));
			ps.setString(	5, r.getTema());
			ps.setString(	6, r.getDescripcion());
			ps.setString(	7, r.getObservacion());
			ps.setInt(		8, r.getT_Reunion_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}