package datos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Diocesana;
import entidades.Familia;


public class DTDiocesana {


		Conexion c = Conexion.getInstance();
		Connection cn = Conexion.getConnection();
		ResultSet rs = null;
		
		public ArrayList<Diocesana> listarDiocesanasInactivas()
		{
			ArrayList<Diocesana> diocesanas = new ArrayList<Diocesana>();
			//Si su tabla tiene auitoria usar: where estado <> 3
			//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
			String sql = ("SELECT * FROM T_Diocesana where estado = 0");
			
			try 
			{
				PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
				rs = ps.executeQuery();
				
				while(rs.next())
				{
					Diocesana dio = new Diocesana();
					dio.setT_Diocesana_ID(rs.getInt("T_Diocesana_ID"));
					dio.setNombre(rs.getString("Nombre"));
					
					
					diocesanas.add(dio);
				}
			} 
			catch (Exception e) 
			{
				System.out.println("DATOS: ERROR AL OBTENER LOS FAMILIAS "+ e.getMessage());
				e.printStackTrace();
			}
			return diocesanas;
		}
		
		
		public ArrayList<Diocesana> listarDiocesanas()
		{
			ArrayList<Diocesana> diocesanas= new ArrayList<Diocesana>();
			//Si su tabla tiene auitoria usar: where estado <> 3
			//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
			String sql = ("SELECT * FROM T_Diocesana where estado <> 0");
			
			try 
			{
				PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
				rs = ps.executeQuery();
				
				while(rs.next())
				{
					Diocesana dio = new Diocesana();
					dio.setT_Diocesana_ID(rs.getInt("T_Diocesana_ID"));
				    dio.setNombre(rs.getString("Nombre"));
					//dio.setEstado(rs.getBoolean("Estado"));
					diocesanas.add(dio);
				}
			} 
			catch (Exception e) 
			{
				System.out.println("DATOS: ERROR AL OBTENER LOS MUNICIPIOS "+ e.getMessage());
				e.printStackTrace();
			}
			return diocesanas;
		}
		
		public boolean guardarDiocesana(Diocesana dio)
		{
			boolean guardado = false;
			try 
			{
				this.listarDiocesanas();
				rs.moveToInsertRow();
				
				//rs.updateInt("T_Diocesanas_ID", dio.getT_Diocesana_ID());
				rs.updateString("Nombre", dio.getNombre());
				rs.updateBoolean("Estado", dio.isEstado());
				rs.insertRow();
				rs.moveToCurrentRow();
				guardado = true;
			} 
			catch (Exception e) 
			{
				System.err.println("ERROR AL GUARDAR DIOCESANA"+e.getMessage());
				e.printStackTrace();
			}
			
			return guardado;
		}
		
		public boolean eliminarDiocesana(Diocesana dio) throws SQLException
		{
			/*
			 * Estados para tablas con auditoria
			 * 1 = agregado
			 * 2 = modificado
			 * 3 = eliminado
			 *  */
			boolean eliminado = false;
			PreparedStatement ps;
			//Si su tabla tiene auditoria usar: set estado = 3
			//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
			String sql = ("Update T_Diocesana set estado = 0 where T_Diocesana_ID = ?");
			try 
			{
				ps = cn.prepareStatement(sql);
				ps.setInt(1, dio.getT_Diocesana_ID());
				ps.executeUpdate();
				eliminado = true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.err.println("ERROR AL ELIMINAR "+e.getMessage());
			}
			return eliminado;
		}
		
		
		public boolean restaurarDiocesana(Diocesana dio) throws SQLException
		{
			/*
			 * Estados para tablas con auditoria
			 * 1 = agregado
			 * 2 = modificado
			 * 3 = eliminado
			 *  */
			boolean restaurado = false;
			PreparedStatement ps;
			//Si su tabla tiene auditoria usar: set estado = 3
			//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
			String sql = ("Update T_Diocesana set estado = 1 where T_Diocesana_ID = ?");
			try 
			{
				ps = cn.prepareStatement(sql);
				ps.setInt(1, dio.getT_Diocesana_ID());
				ps.executeUpdate();
				restaurado = true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.err.println("ERROR AL RESTAURAR "+e.getMessage());
			}
			return restaurado;
		}
		
		
		public boolean modificarDiocesana(Diocesana dio)
		{
			boolean modificado = false;
			PreparedStatement ps;
			//Para los que tengan auditoria tienen que agregar set estado = 2
			//String sql = ("UPDATE T_Formador set estado = 2, .......");
			String sql = ("UPDATE T_Diocesana set  Nombre = ?, Estado=? where T_Diocesana_ID = ?");
			try 
			{
				ps = cn.prepareStatement(sql);
				ps.setString(	1, dio.getNombre());
				ps.setBoolean(	2, dio.isEstado());
				ps.setInt(		3, dio.getT_Diocesana_ID());
				ps.executeUpdate();
				modificado = true;
			} 
			catch (SQLException e) 
			{
				e.printStackTrace();
				System.err.println("ERROR AL MODIFICAR "+e.getMessage());
				
			}
			return modificado;
		}
	}


