package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.MedicionCC;

public class DTMedicionCC {

	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<MedicionCC> listarMedicionCCInactivos(int herramientaCC)
	{
		ArrayList<MedicionCC> medicionCC = new ArrayList<MedicionCC>();
		String sql = ("SELECT * FROM T_MedicionCC where estado = 0 and T_HerramientaCC_ID = " + herramientaCC);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MedicionCC mc = new MedicionCC();
				mc.setT_MedicionCC_ID(rs.getInt("T_MedicionCC_ID"));
				mc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				mc.setFecha(rs.getDate("Fecha"));
				mc.setEstado(rs.getBoolean("Estado"));
				medicionCC.add(mc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS MEDICIONES CC "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCC;
	}
	
	public ArrayList<MedicionCC> listarMedicionCC(int herramientaCC)
	{
		ArrayList<MedicionCC> medicionCC = new ArrayList<MedicionCC>();
		String sql = ("SELECT * FROM T_MedicionCC where estado <> 0 and T_HerramientaCC_ID = " + herramientaCC);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MedicionCC mc = new MedicionCC();
				mc.setT_MedicionCC_ID(rs.getInt("T_MedicionCC_ID"));
				mc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				mc.setFecha(rs.getDate("Fecha"));
				mc.setEstado(rs.getBoolean("Estado"));
				medicionCC.add(mc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS MEDICIONES CC "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCC;
	}
	
	public ArrayList<MedicionCC> listarMedicionCC()
	{
		ArrayList<MedicionCC> medicionCC = new ArrayList<MedicionCC>();
		String sql = ("SELECT * FROM T_MedicionCC where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MedicionCC mc = new MedicionCC();
				mc.setT_MedicionCC_ID(rs.getInt("T_MedicionCC_ID"));
				mc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				mc.setFecha(rs.getDate("Fecha"));
				mc.setEstado(rs.getBoolean("Estado"));
				medicionCC.add(mc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS MEDICIONES CC "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCC;
	}
	
	public boolean guardarMedicionCC(MedicionCC mc)
	{
		boolean guardado = false;
		try 
		{
			this.listarMedicionCC();
			rs.moveToInsertRow();
			rs.updateInt("T_HerramientaCC_ID", mc.getT_HerramientaCC_ID());
			rs.updateDate("Fecha", new java.sql.Date(mc.getFecha().getTime()));
			rs.updateBoolean("Estado", mc.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MEDICION CC "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMedicionCC(MedicionCC mc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_MedicionCC set estado = 0 where T_MedicionCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, mc.getT_MedicionCC_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarMedicionCC(MedicionCC mc) throws SQLException
	{
		boolean restaurado = false;
		PreparedStatement ps;
		String sql = ("Update T_MedicionCC set estado = 0 where T_MedicionCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, mc.getT_MedicionCC_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL RESTAURAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMedicionCC(MedicionCC mc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_MedicionCC set T_HerramientaCC_ID=?, Fecha=?, Estado=? where T_MedicionCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, mc.getT_HerramientaCC_ID());
			ps.setDate(		2, new java.sql.Date(mc.getFecha().getTime()));
			ps.setBoolean(	3, mc.isEstado());
			ps.setInt(		4, mc.getT_MedicionCC_ID());
			
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
