package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.HerramientaCI;
import vistas.V_Info_HerramientaCI;

public class DTHerramientaCI {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	
	public ArrayList<V_Info_HerramientaCI> listarInfoHerramientaCIInactivos()
	{
		ArrayList<V_Info_HerramientaCI> infoHerramientaCI = new ArrayList<V_Info_HerramientaCI>();
		String sql = ("SELECT * FROM V_Info_HerramientaCI where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_HerramientaCI ihc = new V_Info_HerramientaCI();
				ihc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				ihc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				ihc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				ihc.setFechaCreacion(rs.getDate("FechaCreacion"));
				ihc.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				ihc.setNombreMicroProyecto(rs.getString("NombreMicroProyecto"));
				infoHerramientaCI.add(ihc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CI "+ e.getMessage());
			e.printStackTrace();
		}
		return infoHerramientaCI;
	}
	
	public ArrayList<V_Info_HerramientaCI> listarInfoHerramientaCI()
	{
		ArrayList<V_Info_HerramientaCI> infoHerramientaCI = new ArrayList<V_Info_HerramientaCI>();
		String sql = ("SELECT * FROM V_Info_HerramientaCI where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_HerramientaCI ihc = new V_Info_HerramientaCI();
				ihc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				ihc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				ihc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				ihc.setFechaCreacion(rs.getDate("FechaCreacion"));
				ihc.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				ihc.setNombreMicroProyecto(rs.getString("NombreMicroProyecto"));
				infoHerramientaCI.add(ihc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CI "+ e.getMessage());
			e.printStackTrace();
		}
		return infoHerramientaCI;
	}
	
	
	
	public ArrayList<HerramientaCI> listarHerramientaCIInactivos()
	{
		ArrayList<HerramientaCI> herramientaCI = new ArrayList<HerramientaCI>();
		String sql = ("SELECT * FROM T_HerramientaCI where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				HerramientaCI hc = new HerramientaCI();
				hc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				hc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				hc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				hc.setFechaCreacion(rs.getDate("FechaCreacion"));
				herramientaCI.add(hc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CI "+ e.getMessage());
			e.printStackTrace();
		}
		return herramientaCI;
	}
	
	public ArrayList<HerramientaCI> listarHerramientaCI()
	{
		ArrayList<HerramientaCI> herramientaCI = new ArrayList<HerramientaCI>();
		String sql = ("SELECT * FROM T_HerramientaCI where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				HerramientaCI hc = new HerramientaCI();
				hc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				hc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				hc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				hc.setFechaCreacion(rs.getDate("FechaCreacion"));
				herramientaCI.add(hc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CI "+ e.getMessage());
			e.printStackTrace();
		}
		return herramientaCI;
	}
	
	public boolean guardarHerramientaCI(HerramientaCI hc)
	{
		boolean guardado = false;
		try 
		{
			this.listarHerramientaCI();
			rs.moveToInsertRow();
			rs.updateInt("T_FamiliaKolping_ID", hc.getT_FamiliaKolping_ID());
			rs.updateInt("T_MicroProyecto_ID", hc.getT_MicroProyecto_ID());
			rs.updateDate("FechaCreacion", new java.sql.Date(hc.getFechaCreacion().getTime()));
			rs.updateBoolean("Estado", hc.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR HERRAMIENTA CI "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarHerramientaCI(HerramientaCI hc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_HerramientaCI set estado = 0 where T_HerramientaCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, hc.getT_HerramientaCI_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarHerramientaCI(HerramientaCI hc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_HerramientaCI set estado = 1 where T_HerramientaCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, hc.getT_HerramientaCI_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarHerramientaCI(HerramientaCI hc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_HerramientaCI set T_FamiliaKolping_ID=?, T_MicroProyecto_ID=?, FechaCreacion=? where T_HerramientaCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, hc.getT_FamiliaKolping_ID());
			ps.setInt(		2, hc.getT_MicroProyecto_ID());
			ps.setDate(		3, new java.sql.Date(hc.getFechaCreacion().getTime()));
			ps.setInt(		4, hc.getT_HerramientaCI_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
