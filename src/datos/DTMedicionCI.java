package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.MedicionCI;

public class DTMedicionCI {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<MedicionCI> listarMedicionCIInactivos(int herramientaCI)
	{
		ArrayList<MedicionCI> medicionCI = new ArrayList<MedicionCI>();
		String sql = ("SELECT * FROM T_MedicionCI where estado = 0 and T_HerramientaCI_ID = " + herramientaCI);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MedicionCI mc = new MedicionCI();
				mc.setT_MedicionCI_ID(rs.getInt("T_MedicionCI_ID"));
				mc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				mc.setFecha(rs.getDate("Fecha"));
				mc.setEstado(rs.getBoolean("Estado"));
				medicionCI.add(mc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS MEDICIONES CI "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCI;
	}
	
	public ArrayList<MedicionCI> listarMedicionCI(int herramientaCI)
	{
		ArrayList<MedicionCI> medicionCI = new ArrayList<MedicionCI>();
		String sql = ("SELECT * FROM T_MedicionCI where estado <> 0 and T_HerramientaCI_ID = " + herramientaCI);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MedicionCI mc = new MedicionCI();
				mc.setT_MedicionCI_ID(rs.getInt("T_MedicionCI_ID"));
				mc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				mc.setFecha(rs.getDate("Fecha"));
				mc.setEstado(rs.getBoolean("Estado"));
				medicionCI.add(mc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS MEDICIONES CI "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCI;
	}
	
	public MedicionCI obtenerMedicionCI(int medicionCIID)
	{
		MedicionCI medicionCI = new MedicionCI();
		String sql = ("SELECT * FROM T_MedicionCI where estado <> 0 and T_MedicionCI_ID = ?");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, medicionCIID);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				medicionCI.setT_MedicionCI_ID(rs.getInt("T_MedicionCI_ID"));
				medicionCI.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				medicionCI.setFecha(rs.getDate("Fecha"));
				medicionCI.setEstado(rs.getBoolean("Estado"));
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LA MEDICION CI "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCI;
	}
	
	public ArrayList<MedicionCI> listarMedicionCI()
	{
		ArrayList<MedicionCI> medicionCI = new ArrayList<MedicionCI>();
		String sql = ("SELECT * FROM T_MedicionCI where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MedicionCI mc = new MedicionCI();
				mc.setT_MedicionCI_ID(rs.getInt("T_MedicionCI_ID"));
				mc.setT_HerramientaCI_ID(rs.getInt("T_HerramientaCI_ID"));
				mc.setFecha(rs.getDate("Fecha"));
				mc.setEstado(rs.getBoolean("Estado"));
				medicionCI.add(mc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS MEDICIONES CI "+ e.getMessage());
			e.printStackTrace();
		}
		return medicionCI;
	}
	
	public boolean guardarMedicionCI(MedicionCI mc)
	{
		boolean guardado = false;
		try 
		{
			this.listarMedicionCI();
			rs.moveToInsertRow();
			rs.updateInt("T_HerramientaCI_ID", mc.getT_HerramientaCI_ID());
			rs.updateDate("Fecha", new java.sql.Date(mc.getFecha().getTime()));
			rs.updateBoolean("Estado", mc.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MEDICION CI "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMedicionCI(MedicionCI mc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_MedicionCI set estado = 0 where T_MedicionCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, mc.getT_MedicionCI_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarMedicionCI(MedicionCI mc) throws SQLException
	{
		boolean restaurado = false;
		PreparedStatement ps;
		String sql = ("Update T_MedicionCI set estado = 0 where T_MedicionCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, mc.getT_MedicionCI_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL RESTAURAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMedicionCI(MedicionCI mc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_MedicionCI set T_HerramientaCI_ID=?, Fecha=?, Estado=? where T_MedicionCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, mc.getT_HerramientaCI_ID());
			ps.setDate(		2, new java.sql.Date(mc.getFecha().getTime()));
			ps.setBoolean(	3, mc.isEstado());
			ps.setInt(		4, mc.getT_MedicionCI_ID());
			
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
