package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Formador;

public class DTFormador {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Formador> listarFormadoresInactivos()
	{
		ArrayList<Formador> formadores = new ArrayList<Formador>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Formador where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Formador f = new Formador();
				f.setT_Formador_ID(rs.getInt("T_Formador_ID"));
				f.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				f.setNombre(rs.getString("Nombre"));
				f.setApellido(rs.getString("Apellido"));
				f.setCedula(rs.getString("Cedula"));
				f.setFechaNacimiento(rs.getDate("FechaNacimiento"));
				f.setFechaUnion(rs.getDate("FechaUnion"));
				f.setTipo(rs.getString("Tipo"));
				f.setSexo(rs.getInt("Sexo"));
				f.setTelefono(rs.getString("Telefono"));
				formadores.add(f);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS FORMADORES "+ e.getMessage());
			e.printStackTrace();
		}
		return formadores;
	}
	
	public ArrayList<Formador> listarFormadores()
	{
		ArrayList<Formador> formadores = new ArrayList<Formador>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Formador where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Formador f = new Formador();
				f.setT_Formador_ID(rs.getInt("T_Formador_ID"));
				f.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				f.setNombre(rs.getString("Nombre"));
				f.setApellido(rs.getString("Apellido"));
				f.setCedula(rs.getString("Cedula"));
				f.setFechaNacimiento(rs.getDate("FechaNacimiento"));
				f.setFechaUnion(rs.getDate("FechaUnion"));
				f.setTipo(rs.getString("Tipo"));
				f.setSexo(rs.getInt("Sexo"));
				f.setTelefono(rs.getString("Telefono"));
				formadores.add(f);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS FORMADORES "+ e.getMessage());
			e.printStackTrace();
		}
		return formadores;
	}
	
	public boolean guardarFormador(Formador f)
	{
		boolean guardado = false;
		try 
		{
			this.listarFormadores();
			rs.moveToInsertRow();
			rs.updateInt("T_Municipio_ID", f.getT_Municipio_ID());
			rs.updateString("Nombre", f.getNombre());
			rs.updateString("Apellido", f.getApellido());
			rs.updateString("Cedula", f.getCedula());
			rs.updateDate("FechaNacimiento", new java.sql.Date(f.getFechaNacimiento().getTime()));
			rs.updateDate("FechaUnion", new java.sql.Date(f.getFechaUnion().getTime()));
			rs.updateString("Tipo", f.getTipo());
			rs.updateInt("Sexo", f.getSexo());
			rs.updateString("Telefono", f.getTelefono());
			rs.updateBoolean("Estado", f.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR FORMADOR "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarFormador(Formador f) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Formador set estado = 0 where T_Formador_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, f.getT_Formador_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarFormador(Formador f) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Formador set estado = 1 where T_Formador_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, f.getT_Formador_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarFormador(Formador f)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_Formador set T_Municipio_ID=?, Nombre = ?, Apellido=?, Cedula = ?, FechaNacimiento=?, FechaUnion = ?, Tipo=?, Sexo = ?, Telefono=? where T_Formador_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, f.getT_Municipio_ID());
			ps.setString(	2, f.getNombre());
			ps.setString(	3, f.getApellido());
			ps.setString(	4, f.getCedula());
			ps.setDate(		5, new java.sql.Date(f.getFechaNacimiento().getTime()));
			ps.setDate(		6, new java.sql.Date(f.getFechaUnion().getTime()));
			ps.setString(	7, f.getTipo());
			ps.setInt(		8, f.getSexo());
			ps.setString(	9, f.getTelefono());
			ps.setInt(		10, f.getT_Formador_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}