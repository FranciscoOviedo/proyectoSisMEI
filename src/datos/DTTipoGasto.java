package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.TipoGasto;

public class DTTipoGasto {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<TipoGasto> listarTipoGastosInactivos()
	{
		ArrayList<TipoGasto> tipoGastos = new ArrayList<TipoGasto>();
		String sql = ("SELECT * FROM T_TipoGasto where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoGasto tg = new TipoGasto();
				tg.setT_TipoGasto_ID(rs.getInt("T_TipoGasto_ID"));
				tg.setNombre(rs.getString("Nombre"));
				tipoGastos.add(tg);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS TIPOS DE GASTO "+ e.getMessage());
			e.printStackTrace();
		}
		return tipoGastos;
	}
	
	public ArrayList<TipoGasto> listarTipoGastos()
	{
		ArrayList<TipoGasto> tipoGastos = new ArrayList<TipoGasto>();
		String sql = ("SELECT * FROM T_TipoGasto where estado = 1");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoGasto tg = new TipoGasto();
				tg.setT_TipoGasto_ID(rs.getInt("T_TipoGasto_ID"));
				tg.setNombre(rs.getString("Nombre"));
				tipoGastos.add(tg);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS TIPOS DE GASTO "+ e.getMessage());
			e.printStackTrace();
		}
		return tipoGastos;
	}
	
	public boolean guardarTipoGasto(TipoGasto tg)
	{
		boolean guardado = false;
		try 
		{
			this.listarTipoGastos();
			rs.moveToInsertRow();
			rs.updateString("Nombre", tg.getNombre());
			rs.updateBoolean("Estado", tg.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR TIPO DE GASTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarTipoGasto(TipoGasto tg) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_TipoGasto set estado = 0 where T_TipoGasto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, tg.getT_TipoGasto_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarTipoGasto(TipoGasto tg) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_TipoGasto set estado = 1 where T_TipoGasto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, tg.getT_TipoGasto_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarTipoGasto(TipoGasto tg)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_TipoGasto set Nombre=? where T_TipoGasto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, tg.getNombre());
			ps.setInt(		2, tg.getT_TipoGasto_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
