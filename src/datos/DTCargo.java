package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Cargo;

public class DTCargo {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Cargo> listarCargosInacivos()
	{
		ArrayList<Cargo> cargos = new ArrayList<Cargo>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Cargo where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cargo car = new Cargo();
				car.setT_Cargo_ID(rs.getInt("T_Cargo_ID"));
				car.setNombre(rs.getString("Nombre"));
				cargos.add(car);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS CARGOS "+ e.getMessage());
			e.printStackTrace();
		}
		return cargos;
	}
	
	public ArrayList<Cargo> listarCargos()
	{
		ArrayList<Cargo> cargos = new ArrayList<Cargo>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Cargo where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cargo car = new Cargo();
				car.setT_Cargo_ID(rs.getInt("T_Cargo_ID"));
				car.setNombre(rs.getString("Nombre"));
				cargos.add(car);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS CARGOS "+ e.getMessage());
			e.printStackTrace();
		}
		return cargos;
	}
	
	public boolean guardarCargo(Cargo car)
	{
		boolean guardado = false;
		try 
		{
			this.listarCargos();
			rs.moveToInsertRow();
			rs.updateString("Nombre", car.getNombre());
			rs.updateBoolean("Estado", car.isEstado());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR CARGO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarCargo(Cargo car) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Cargo set estado = 0 where T_Cargo_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, car.getT_Cargo_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarCargo(Cargo car) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_Cargo set estado = 1 where T_Cargo_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, car.getT_Cargo_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarCargo(Cargo car)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_Cargo set Nombre = ? where T_Cargo_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, car.getNombre());
			ps.setInt(		2, car.getT_Cargo_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}