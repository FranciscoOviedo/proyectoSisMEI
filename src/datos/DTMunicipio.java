package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Municipio;

public class DTMunicipio {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Municipio> listarMunicipios()
	{
		ArrayList<Municipio> municipios = new ArrayList<Municipio>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Municipio where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Municipio m = new Municipio();
				m.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				m.setT_Departamento_ID(rs.getInt("T_Departamento_ID"));
				m.setNombre(rs.getString("Nombre"));
				m.setEstado(rs.getBoolean("Estado"));
				municipios.add(m);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MUNICIPIOS "+ e.getMessage());
			e.printStackTrace();
		}
		return municipios;
	}
	
	public boolean guardarMunicipio(Municipio m)
	{
		boolean guardado = false;
		try 
		{
			this.listarMunicipios();
			rs.moveToInsertRow();
			rs.updateInt("T_Departamento_ID", m.getT_Departamento_ID());
			rs.updateString("Nombre", m.getNombre());
			rs.updateBoolean("Estado", m.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MUNICIPIO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMunicipio(Municipio m) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Municipio set estado = 0 where T_Municipio_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, m.getT_Municipio_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarMunicipio(Municipio m)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_Municipio set T_Departamento_ID=?, Nombre = ?, Estado=? where T_Municipio_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, m.getT_Departamento_ID());
			ps.setString(	2, m.getNombre());
			ps.setBoolean(	3, m.isEstado());
			ps.setInt(		4, m.getT_Municipio_ID());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
