package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Gasto;
import entidades.MacroObjetivo;

public class DTMacroObjetivo {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<MacroObjetivo> listaMacroObjetivoInactivos()
	{
		ArrayList<MacroObjetivo> macroObjetivo = new ArrayList<MacroObjetivo>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MacroObjetivo where estado = 3");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroObjetivo obj = new MacroObjetivo();
				
				obj.setT_MacroObjetivo_ID(rs.getInt("T_MacroObjetivo_ID"));
				obj.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				obj.setDefinicion(rs.getString("Definicion"));
				obj.setEstado(rs.getInt("Estado"));
				obj.setID_User_Registra(rs.getInt("ID_User_Registra"));
				obj.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				obj.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				obj.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				obj.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				obj.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				macroObjetivo.add(obj);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MACROPROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return macroObjetivo;
	}
	
	public ArrayList<MacroObjetivo> listarMacroObjetivo(int idMacro)
	{
		ArrayList<MacroObjetivo> macroObjetivo = new ArrayList<MacroObjetivo>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MacroObjetivo where estado <> 3 and T_MacroProyecto_ID="+idMacro);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroObjetivo obj = new MacroObjetivo();
				obj.setT_MacroObjetivo_ID(rs.getInt("T_MacroObjetivo_ID"));
				obj.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				obj.setDefinicion(rs.getString("Definicion"));
				obj.setEstado(rs.getInt("Estado"));
				obj.setID_User_Registra(rs.getInt("ID_User_Registra"));
				obj.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				obj.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				obj.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				obj.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				obj.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				macroObjetivo.add(obj);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MACROPROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return macroObjetivo;
	}
	
	public ArrayList<MacroObjetivo> listarObjetivo()
	{
		ArrayList<MacroObjetivo> objetivos = new ArrayList<MacroObjetivo>();
		String sql = ("SELECT * FROM T_MacroObjetivo where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroObjetivo o = new MacroObjetivo();
				o.setT_MacroObjetivo_ID(rs.getInt("T_MacroObjetivo_ID"));
				o.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				o.setDefinicion(rs.getString("Definicion"));
				objetivos.add(o);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS OBJETIVOS "+ e.getMessage());
			e.printStackTrace();
		}
		return objetivos;
	}
	
	public boolean guardarMacroObjetivo(MacroObjetivo obj)
	{
		boolean guardado = false;
		try 
		{
			this.listarObjetivo();
			rs.moveToInsertRow();
			rs.updateInt("T_MacroProyecto_ID", obj.getT_MacroProyecto_ID());
			rs.updateString("Definicion", obj.getDefinicion());
			rs.updateInt("Estado", obj.getEstado());
			rs.updateInt("ID_User_Registra", obj.getID_User_Registra());
			rs.updateTimestamp("Fecha_Registra",obj.getFecha_Registra());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MACROOBJETIVO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMacroObjetivo(MacroObjetivo obj) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_MacroProyecto set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MacroObjetivo set Estado = 3, ID_User_Elimina=?, Fecha_Elimina=? where T_MacroObjetivo_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, obj.getID_User_Elimina());
			ps.setTimestamp(2, obj.getFecha_Elimina());
			ps.setInt(3, obj.getT_MacroProyecto_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR MACROOBJETIVO"+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarMacroObjetivo(MacroObjetivo obj) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MacroObjetivo set estado = 1 where T_MacroObjetivo_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, obj.getT_MacroObjetivo_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMacroObjetivo(MacroObjetivo obj)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_MacroProyecto set estado = 2, .......");
		String sql = ("UPDATE T_MacroProyecto set Estado = 2, Nombre = ?, ObjetivoGeneral=?, FechaInicio=?, FechaFin = ?, Estado=?, ID_User_Modifica=?, Fecha_Modifica=? where T_MicroProyecto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, obj.getDefinicion());
			ps.setInt(		2, obj.getEstado());
			ps.setInt(		3, obj.getID_User_Modifica());
			ps.setTimestamp(4, obj.getFecha_Modifica());
			ps.setInt(		5, obj.getT_MacroProyecto_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR MACROOBJETIVO "+e.getMessage());
			
		}
		return modificado;
	}
}
