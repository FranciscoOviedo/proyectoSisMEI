package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Cuota;

public class DTCuotaMiembro {

	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<Cuota> listarCuotas()
	{
		ArrayList<Cuota> cuotas = new ArrayList<Cuota>();
		String sql = ("SELECT * FROM T_CuotaMiembro where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Cuota cuo = new Cuota();
				cuo.setT_CuotaMiembro_ID(rs.getInt("T_CuotaMiembro_ID"));
				cuo.setAnio(rs.getInt("Anio"));
				cuo.setCancelado(rs.getBoolean("Cancelado"));
				cuotas.add(cuo);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS CUOTAS "+ e.getMessage());
			e.printStackTrace();
		}
		return cuotas;
	}
	public boolean guardarCuota(Cuota cuo)
	{
		boolean guardado = false;
		try 
		{
			this.listarCuotas();
			rs.moveToInsertRow();
			rs.updateInt("Anio", cuo.getAnio());
			rs.updateBoolean("Cancelado", cuo.isCancelado());
			rs.updateBoolean("Estado", cuo.isEstado());
			rs.updateInt("T_MiembroCuota_ID", cuo.getT_CuotaMiembro_ID());
			rs.updateInt("T_Miembro_ID", cuo.getT_Miembro_ID());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR CUOTA "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	public boolean eliminarCuota(Cuota cuo) throws SQLException
	{
		
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_CuotaMiembro set estado = false where T_CuotaMiembro_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, cuo.getT_CuotaMiembro_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	public boolean modificarCuota(Cuota cuo)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_CuotaMiembro set Nombre = ? where T_CuotaMiembro_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			
			ps.setInt(	1, cuo.getAnio());
			ps.setBoolean(	2, cuo.isCancelado());
			ps.setBoolean(	3, cuo.isEstado());
			ps.setInt(		4, cuo.getT_CuotaMiembro_ID());
			ps.setInt(		5, cuo.getT_Miembro_ID());
			
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
	
}
