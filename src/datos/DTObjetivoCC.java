package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.ObjetivoCC;
import vistas.V_Info_ObjetivoCC;

public class DTObjetivoCC {

	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<V_Info_ObjetivoCC> listarInfoObjetivoCC(int herramientaCC)
	{
		ArrayList<V_Info_ObjetivoCC> infoObjetivoCC = new ArrayList<V_Info_ObjetivoCC>();
		String sql = ("SELECT * FROM V_Info_ObjetivoCC where estado <> 0 and T_HerramientaCC_ID=" + herramientaCC + " ORDER BY T_Categoria_ID desc");
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_ObjetivoCC ioc = new V_Info_ObjetivoCC();
				ioc.setT_ObjetivoCC_ID(rs.getInt("T_ObjetivoCC_ID"));
				ioc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				ioc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				ioc.setNombre(rs.getString("Nombre"));
				ioc.setCriterio1(rs.getString("Criterio1"));
				ioc.setCriterio2(rs.getString("Criterio2"));
				ioc.setCriterio3(rs.getString("Criterio3"));
				ioc.setCriterio4(rs.getString("Criterio4"));
				ioc.setCriterio5(rs.getString("Criterio5"));
				ioc.setNombreCategoria(rs.getString("NombreCategoria"));
				infoObjetivoCC.add(ioc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LA INFORMACION DE LOS OBJETIVOS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return infoObjetivoCC;
	}
	
	
	public ArrayList<ObjetivoCC> listarObjetivoCCInactivos(int herramientaCC)
	{
		ArrayList<ObjetivoCC> objetivoCC = new ArrayList<ObjetivoCC>();
		String sql = ("SELECT * FROM T_ObjetivoCC where estado = 0 and T_HerramientaCC_ID = " + herramientaCC);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				ObjetivoCC oc = new ObjetivoCC();
				oc.setT_ObjetivoCC_ID(rs.getInt("T_ObjetivoCC_ID"));
				oc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				oc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				oc.setNombre(rs.getString("Nombre"));
				oc.setCriterio1(rs.getString("Criterio1"));
				oc.setCriterio2(rs.getString("Criterio2"));
				oc.setCriterio3(rs.getString("Criterio3"));
				oc.setCriterio4(rs.getString("Criterio4"));
				oc.setCriterio5(rs.getString("Criterio5"));
				objetivoCC.add(oc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS OBJETIVOS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return objetivoCC;
	}
	
	public ArrayList<ObjetivoCC> listarObjetivoCCInactivos()
	{
		ArrayList<ObjetivoCC> objetivoCC = new ArrayList<ObjetivoCC>();
		String sql = ("SELECT * FROM T_ObjetivoCC where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				ObjetivoCC oc = new ObjetivoCC();
				oc.setT_ObjetivoCC_ID(rs.getInt("T_ObjetivoCC_ID"));
				oc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				oc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				oc.setNombre(rs.getString("Nombre"));
				oc.setCriterio1(rs.getString("Criterio1"));
				oc.setCriterio2(rs.getString("Criterio2"));
				oc.setCriterio3(rs.getString("Criterio3"));
				oc.setCriterio4(rs.getString("Criterio4"));
				oc.setCriterio5(rs.getString("Criterio5"));
				objetivoCC.add(oc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS OBJETIVOS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return objetivoCC;
	}
	
	//Metodo Sobrecargado que solo lista los objetivos que tengan el id especifico de esta herramientaCC
	public ArrayList<ObjetivoCC> listarObjetivoCC(int herramientaCC)
	{
		ArrayList<ObjetivoCC> objetivoCC = new ArrayList<ObjetivoCC>();
		String sql = ("SELECT * FROM T_ObjetivoCC where estado <> 0 and T_OjbetivoCC_ID = " + herramientaCC);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				ObjetivoCC oc = new ObjetivoCC();
				oc.setT_ObjetivoCC_ID(rs.getInt("T_ObjetivoCC_ID"));
				oc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				oc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				oc.setNombre(rs.getString("Nombre"));
				oc.setCriterio1(rs.getString("Criterio1"));
				oc.setCriterio2(rs.getString("Criterio2"));
				oc.setCriterio3(rs.getString("Criterio3"));
				oc.setCriterio4(rs.getString("Criterio4"));
				oc.setCriterio5(rs.getString("Criterio5"));
				objetivoCC.add(oc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS OBJETIVOS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return objetivoCC;
	}
	
	public ArrayList<ObjetivoCC> listarObjetivoCC()
	{
		ArrayList<ObjetivoCC> objetivoCC = new ArrayList<ObjetivoCC>();
		String sql = ("SELECT * FROM T_ObjetivoCC where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				ObjetivoCC oc = new ObjetivoCC();
				oc.setT_ObjetivoCC_ID(rs.getInt("T_ObjetivoCC_ID"));
				oc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				oc.setT_Categoria_ID(rs.getInt("T_Categoria_ID"));
				oc.setNombre(rs.getString("Nombre"));
				oc.setCriterio1(rs.getString("Criterio1"));
				oc.setCriterio2(rs.getString("Criterio2"));
				oc.setCriterio3(rs.getString("Criterio3"));
				oc.setCriterio4(rs.getString("Criterio4"));
				oc.setCriterio5(rs.getString("Criterio5"));
				objetivoCC.add(oc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS OBJETIVOS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return objetivoCC;
	}
	
	public boolean guardarObjetivoCC(ObjetivoCC oc)
	{
		boolean guardado = false;
		try 
		{
			this.listarObjetivoCC();
			rs.moveToInsertRow();
			rs.updateInt("T_HerramientaCC_ID", oc.getT_HerramientaCC_ID());
			rs.updateInt("T_Categoria_ID", oc.getT_Categoria_ID());
			rs.updateString("Nombre", oc.getNombre());
			rs.updateBoolean("Estado", oc.isEstado());
			rs.updateString("Criterio1", oc.getCriterio1());
			rs.updateString("Criterio2", oc.getCriterio2());
			rs.updateString("Criterio3", oc.getCriterio3());
			rs.updateString("Criterio4", oc.getCriterio4());
			rs.updateString("Criterio5", oc.getCriterio5());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR OBJETIVO CC "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarObjetivoCC(ObjetivoCC oc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_ObjetivoCC set estado = 0 where T_ObjetivoCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, oc.getT_ObjetivoCC_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarObjetivoCC(ObjetivoCC oc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_ObjetivoCC set estado = 1 where T_ObjetivoCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, oc.getT_ObjetivoCC_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL RESTAURAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarObjetivoCC(ObjetivoCC oc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_ObjetivoCC set T_HerramientaCC_ID=?, T_Categoria_ID=?, Nombre=?, Criterio1=?, Criterio2=?, Criterio3=?, Criterio4=?, Criterio5=?, Estado=? where T_ObjetivoCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, oc.getT_HerramientaCC_ID());
			ps.setInt(		2, oc.getT_Categoria_ID());
			ps.setString(	3, oc.getNombre());
			ps.setString(	4, oc.getCriterio1());
			ps.setString(	5, oc.getCriterio2());
			ps.setString(	6, oc.getCriterio3());
			ps.setString(	7, oc.getCriterio4());
			ps.setString(	8, oc.getCriterio5());
			ps.setBoolean(	9, oc.isEstado());
			ps.setInt(		10, oc.getT_ObjetivoCC_ID());
			
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
