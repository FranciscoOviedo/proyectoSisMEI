package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.HerramientaADIB;
import vistas.V_Info_HerramientaADIB;

public class DTHerramientaADIB {
	
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	
	public ArrayList<V_Info_HerramientaADIB> listarInfoHerramientaADIBInactivos()
	{
		ArrayList<V_Info_HerramientaADIB> infoHerramientaADIB = new ArrayList<V_Info_HerramientaADIB>();
		String sql = ("SELECT * FROM V_Info_HerramientaADIB where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_HerramientaADIB iha = new V_Info_HerramientaADIB();
				iha.setT_HerramientaADIB_ID(rs.getInt("T_HerramientaADIB_ID"));
				iha.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				iha.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				iha.setFechaCreacion(rs.getDate("FechaCreacion"));
				iha.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				iha.setNombreMacroProyecto(rs.getString("NombreMacroProyecto"));
				infoHerramientaADIB.add(iha);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS ADIB "+ e.getMessage());
			e.printStackTrace();
		}
		return infoHerramientaADIB;
	}
	
	public ArrayList<V_Info_HerramientaADIB> listaInfoHerramientaADIB()
	{
		ArrayList<V_Info_HerramientaADIB> infoHerramientaADIB = new ArrayList<V_Info_HerramientaADIB>();
		String sql = ("SELECT * FROM V_Info_HerramientaADIB where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_HerramientaADIB ihc = new V_Info_HerramientaADIB();
				ihc.setT_HerramientaADIB_ID(rs.getInt("T_HerramientaADIB_ID"));
				ihc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				ihc.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				ihc.setFechaCreacion(rs.getDate("FechaCreacion"));
				ihc.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				ihc.setNombreMacroProyecto(rs.getString("NombreMacroProyecto"));
				infoHerramientaADIB.add(ihc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS ADIB "+ e.getMessage());
			e.printStackTrace();
		}
		return infoHerramientaADIB;
	}
	
	
	
	public ArrayList<HerramientaADIB> listarHerramientaADIBInactivos()
	{
		ArrayList<HerramientaADIB> herramientaADIB = new ArrayList<HerramientaADIB>();
		String sql = ("SELECT * FROM T_HerramientaADIB where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				HerramientaADIB hc = new HerramientaADIB();
				hc.setT_HerramientaADIB_ID(rs.getInt("T_HerramientaADIB_ID"));
				hc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				hc.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				hc.setFechaCreacion(rs.getDate("FechaCreacion"));
				herramientaADIB.add(hc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS ADIB"+ e.getMessage());
			e.printStackTrace();
		}
		return herramientaADIB;
	}
	
	public ArrayList<HerramientaADIB> listarHerramientaADIB()
	{
		ArrayList<HerramientaADIB> herramientaADIB = new ArrayList<HerramientaADIB>();
		String sql = ("SELECT * FROM T_HerramientaADIB where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				HerramientaADIB ha = new HerramientaADIB();
				ha.setT_HerramientaADIB_ID(rs.getInt("T_HerramientaADIB_ID"));
				ha.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				ha.setT_MacroProyecto_ID(rs.getInt("T_MacroProyecto_ID"));
				ha.setFechaCreacion(rs.getDate("FechaCreacion"));
				herramientaADIB.add(ha);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS ADIB "+ e.getMessage());
			e.printStackTrace();
		}
		return herramientaADIB;
	}
	
	public boolean guardarHerramientaADIB(HerramientaADIB ha)
	{
		boolean guardado = false;
		try 
		{
			this.listarHerramientaADIB();
			rs.moveToInsertRow();
			rs.updateInt("T_FamiliaKolping_ID", ha.getT_FamiliaKolping_ID());
			rs.updateInt("T_MacroProyecto_ID", ha.getT_MacroProyecto_ID());
			rs.updateDate("FechaCreacion", new java.sql.Date(ha.getFechaCreacion().getTime()));
			rs.updateBoolean("Estado", ha.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR HERRAMIENTA ADIB "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarHerramientaADIB(HerramientaADIB ha) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_HerramientaADIB set estado = 0 where T_HerramientaADIB_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, ha.getT_HerramientaADIB_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR HERRAMIENTA ADIB "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarHerramientaADIB(HerramientaADIB ha) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_HerramientaADIB set estado = 1 where T_HerramientaADIB_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, ha.getT_HerramientaADIB_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarHerramientaADIB(HerramientaADIB ha)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_HerramientaADIB set T_FamiliaKolping_ID=?, T_MicroProyecto_ID=?, FechaCreacion=? where T_HerramientaADIB_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, ha.getT_FamiliaKolping_ID());
			ps.setInt(		2, ha.getT_MacroProyecto_ID());
			ps.setDate(		3, new java.sql.Date(ha.getFechaCreacion().getTime()));
			ps.setInt(		4, ha.getT_HerramientaADIB_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
