package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.DetalleMedicionCI;
import entidades.Formador;
import vistas.V_Info_ObjetivoCI;
import vistas.V_Miembro_DetalleMedicionCI;

public class DTDetalleMedicionCI {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<V_Miembro_DetalleMedicionCI> listarMiembroMedicionCI(int medicionCIID, int familiaKolpingID, ArrayList<V_Info_ObjetivoCI> objetivos)
	{
		ArrayList<V_Miembro_DetalleMedicionCI> detalleMedicionCI = new ArrayList<V_Miembro_DetalleMedicionCI>();
		String sql = ("select T_Miembro.T_Miembro_ID, T_Miembro.Nombre, T_Miembro.Apellido, T_DetalleMedicionCI.* "
				+ "from T_Miembro "
				+ "left join T_DetalleMedicionCI "
				+ "on T_Miembro.T_Miembro_ID = T_DetalleMedicionCI.T_Miembro_ID "
				+ "AND T_DetalleMedicionCI.T_MedicionCI_ID = ? "
				+ "left join T_ObjetivoCI "
				+ "on T_ObjetivoCI.T_ObjetivoCI_ID = T_DetalleMedicionCI.T_ObjetivoCI_ID "
				+ "where T_Miembro.T_FamiliaKolping_ID = ? "
				+ "ORDER BY T_Miembro.T_Miembro_ID, T_ObjetivoCI.T_Categoria_ID, T_ObjetivoCI.T_ObjetivoCI_ID desc;");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, medicionCIID);
			ps.setInt(2, familiaKolpingID);
			rs = ps.executeQuery();
			int detalleID, miembroID;
			
			while(rs.next())
			{
				V_Miembro_DetalleMedicionCI mdmc = new V_Miembro_DetalleMedicionCI();
				mdmc.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				mdmc.setNombre(rs.getString("Nombre"));
				mdmc.setApellido(rs.getString("Apellido"));
				System.out.println(mdmc.getNombre());
				miembroID = rs.getInt("T_Miembro_ID");
				detalleID = rs.getInt("T_DetalleMedicionCI_ID");
				if(rs.wasNull()){
					detalleID = -1;
				}
				for(V_Info_ObjetivoCI ioc : objetivos){
					DetalleMedicionCI temp = new DetalleMedicionCI();
					temp.setT_MedicionCI_ID(medicionCIID);
					temp.setT_Miembro_ID(mdmc.getT_Miembro_ID());
					temp.setT_ObjetivoCI_ID(ioc.getT_ObjetivoCI_ID());
					if(detalleID==-1){
						temp.setT_DetalleMedicionCI_ID(-1);
						temp.setValor(0);
						temp.setEstado(true);
					} else {
						if(rs.getInt("T_ObjetivoCI_ID")==temp.getT_ObjetivoCI_ID()){
							temp.setT_DetalleMedicionCI_ID(rs.getInt("T_DetalleMedicionCI_ID"));
							temp.setValor(rs.getInt("Valor"));
							temp.setEstado(rs.getBoolean("Estado"));
							if(rs.next()){
								if(miembroID!=rs.getInt("T_Miembro_ID")){
									detalleID = -1;
									rs.previous();
								}
							}else{
								detalleID = -1;
								rs.previous();
							}
							
						} else{
							temp.setT_DetalleMedicionCI_ID(-1);
							temp.setValor(0);
							temp.setEstado(true);
						}
					}
					mdmc.getMediciones().add(temp);
				}
				detalleMedicionCI.add(mdmc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS DETALLES DE MEDICION CI "+ e.getMessage());
			e.printStackTrace();
		}
		return detalleMedicionCI;
	}
	
	public ArrayList<DetalleMedicionCI> listarDetalleMedicionCI()
	{
		ArrayList<DetalleMedicionCI> detalleMediciones = new ArrayList<DetalleMedicionCI>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_DetalleMedicionCI where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				DetalleMedicionCI dmc = new DetalleMedicionCI();
				dmc.setT_DetalleMedicionCI_ID(rs.getInt("T_DetalleMedicionCI_ID"));
				dmc.setT_ObjetivoCI_ID(rs.getInt("T_ObjetivoCI_ID"));
				dmc.setT_MedicionCI_ID(rs.getInt("T_MedicionCI_ID"));
				dmc.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				dmc.setValor(rs.getInt("Valor"));
				detalleMediciones.add(dmc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS DETALLES DE MEDICIONES "+ e.getMessage());
			e.printStackTrace();
		}
		return detalleMediciones;
	}
	
	public boolean guardarListaDetalleMedicionCI(ArrayList<DetalleMedicionCI> mc)
	{
		boolean guardado = false;
		PreparedStatement ps;
		String sql = ("Insert into T_DetalleMedicionCI (T_ObjetivoCI_ID, T_MedicionCI_ID, T_Miembro_ID, Valor, Estado) values (?, ?, ?, ?, ?);");
		try 
		{
			ps = cn.prepareStatement(sql);
			boolean estado = true;
			for(DetalleMedicionCI dmcTemp : mc){
				ps.setInt(1, dmcTemp.getT_ObjetivoCI_ID());
				ps.setInt(2, dmcTemp.getT_MedicionCI_ID());
				ps.setInt(3, dmcTemp.getT_Miembro_ID());
				ps.setInt(4, dmcTemp.getValor());
				ps.setBoolean(5, estado);
				ps.addBatch();
			}
			ps.executeBatch();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR LISTA DE DETALLE MEDICION CI "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarListaDetalleMedicionCI(ArrayList<DetalleMedicionCI> mc)
	{
		boolean eliminado = true;
		PreparedStatement ps;
		String sql = ("Delete from T_DetalleMedicionCI where T_DetalleMedicionCI_ID = ?;");
		try 
		{
			ps = cn.prepareStatement(sql);
			for(DetalleMedicionCI dmcTemp : mc){
				ps.setInt(1, dmcTemp.getT_DetalleMedicionCI_ID());
				ps.addBatch();
			}
			ps.executeBatch();
			eliminado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR LISTA DE DETALLE MEDICION CI "+e.getMessage());
			e.printStackTrace();
		}
		
		return eliminado;
	}
	
	public boolean modificarListaMedicionCI(ArrayList<DetalleMedicionCI> mc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_DetalleMedicionCI set T_ObjetivoCI_ID = ?, T_MedicionCI_ID = ?, T_Miembro_ID = ?, Valor = ?, Estado = ? where T_DetalleMedicionCI_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			boolean estado = true;
			for(DetalleMedicionCI dmcTemp : mc){
				ps.setInt(1, dmcTemp.getT_ObjetivoCI_ID());
				ps.setInt(2, dmcTemp.getT_MedicionCI_ID());
				ps.setInt(3, dmcTemp.getT_Miembro_ID());
				ps.setInt(4, dmcTemp.getValor());
				ps.setBoolean(5, estado);
				ps.setInt(6, dmcTemp.getT_DetalleMedicionCI_ID());
				ps.addBatch();
			}
			ps.executeBatch();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
