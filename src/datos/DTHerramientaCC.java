package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.HerramientaCC;
import vistas.V_Info_HerramientaCC;

public class DTHerramientaCC {
	
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	
	public ArrayList<V_Info_HerramientaCC> listarInfoHerramientaCCInactivos()
	{
		ArrayList<V_Info_HerramientaCC> infoHerramientaCC = new ArrayList<V_Info_HerramientaCC>();
		String sql = ("SELECT * FROM V_Info_HerramientaCC where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_HerramientaCC ihc = new V_Info_HerramientaCC();
				ihc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				ihc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				ihc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				ihc.setFechaCreacion(rs.getDate("FechaCreacion"));
				ihc.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				ihc.setNombreMicroProyecto(rs.getString("NombreMicroProyecto"));
				infoHerramientaCC.add(ihc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return infoHerramientaCC;
	}
	
	public ArrayList<V_Info_HerramientaCC> listarInfoHerramientaCC()
	{
		ArrayList<V_Info_HerramientaCC> infoHerramientaCC = new ArrayList<V_Info_HerramientaCC>();
		String sql = ("SELECT * FROM V_Info_HerramientaCC where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_HerramientaCC ihc = new V_Info_HerramientaCC();
				ihc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				ihc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				ihc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				ihc.setFechaCreacion(rs.getDate("FechaCreacion"));
				ihc.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				ihc.setNombreMicroProyecto(rs.getString("NombreMicroProyecto"));
				infoHerramientaCC.add(ihc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return infoHerramientaCC;
	}
	
	
	
	public ArrayList<HerramientaCC> listarHerramientaCCInactivos()
	{
		ArrayList<HerramientaCC> herramientaCC = new ArrayList<HerramientaCC>();
		String sql = ("SELECT * FROM T_HerramientaCC where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				HerramientaCC hc = new HerramientaCC();
				hc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				hc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				hc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				hc.setFechaCreacion(rs.getDate("FechaCreacion"));
				herramientaCC.add(hc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return herramientaCC;
	}
	
	public ArrayList<HerramientaCC> listarHerramientaCC()
	{
		ArrayList<HerramientaCC> herramientaCC = new ArrayList<HerramientaCC>();
		String sql = ("SELECT * FROM T_HerramientaCC where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				HerramientaCC hc = new HerramientaCC();
				hc.setT_HerramientaCC_ID(rs.getInt("T_HerramientaCC_ID"));
				hc.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				hc.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				hc.setFechaCreacion(rs.getDate("FechaCreacion"));
				herramientaCC.add(hc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS HERRAMIENTAS CC "+ e.getMessage());
			e.printStackTrace();
		}
		return herramientaCC;
	}
	
	public boolean guardarHerramientaCC(HerramientaCC hc)
	{
		boolean guardado = false;
		try 
		{
			this.listarHerramientaCC();
			rs.moveToInsertRow();
			rs.updateInt("T_FamiliaKolping_ID", hc.getT_FamiliaKolping_ID());
			rs.updateInt("T_MicroProyecto_ID", hc.getT_MicroProyecto_ID());
			rs.updateDate("FechaCreacion", new java.sql.Date(hc.getFechaCreacion().getTime()));
			rs.updateBoolean("Estado", hc.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR HERRAMIENTA CC "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarHerramientaCC(HerramientaCC hc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_HerramientaCC set estado = 0 where T_HerramientaCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, hc.getT_HerramientaCC_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarHerramientaCC(HerramientaCC hc) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_HerramientaCC set estado = 1 where T_HerramientaCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, hc.getT_HerramientaCC_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarHerramientaCC(HerramientaCC hc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_HerramientaCC set T_FamiliaKolping_ID=?, T_MicroProyecto_ID=?, FechaCreacion=? where T_HerramientaCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, hc.getT_FamiliaKolping_ID());
			ps.setInt(		2, hc.getT_MicroProyecto_ID());
			ps.setDate(		3, new java.sql.Date(hc.getFechaCreacion().getTime()));
			ps.setInt(		4, hc.getT_HerramientaCC_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
