package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.DetalleMedicionCC;
import entidades.Formador;
import vistas.V_Info_ObjetivoCC;
import vistas.V_Miembro_DetalleMedicionCC;

public class DTDetalleMedicionCC {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<V_Miembro_DetalleMedicionCC> listarMiembroMedicionCC(int medicionCCID, int familiaKolpingID, ArrayList<V_Info_ObjetivoCC> objetivos)
	{
		ArrayList<V_Miembro_DetalleMedicionCC> detalleMedicionCC = new ArrayList<V_Miembro_DetalleMedicionCC>();
		String sql = ("select T_Miembro.T_Miembro_ID, T_Miembro.Nombre, T_Miembro.Apellido, T_DetalleMedicionCC.* "
				+ "from T_Miembro "
				+ "left join T_DetalleMedicionCC "
				+ "on T_Miembro.T_Miembro_ID = T_DetalleMedicionCC.T_Miembro_ID "
				+ "AND T_DetalleMedicionCC.T_MedicionCI_ID = ? "
				+ "left join T_ObjetivoCC "
				+ "on T_ObjetivoCC.T_ObjetivoCC_ID = T_DetalleMedicionCC.T_ObjetivoCC_ID "
				+ "where T_Miembro.T_FamiliaKolping_ID = ? "
				+ "ORDER BY T_Miembro.T_Miembro_ID, T_ObjetivoCC.T_Categoria_ID, T_ObjetivoCC.T_ObjetivoCC_ID desc;");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, medicionCCID);
			ps.setInt(2, familiaKolpingID);
			rs = ps.executeQuery();
			int detalleID, miembroID;
			
			while(rs.next())
			{
				V_Miembro_DetalleMedicionCC mdmc = new V_Miembro_DetalleMedicionCC();
				mdmc.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				mdmc.setNombre(rs.getString("Nombre"));
				mdmc.setApellido(rs.getString("Apellido"));
				System.out.println(mdmc.getNombre());
				miembroID = rs.getInt("T_Miembro_ID");
				detalleID = rs.getInt("T_DetalleMedicionCC_ID");
				if(rs.wasNull()){
					detalleID = -1;
				}
				for(V_Info_ObjetivoCC ioc : objetivos){
					DetalleMedicionCC temp = new DetalleMedicionCC();
					temp.setT_MedicionCC_ID(medicionCCID);
					temp.setT_Miembro_ID(mdmc.getT_Miembro_ID());
					temp.setT_ObjetivoCC_ID(ioc.getT_ObjetivoCC_ID());
					if(detalleID==-1){
						temp.setT_DetalleMedicionCC_ID(-1);
						temp.setValor(0);
						temp.setEstado(true);
					} else {
						if(rs.getInt("T_ObjetivoCC_ID")==temp.getT_ObjetivoCC_ID()){
							temp.setT_DetalleMedicionCC_ID(rs.getInt("T_DetalleMedicionCC_ID"));
							temp.setValor(rs.getInt("Valor"));
							temp.setEstado(rs.getBoolean("Estado"));
							if(rs.next()){
								if(miembroID!=rs.getInt("T_Miembro_ID")){
									detalleID = -1;
									rs.previous();
								}
							}else{
								detalleID = -1;
								rs.previous();
							}
							
						} else{
							temp.setT_DetalleMedicionCC_ID(-1);
							temp.setValor(0);
							temp.setEstado(true);
						}
					}
					mdmc.getMediciones().add(temp);
				}
				detalleMedicionCC.add(mdmc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS DETALLES DE MEDICION CC "+ e.getMessage());
			e.printStackTrace();
		}
		return detalleMedicionCC;
	}
	
	public ArrayList<DetalleMedicionCC> listarDetalleMedicionCC()
	{
		ArrayList<DetalleMedicionCC> detalleMediciones = new ArrayList<DetalleMedicionCC>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_DetalleMedicionCC where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				DetalleMedicionCC dmc = new DetalleMedicionCC();
				dmc.setT_DetalleMedicionCC_ID(rs.getInt("T_DetalleMedicionCC_ID"));
				dmc.setT_ObjetivoCC_ID(rs.getInt("T_ObjetivoCC_ID"));
				dmc.setT_MedicionCC_ID(rs.getInt("T_MedicionCC_ID"));
				dmc.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				dmc.setValor(rs.getInt("Valor"));
				detalleMediciones.add(dmc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS DETALLES DE MEDICIONES "+ e.getMessage());
			e.printStackTrace();
		}
		return detalleMediciones;
	}
	
	public boolean guardarListaDetalleMedicionCC(ArrayList<DetalleMedicionCC> mc)
	{
		boolean guardado = false;
		PreparedStatement ps;
		String sql = ("Insert into T_DetalleMedicionCC (T_ObjetivoCC_ID, T_MedicionCC_ID, T_Miembro_ID, Valor, Estado) values (?, ?, ?, ?, ?);");
		try 
		{
			ps = cn.prepareStatement(sql);
			boolean estado = true;
			for(DetalleMedicionCC dmcTemp : mc){
				ps.setInt(1, dmcTemp.getT_ObjetivoCC_ID());
				ps.setInt(2, dmcTemp.getT_MedicionCC_ID());
				ps.setInt(3, dmcTemp.getT_Miembro_ID());
				ps.setInt(4, dmcTemp.getValor());
				ps.setBoolean(5, estado);
				ps.addBatch();
			}
			ps.executeBatch();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR LISTA DE DETALLE MEDICION CC "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarListaDetalleMedicionCC(ArrayList<DetalleMedicionCC> mc)
	{
		boolean eliminado = true;
		PreparedStatement ps;
		String sql = ("Delete from T_DetalleMedicionCC where T_DetalleMedicionCC_ID = ?;");
		try 
		{
			ps = cn.prepareStatement(sql);
			for(DetalleMedicionCC dmcTemp : mc){
				ps.setInt(1, dmcTemp.getT_DetalleMedicionCC_ID());
				ps.addBatch();
			}
			ps.executeBatch();
			eliminado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR LISTA DE DETALLE MEDICION CC "+e.getMessage());
			e.printStackTrace();
		}
		
		return eliminado;
	}
	
	public boolean modificarListaMedicionCC(ArrayList<DetalleMedicionCC> mc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_DetalleMedicionCC set T_ObjetivoCC_ID = ?, T_MedicionCC_ID = ?, T_Miembro_ID = ?, Valor = ?, Estado = ? where T_DetalleMedicionCC_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			boolean estado = true;
			for(DetalleMedicionCC dmcTemp : mc){
				ps.setInt(1, dmcTemp.getT_ObjetivoCC_ID());
				ps.setInt(2, dmcTemp.getT_MedicionCC_ID());
				ps.setInt(3, dmcTemp.getT_Miembro_ID());
				ps.setInt(4, dmcTemp.getValor());
				ps.setBoolean(5, estado);
				ps.setInt(6, dmcTemp.getT_DetalleMedicionCC_ID());
				ps.addBatch();
			}
			ps.executeBatch();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
