package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import entidades.Miembro;
import vistas.V_Info_Miembro;
public class DTMiembro {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<V_Info_Miembro> listarInfoMiembrosInactivos()
	{
		ArrayList<V_Info_Miembro> miembros = new ArrayList<V_Info_Miembro>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM V_Info_Miembro where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_Miembro mi = new V_Info_Miembro();
				mi.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				mi.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				mi.setT_Cargo_ID(rs.getInt("T_Cargo_ID"));
				mi.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				mi.setT_TipoSocio_ID(rs.getInt("T_TipoSocio_ID"));
				mi.setNombre(rs.getString("Nombre"));
				mi.setApellido(rs.getString("Apellido"));
				mi.setCedula(rs.getString("Cedula"));
				mi.setFechaNacimiento(rs.getDate("FechaNacimiento"));
				mi.setFechaUnion(rs.getDate("FechaUnion"));
				mi.setSexo(rs.getInt("Sexo"));
				mi.setTelefono(rs.getString("Telefono"));
				mi.setNivelAcademico(rs.getString("NivelAcademico"));
				mi.setNombreTipoSocio(rs.getString("NombreTipoSocio"));
				mi.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				mi.setNombreDepartamento(rs.getString("NombreDepartamento"));
				mi.setNombreMunicipio(rs.getString("NombreMunicipio"));
				mi.setNombreCargo(rs.getString("NombreCargo"));
				
				miembros.add(mi);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MIEMBROS "+ e.getMessage());
			e.printStackTrace();
		}
		return miembros;
	}
	
	public ArrayList<V_Info_Miembro> listarInfoMiembros()
	{
		ArrayList<V_Info_Miembro> miembros = new ArrayList<V_Info_Miembro>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM V_Info_Miembro where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_Miembro mi = new V_Info_Miembro();
				mi.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				mi.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				mi.setT_Cargo_ID(rs.getInt("T_Cargo_ID"));
				mi.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				mi.setT_TipoSocio_ID(rs.getInt("T_TipoSocio_ID"));
				mi.setNombre(rs.getString("Nombre"));
				mi.setApellido(rs.getString("Apellido"));
				mi.setCedula(rs.getString("Cedula"));
				mi.setFechaNacimiento(rs.getDate("FechaNacimiento"));
				mi.setFechaUnion(rs.getDate("FechaUnion"));
				mi.setSexo(rs.getInt("Sexo"));
				mi.setTelefono(rs.getString("Telefono"));
				mi.setNivelAcademico(rs.getString("NivelAcademico"));
				mi.setNombreTipoSocio(rs.getString("NombreTipoSocio"));
				mi.setNombreFamiliaKolping(rs.getString("NombreFamiliaKolping"));
				mi.setNombreDepartamento(rs.getString("NombreDepartamento"));
				mi.setNombreMunicipio(rs.getString("NombreMunicipio"));
				mi.setNombreCargo(rs.getString("NombreCargo"));
				
				miembros.add(mi);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MIEMBROS "+ e.getMessage());
			e.printStackTrace();
		}
		return miembros;
	}
	
	public ArrayList<Miembro> listarMiembrosInactivos()
	{
		ArrayList<Miembro> miembros = new ArrayList<Miembro>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Miembro where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Miembro mi = new Miembro();
				mi.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				mi.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				mi.setT_Cargo_ID(rs.getInt("T_Cargo_ID"));
				mi.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				mi.setT_TipoSocio_ID(rs.getInt("T_TipoSocio_ID"));
				mi.setNombre(rs.getString("Nombre"));
				mi.setApellido(rs.getString("Apellido"));
				mi.setCedula(rs.getString("Cedula"));
				mi.setFechaNacimiento(rs.getDate("FechaNacimiento"));
				mi.setFechaUnion(rs.getDate("FechaUnion"));
				mi.setSexo(rs.getInt("Sexo"));
				mi.setTelefono(rs.getString("Telefono"));
				mi.setNivelAcademico(rs.getString("NivelAcademico"));
				
				miembros.add(mi);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MIEMBROS "+ e.getMessage());
			e.printStackTrace();
		}
		return miembros;
	}
	
	// espacio.....
	public ArrayList<Miembro> listarMiembros()
	{
		ArrayList<Miembro> miembros = new ArrayList<Miembro>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_Miembro where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Miembro mi = new Miembro();
				mi.setT_Miembro_ID(rs.getInt("T_Miembro_ID"));
				mi.setT_FamiliaKolping_ID(rs.getInt("T_FamiliaKolping_ID"));
				mi.setT_Cargo_ID(rs.getInt("T_Cargo_ID"));
				mi.setT_Municipio_ID(rs.getInt("T_Municipio_ID"));
				mi.setT_TipoSocio_ID(rs.getInt("T_TipoSocio_ID"));
				mi.setNombre(rs.getString("Nombre"));
				mi.setApellido(rs.getString("Apellido"));
				mi.setCedula(rs.getString("Cedula"));
				mi.setFechaNacimiento(rs.getDate("FechaNacimiento"));
				mi.setFechaUnion(rs.getDate("FechaUnion"));
				mi.setSexo(rs.getInt("Sexo"));
				mi.setTelefono(rs.getString("Telefono"));
				mi.setNivelAcademico(rs.getString("NivelAcademico"));
				
				miembros.add(mi);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MIEMBROS "+ e.getMessage());
			e.printStackTrace();
		}
		return miembros;
	}
	
	public boolean guardarMiembro(Miembro mi)
	{
		boolean guardado = false;
		try 
		{
			this.listarMiembros();
			rs.moveToInsertRow();
			rs.updateInt("T_FamiliaKolping_ID", mi.getT_FamiliaKolping_ID());
			rs.updateInt("T_Cargo_ID", mi.getT_Cargo_ID());
			rs.updateInt("T_Municipio_ID", mi.getT_Municipio_ID());
			rs.updateInt("T_TipoSocio_ID", mi.getT_TipoSocio_ID());
			rs.updateString("Nombre", mi.getNombre());
			rs.updateString("Apellido", mi.getApellido());
			rs.updateString("Cedula", mi.getCedula());
			rs.updateDate("FechaNacimiento", new java.sql.Date(mi.getFechaNacimiento().getTime()));
			rs.updateDate("FechaUnion", new java.sql.Date(mi.getFechaUnion().getTime()));
			rs.updateString("NivelAcademico", mi.getNivelAcademico());
			rs.updateInt("Sexo", mi.getSexo());
			rs.updateString("Telefono", mi.getTelefono());
			rs.updateBoolean("Estado", mi.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MIEMBRO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMiembro(Miembro mi) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Miembro set estado = 0 where T_Miembro_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, mi.getT_Miembro_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	public boolean restaurarMiembro(Miembro m) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_Miembro set estado = 1 where T_Miembro_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, m.getT_Miembro_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL RESTAURAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMiembro(Miembro mi)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_Miembro set T_Municipio_ID=?, T_FamiliaKolping_ID=?, T_Cargo_ID=?, T_TipoSocio_ID=?, Nombre = ?, Apellido=?, Cedula = ?, FechaNacimiento=?, FechaUnion = ?, Sexo = ?, NivelAcademico = ?, Telefono=? where T_Miembro_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, mi.getT_Municipio_ID());
			ps.setInt(		2, mi.getT_FamiliaKolping_ID());
			ps.setInt(		3, mi.getT_Cargo_ID());
			ps.setInt(		4, mi.getT_TipoSocio_ID());
			ps.setString(	5, mi.getNombre());
			ps.setString(	6, mi.getApellido());
			ps.setString(	7, mi.getCedula());
			ps.setDate(		8, new java.sql.Date(mi.getFechaNacimiento().getTime()));
			ps.setDate(		9, new java.sql.Date(mi.getFechaUnion().getTime()));
			ps.setInt(		10, mi.getSexo());
			ps.setString(	11, mi.getNivelAcademico());
			ps.setString(	12, mi.getTelefono());
			ps.setInt(		13, mi.getT_Miembro_ID());
			System.out.println(mi.getT_Miembro_ID());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
