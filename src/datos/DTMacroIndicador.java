package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


import entidades.MacroIndicador;

public class DTMacroIndicador {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<MacroIndicador> listaMacroIndicadorInactivos()
	{
		ArrayList<MacroIndicador> macroIndicador = new ArrayList<MacroIndicador>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MacroIndicador where estado = 3");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroIndicador ind = new MacroIndicador();
				
				ind.setT_MacroObjetivo_ID(rs.getInt("T_MacroObjetivo_ID"));
				ind.setT_MacroIndicador_ID(rs.getInt("T_MacroIndicador_ID"));
				ind.setDefinicion(rs.getString("Definicion"));
				ind.setTipoMedicion(rs.getString("TipoDato"));
				ind.setEstado(rs.getInt("Estado"));
				ind.setID_User_Registra(rs.getInt("ID_User_Registra"));
				ind.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				ind.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				ind.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				ind.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				ind.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				macroIndicador.add(ind);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MACROINDICADOR "+ e.getMessage());
			e.printStackTrace();
		}
		return macroIndicador;
	}
	
	public ArrayList<MacroIndicador> listarMacroIndicador(int idObj)
	{
		ArrayList<MacroIndicador> macroIndicador = new ArrayList<MacroIndicador>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_MacroIndicador where estado <> 3 and T_MacroObjetivo_ID="+idObj);
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroIndicador ind = new MacroIndicador();
				
				ind.setT_MacroObjetivo_ID(rs.getInt("T_MacroObjetivo_ID"));
				ind.setT_MacroIndicador_ID(rs.getInt("T_MacroIndicador_ID"));
				ind.setDefinicion(rs.getString("Definicion"));
				ind.setTipoMedicion(rs.getString("TipoMedicion"));
				ind.setEstado(rs.getInt("Estado"));
				ind.setID_User_Registra(rs.getInt("ID_User_Registra"));
				ind.setID_User_Modifica(rs.getInt("ID_User_Modifica"));
				ind.setID_User_Elimina(rs.getInt("ID_User_Elimina"));
				ind.setFecha_Registra(rs.getTimestamp("Fecha_Registra"));
				ind.setFecha_Modifica(rs.getTimestamp("Fecha_Modifica"));
				ind.setFecha_Elimina(rs.getTimestamp("Fecha_Elimina"));
				macroIndicador.add(ind);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS MACROINDICADORES "+ e.getMessage());
			e.printStackTrace();
		}
		return macroIndicador;
	}
	
	public ArrayList<MacroIndicador> listarObjetivo()
	{
		ArrayList<MacroIndicador> indicadores = new ArrayList<MacroIndicador>();
		String sql = ("SELECT * FROM T_MacroIndicador where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				MacroIndicador i = new MacroIndicador();
				i.setT_MacroObjetivo_ID(rs.getInt("T_MacroObjetivo_ID"));
				i.setT_MacroIndicador_ID(rs.getInt("T_MacroIndicador_ID"));
				i.setDefinicion(rs.getString("Definicion"));
				indicadores.add(i);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS INDICADORES "+ e.getMessage());
			e.printStackTrace();
		}
		return indicadores;
	}
	
	public boolean guardarMacroIndicador(MacroIndicador ind)
	{
		boolean guardado = false;
		try 
		{
			this.listarObjetivo();
			rs.moveToInsertRow();
			rs.updateInt("T_MacroObjetivo_ID", ind.getT_MacroObjetivo_ID());
			rs.updateString("Definicion", ind.getDefinicion());
			rs.updateString("TipoMedicion", ind.getTipoMedicion());
			rs.updateInt("Estado", ind.getEstado());
			rs.updateInt("ID_User_Registra", ind.getID_User_Registra());
			rs.updateTimestamp("Fecha_Registra",ind.getFecha_Registra());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR MACROINDICADOR "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarMacroIndicador(MacroIndicador ind) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_MacroProyecto set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MacroIndicador set Estado = 3, ID_User_Elimina=?, Fecha_Elimina=? where T_MacroIndicador_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, ind.getID_User_Elimina());
			ps.setTimestamp(2, ind.getFecha_Elimina());
			ps.setInt(3, ind.getT_MacroObjetivo_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR MACROINDICADOR"+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarMacroIndicador(MacroIndicador ind) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean restaurado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_MacroIndicador set estado = 3 where T_MacroIndicador_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, ind.getT_MacroIndicador_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarMacroIndicador(MacroIndicador ind)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_MacroProyecto set estado = 2, .......");
		String sql = ("UPDATE T_MacroIndicador set Estado = 2, Definicion = ?, TipoMedicion=?, Estado=?, ID_User_Modifica=?, Fecha_Modifica=? where T_MicroIndicador_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, ind.getDefinicion());
			ps.setInt(	2, ind.getEstado());
			ps.setString(	3, ind.getTipoMedicion());
			ps.setInt(		4, ind.getID_User_Modifica());
			ps.setTimestamp(5, ind.getFecha_Modifica());
			ps.setInt(		6, ind.getT_MacroObjetivo_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR MACROINDICADOR "+e.getMessage());
			
		}
		return modificado;
	}
}
