package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.TipoSocio;


public class DTTipoSocio {

	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<TipoSocio> listarSociosInactivos()
	{
		ArrayList<TipoSocio> socio = new ArrayList<TipoSocio>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_TipoSocio where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoSocio soc = new TipoSocio();
				soc.setT_TipoSocio_ID(rs.getInt("T_TipoSocio_ID"));
				soc.setNombre(rs.getString("Nombre"));
				socio.add(soc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS SOCIOS "+ e.getMessage());
			e.printStackTrace();
		}
		return socio;
	}
	
	public ArrayList<TipoSocio> listarSocios()
	{
		ArrayList<TipoSocio> socio = new ArrayList<TipoSocio>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_TipoSocio where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoSocio soc = new TipoSocio();
				soc.setT_TipoSocio_ID(rs.getInt("T_TipoSocio_ID"));
				soc.setNombre(rs.getString("Nombre"));
				socio.add(soc);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS SOCIOS "+ e.getMessage());
			e.printStackTrace();
		}
		return socio;
	}
	public boolean guardarSocio(TipoSocio soc)
	{
		boolean guardado = false;
		try 
		{
			this.listarSocios();
			rs.moveToInsertRow();
			rs.updateString("Nombre", soc.getNombre());
			rs.updateBoolean("Estado", soc.isEstado());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR SOCIO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	public boolean eliminarSocio(TipoSocio soc) throws SQLException
	{
		
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_TipoSocio set estado = 0 where T_TipoSocio_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, soc.getT_TipoSocio_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarTipoSocio(TipoSocio ts) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_TipoSocio set estado = 1 where T_TipoSocio_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, ts.getT_TipoSocio_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarSocio(TipoSocio soc)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_TipoSocio set Nombre = ? where T_TipoSocio_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, soc.getNombre());
			ps.setInt(		2, soc.getT_TipoSocio_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
	
}
