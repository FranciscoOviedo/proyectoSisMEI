package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.CriterioADIB;;

public class DTCriterioADIB {
	
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<CriterioADIB> listaCriterioADIB()
	{
		ArrayList<CriterioADIB> criterioBienestar = new ArrayList<CriterioADIB>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_CriterioBienestar where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				CriterioADIB Crit = new CriterioADIB(); 
				Crit.setT_CriterioBienestar_ID(rs.getInt("T_CriterioBienestar_ID"));
				Crit.setNombre(rs.getString("nombre"));
				Crit.setDescripcion(rs.getString("Descripcion"));
				Crit.setCategoria1(rs.getString("categoria1"));
				Crit.setCategoria2(rs.getString("categoria2"));
				Crit.setCategoria3(rs.getString("categoria3"));
				Crit.setCategoria4(rs.getString("categoria4"));
				Crit.setCategoria5(rs.getString("categoria5"));
				Crit.setEstado(rs.getBoolean("estado"));
				criterioBienestar.add(Crit);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS CRITERIOS "+ e.getMessage());
			e.printStackTrace();
		}
		return criterioBienestar;
	}
	
	public boolean guardarCriterioADIB(CriterioADIB Crit)
	{
		boolean guardado = false;
		try 
		{
			this.listaCriterioADIB();
			rs.moveToInsertRow();
			rs.updateString("Nombre", Crit.getNombre());
			rs.updateString("Descripcion", Crit.getDescripcion());
			rs.updateString("Categoria1", Crit.getCategoria1());
			rs.updateString("Categoria2", Crit.getCategoria2());
			rs.updateString("Categoria3", Crit.getCategoria3());
			rs.updateString("Categoria4", Crit.getCategoria4());
			rs.updateString("Categoria5", Crit.getCategoria5());
			rs.updateBoolean("Estado", Crit.isEstado());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR CRITERIO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarCriterioADIB(CriterioADIB crit) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_MacroProyecto set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_CriterioBienestar set Estado = 0 where T_CriterioBienestar_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, crit.getT_CriterioBienestar_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR CRITERIO "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarCriterioADIB(CriterioADIB crit)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_MacroProyecto set estado = 2, .......");
		String sql = ("UPDATE T_CriterioBienestar set Nombre = ?, Descripcion = ?, Categoria1 = ?, Categoria2 = ?, Categoria3 = ?, Categoria4 = ?, Categoria5 = ? where T_CriterioBienestar_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, crit.getNombre());
			ps.setString(	2, crit.getDescripcion());
			ps.setString(   3, crit.getCategoria1());
			ps.setString(   4, crit.getCategoria2());
			ps.setString(   5, crit.getCategoria3());
			ps.setString(   6, crit.getCategoria4());
			ps.setString(   7, crit.getCategoria5());
			ps.setInt(		8, crit.getT_CriterioBienestar_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR CRITERIO "+e.getMessage());
			
		}
		return modificado;
	}
}
