package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.ActividadProyecto;
import vistas.V_Info_ActividadProyecto;

public class DTActividadProyecto {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<V_Info_ActividadProyecto> listarInfoActividadProyectoInactivos()
	{
		ArrayList<V_Info_ActividadProyecto> infoActividadesProyectos = new ArrayList<V_Info_ActividadProyecto>();
		String sql = ("SELECT * FROM V_Info_ActividadProyecto where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_ActividadProyecto ia = new V_Info_ActividadProyecto();
				ia.setT_Actividad_ID(rs.getInt("T_Actividad_ID"));
				ia.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				ia.setT_Formador_ID(rs.getInt("T_Formador_ID"));
				ia.setFecha(rs.getDate("Fecha"));
				ia.setNombre(rs.getString("Nombre"));
				ia.setDescripcion(rs.getString("Descripcion"));
				ia.setEstimado(rs.getFloat("Estimado"));
				ia.setEstado(rs.getBoolean("Estado"));
				ia.setNombreFormador(rs.getString("NombreFormador"));
				ia.setNombreMicroProyecto(rs.getString("NombreMicroProyecto"));
				infoActividadesProyectos.add(ia);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS ACTIVIDADES DE PROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return infoActividadesProyectos;
	}
	
	public ArrayList<V_Info_ActividadProyecto> listarInfoActividadProyecto()
	{
		ArrayList<V_Info_ActividadProyecto> infoActividadesProyectos = new ArrayList<V_Info_ActividadProyecto>();
		String sql = ("SELECT * FROM V_Info_ActividadProyecto where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Info_ActividadProyecto ia = new V_Info_ActividadProyecto();
				ia.setT_Actividad_ID(rs.getInt("T_Actividad_ID"));
				ia.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				ia.setT_Formador_ID(rs.getInt("T_Formador_ID"));
				ia.setFecha(rs.getDate("Fecha"));
				ia.setNombre(rs.getString("Nombre"));
				ia.setDescripcion(rs.getString("Descripcion"));
				ia.setEstimado(rs.getFloat("Estimado"));
				ia.setEstado(rs.getBoolean("Estado"));
				ia.setNombreFormador(rs.getString("NombreFormador"));
				ia.setApellidoFormador(rs.getString("ApellidoFormador"));
				ia.setNombreMicroProyecto(rs.getString("NombreMicroProyecto"));
				infoActividadesProyectos.add(ia);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS ACTIVIDADES DE PROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return infoActividadesProyectos;
	}
	
	public ArrayList<ActividadProyecto> listarActividadProyectoInactivos()
	{
		ArrayList<ActividadProyecto> actividadesProyectos = new ArrayList<ActividadProyecto>();
		String sql = ("SELECT * FROM T_ActividadProyecto where estado = 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				ActividadProyecto a = new ActividadProyecto();
				a.setT_Actividad_ID(rs.getInt("T_Actividad_ID"));
				a.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				a.setT_Formador_ID(rs.getInt("T_Formador_ID"));
				a.setFecha(rs.getDate("Fecha"));
				a.setNombre(rs.getString("Nombre"));
				a.setDescripcion(rs.getString("Descripcion"));
				a.setEstimado(rs.getFloat("Estimado"));
				a.setEstado(rs.getBoolean("Estado"));
				actividadesProyectos.add(a);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS ACTIVIDADES DE PROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return actividadesProyectos;
	}
	
	public ArrayList<ActividadProyecto> listarActividadProyecto()
	{
		ArrayList<ActividadProyecto> actividadesProyectos = new ArrayList<ActividadProyecto>();
		String sql = ("SELECT * FROM T_ActividadProyecto where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				ActividadProyecto a = new ActividadProyecto();
				a.setT_Actividad_ID(rs.getInt("T_Actividad_ID"));
				a.setT_MicroProyecto_ID(rs.getInt("T_MicroProyecto_ID"));
				a.setT_Formador_ID(rs.getInt("T_Formador_ID"));
				a.setFecha(rs.getDate("Fecha"));
				a.setNombre(rs.getString("Nombre"));
				a.setDescripcion(rs.getString("Descripcion"));
				a.setEstimado(rs.getFloat("Estimado"));
				a.setEstado(rs.getBoolean("Estado"));
				actividadesProyectos.add(a);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LAS ACTIVIDADES DE PROYECTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return actividadesProyectos;
	}
	
	public boolean guardarActividadProyecto(ActividadProyecto a)
	{
		boolean guardado = false;
		try 
		{
			this.listarActividadProyecto();
			rs.moveToInsertRow();
			if(a.getT_MicroProyecto_ID()==0){
				rs.updateNull("T_MicroProyecto_ID");
			}else{
				rs.updateInt("T_MicroProyecto_ID", a.getT_MicroProyecto_ID());
			}
			
			rs.updateInt("T_Formador_ID", a.getT_Formador_ID());
			rs.updateDate("Fecha", new java.sql.Date(a.getFecha().getTime()));
			rs.updateString("Nombre", a.getNombre());
			rs.updateString("Descripcion", a.getDescripcion());
			rs.updateFloat("Estimado", a.getEstimado());
			rs.updateBoolean("Estado", a.isEstado());
			
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR ACTIVIDAD PROYECTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarActividadProyecto(ActividadProyecto a) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_ActividadProyecto set estado = 0 where T_Actividad_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, a.getT_Actividad_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarActividadProyecto(ActividadProyecto a) throws SQLException
	{
		boolean restaurado = false;
		PreparedStatement ps;
		String sql = ("Update T_ActividadProyecto set estado = 1 where T_Actividad_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, a.getT_Actividad_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarActividadProyecto(ActividadProyecto a)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_ActividadProyecto set T_MicroProyecto_ID = ?, T_Formador_ID=?, Fecha = ?, Nombre=?, Descripcion = ?, Estimado=?, Estado = ? where T_Actividad_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			if(a.getT_MicroProyecto_ID()==0){
				ps.setNull(		1, java.sql.Types.INTEGER);
			}else{
				ps.setInt(		1, a.getT_MicroProyecto_ID());
			}
			
			ps.setInt(		2, a.getT_Formador_ID());
			ps.setDate(		3, new java.sql.Date(a.getFecha().getTime()));
			ps.setString(	4, a.getNombre());
			ps.setString(	5, a.getDescripcion());
			ps.setFloat(	6, a.getEstimado());
			ps.setBoolean(	7, a.isEstado());
			ps.setInt(		8, a.getT_Actividad_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
