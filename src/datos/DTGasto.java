
package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.Gasto;
import vistas.V_Gasto_TipoGasto;

public class DTGasto {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<V_Gasto_TipoGasto> listarVistaGasto_TipoGasto(int idActividad)
	{
		ArrayList<V_Gasto_TipoGasto> gastos = new ArrayList<V_Gasto_TipoGasto>();
		String sql = ("SELECT * FROM V_Gasto_TipoGasto where EstadoGasto <> 0 and T_Actividad_ID = ?");
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			ps.setInt(1, idActividad);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				V_Gasto_TipoGasto g = new V_Gasto_TipoGasto();
				g.setNombreTipoGasto(rs.getString("NombreTipoGasto"));
				g.setEstadoTipoGasto(rs.getBoolean("EstadoTipoGasto"));
				g.setT_Gasto_ID(rs.getInt("T_Gasto_ID"));
				g.setDescripcionGasto(rs.getString("DescripcionGasto"));
				g.setCantidad(rs.getFloat("Cantidad"));
				g.setEstadoGasto(rs.getBoolean("EstadoGasto"));
				g.setT_TipoGasto_ID(rs.getInt("T_TipoGasto_ID"));
				g.setT_Actividad_ID(rs.getInt("T_Actividad_ID"));
				gastos.add(g);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS GASTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return gastos;
	}
	
	public ArrayList<Gasto> listarGasto()
	{
		ArrayList<Gasto> gastos = new ArrayList<Gasto>();
		String sql = ("SELECT * FROM T_Gasto where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				Gasto g = new Gasto();
				g.setT_Gasto_ID(rs.getInt("T_Gasto_ID"));
				g.setT_Actividad_ID(rs.getInt("T_Actividad_ID"));
				g.setT_TipoGasto_ID(rs.getInt("T_TipoGasto_ID"));
				g.setDescripcion(rs.getString("Descripcion"));
				g.setCantidad(rs.getFloat("Cantidad"));
				gastos.add(g);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS GASTOS "+ e.getMessage());
			e.printStackTrace();
		}
		return gastos;
	}
	
	public boolean guardarGasto(Gasto g)
	{
		boolean guardado = false;
		try 
		{
			this.listarGasto();
			rs.moveToInsertRow();
			rs.updateInt("T_Actividad_ID", g.getT_Actividad_ID());
			rs.updateInt("T_TipoGasto_ID", g.getT_TipoGasto_ID());
			rs.updateString("Descripcion", g.getDescripcion());
			rs.updateFloat("Cantidad", g.getCantidad());
			rs.updateBoolean("Estado", g.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR GASTO "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarGasto(Gasto g) throws SQLException
	{
		boolean eliminado = false;
		PreparedStatement ps;
		String sql = ("Update T_Gasto set estado = 0 where T_Gasto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, g.getT_Gasto_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean restaurarGasto(Gasto g) throws SQLException
	{
		boolean restaurado = false;
		PreparedStatement ps;
		String sql = ("Update T_Gasto set estado = 1 where T_Gasto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, g.getT_Gasto_ID());
			ps.executeUpdate();
			restaurado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return restaurado;
	}
	
	public boolean modificarGasto(Gasto g)
	{
		boolean modificado = false;
		PreparedStatement ps;
		String sql = ("UPDATE T_Gasto set T_Actividad_ID=?, T_TipoGasto_ID=?, Descripcion=?, Cantidad=? where T_Gasto_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(		1, g.getT_Actividad_ID());
			ps.setInt(		2, g.getT_TipoGasto_ID());
			ps.setString(	3, g.getDescripcion());
			ps.setFloat(	4, g.getCantidad());
			ps.setInt(		5, g.getT_Gasto_ID());
			
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}
}
