package datos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import entidades.TipoReunion;;

public class DTTipoReunion {
	Conexion c = Conexion.getInstance();
	Connection cn = Conexion.getConnection();
	ResultSet rs = null;
	
	public ArrayList<TipoReunion> listarTipoReunion()
	{
		ArrayList<TipoReunion> tipoReuniones= new ArrayList<TipoReunion>();
		//Si su tabla tiene auitoria usar: where estado <> 3
		//String sql = ("SELECT * FROM seguridad.usuario where estado <> 3");
		String sql = ("SELECT * FROM T_TipoReunion where estado <> 0");
		
		try 
		{
			PreparedStatement ps = cn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE, ResultSet.HOLD_CURSORS_OVER_COMMIT);
			rs = ps.executeQuery();
			
			while(rs.next())
			{
				TipoReunion tr = new TipoReunion();
				tr.setT_TipoReunion_ID(rs.getInt("T_TipoReunion_ID"));
			    tr.setNombre(rs.getString("Nombre"));
				tr.setEstado(rs.getBoolean("Estado"));
				tipoReuniones.add(tr);
			}
		} 
		catch (Exception e) 
		{
			System.out.println("DATOS: ERROR AL OBTENER LOS TIPOS DE REUNIONES"+ e.getMessage());
			e.printStackTrace();
		}
		return tipoReuniones;
	}
	
	public boolean guardarTipoReunion(TipoReunion tr)
	{
		boolean guardado = false;
		try 
		{
			this.listarTipoReunion();
			rs.moveToInsertRow();
			rs.updateInt("T_TipoReunion_ID", tr.getT_TipoReunion_ID());
			rs.updateString("Nombre", tr.getNombre());
			rs.updateBoolean("Estado", tr.isEstado());
			rs.insertRow();
			rs.moveToCurrentRow();
			guardado = true;
		} 
		catch (Exception e) 
		{
			System.err.println("ERROR AL GUARDAR EL TIPO DE REUNI�N "+e.getMessage());
			e.printStackTrace();
		}
		
		return guardado;
	}
	
	public boolean eliminarTipoReunion(TipoReunion tr) throws SQLException
	{
		/*
		 * Estados para tablas con auditoria
		 * 1 = agregado
		 * 2 = modificado
		 * 3 = eliminado
		 *  */
		boolean eliminado = false;
		PreparedStatement ps;
		//Si su tabla tiene auditoria usar: set estado = 3
		//String sql = ("Update T_Formador set estado = 3 where T_Formador_ID = ?");
		String sql = ("Update T_TipoReunion set estado = 0 where T_TipoReunion_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setInt(1, tr.getT_TipoReunion_ID());
			ps.executeUpdate();
			eliminado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL ELIMINAR "+e.getMessage());
		}
		return eliminado;
	}
	
	public boolean modificarTipoReunion(TipoReunion tr)
	{
		boolean modificado = false;
		PreparedStatement ps;
		//Para los que tengan auditoria tienen que agregar set estado = 2
		//String sql = ("UPDATE T_Formador set estado = 2, .......");
		String sql = ("UPDATE T_TipoReunion set  Nombre = ?, Estado=? where T_TipoReunion_ID = ?");
		try 
		{
			ps = cn.prepareStatement(sql);
			ps.setString(	1, tr.getNombre());
			ps.setBoolean(	2, tr.isEstado());
			ps.setInt(		3, tr.getT_TipoReunion_ID());
			ps.executeUpdate();
			modificado = true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			System.err.println("ERROR AL MODIFICAR "+e.getMessage());
			
		}
		return modificado;
	}

}
