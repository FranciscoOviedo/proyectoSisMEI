<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">SISMEI</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Herramientas">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-briefcase"></i>
            <span class="nav-link-text">Herramientas</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="${pageContext.request.contextPath}/view/herramientaADIB/listaHerramientaADIB.jsp">Analisis Diferencial del Bienestar (ADIB)</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/herramientaADIB/listaCriterioBienestar.jsp">Criterio de Bienestar</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/herramientaCC/listaHerramientaCC.jsp">Cambio Colectivo (CC)</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/herramientaCI/listaHerramientaCI.jsp">Cambio Individual (CI)</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/categoriaCICC/listaCategoriaCICC.jsp">Categoria CI / CC</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Proyectos">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti1" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file-o"></i>
            <span class="nav-link-text">Proyectos</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti1">
            <li>
              <a href="${pageContext.request.contextPath}/view/proyectos/listaMacroProyecto.jsp">Macro Proyectos</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/proyectos/listaMicroProyecto.jsp">Micro Proyectos</a>
            </li>
          </ul>
        </li>
         <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Familia Kolping">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-handshake-o "></i>
            <span class="nav-link-text">Familia Kolping</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti2">
            <li>
              <a href="${pageContext.request.contextPath}/view/Familias/listadoDiocesanas.jsp">Diocesanas</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/Familias/listadoFamilias.jsp">Familias</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/miembros/listaMiembro.jsp">Miembros</a>
            </li>
             <li>
              <a href="${pageContext.request.contextPath}/view/cargos/listaCargo.jsp">Cargos</a>
            </li>
             <li>
              <a href="${pageContext.request.contextPath}/view/Cuotas/listaCuota.jsp">Cuotas</a>
            </li>
             <li>
              <a href="${pageContext.request.contextPath}/view/TipoSocio/listaSocio.jsp">Socios</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Formadores">
          <a class="nav-link" href="${pageContext.request.contextPath}/view/formador/listaFormador.jsp">
            <i class="fa fa-fw fa-id-card-o"></i>
            <span class="nav-link-text">Formadores</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Reuniones">
          <a class="nav-link" href="${pageContext.request.contextPath}/view/Reuniones/listaReunion.jsp">
            <i class="fa fa-fw fa-check-square-o"></i>
            <span class="nav-link-text">Reuniones</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Actividades">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti3" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa fa-list"></i>
            <span class="nav-link-text">Actividades</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti3">
            <li>
              <a href="${pageContext.request.contextPath}/view/actividad/listaActividad.jsp">Actividades</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/tipoGasto/listaTipoGasto.jsp">Tipos de Gasto</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Seguridad">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti4" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa fa-lock"></i>
            <span class="nav-link-text">Seguridad</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti4">
            <li>
              <a href="${pageContext.request.contextPath}/view/Seguridad/listaUsuarios.jsp">Usuarios</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/view/Seguridad/listaRol.jsp">Roles</a>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti5" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text">Menu Levels</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti5">
            <li>
              <a href="#">Second Level Item</a>
            </li>
            <li>
              <a href="#">Second Level Item</a>
            </li>
            <li>
              <a href="#">Second Level Item</a>
            </li>
            <li>
              <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti6">Third Level</a>
              <ul class="sidenav-third-level collapse" id="collapseMulti6">
                <li>
                  <a href="#">Third Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level Item</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>

      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Cerrar Sesi�n</a>
        </li>
      </ul>
    </div>
   </nav>