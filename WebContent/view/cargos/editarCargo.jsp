<%@page import="entidades.Cargo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTCargo"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
   <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
      <div class="container-fluid">
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
        
  
  
  
  
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="../../index.html">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
              <a href="listaCargo.jsp">Cargo</a>
          </li>
          <li class="breadcrumb-item active">Editar Cargo</li>
        </ol>
        
        <h1>Editar Cargo</h1>
        <hr class="mt-2">
        
        <div class="item_container card-body">
           <form id="formCargo" class="form" method="POST" action="${pageContext.request.contextPath}/SLeditarCargo">
           <%
					ArrayList<Cargo> listaCargo = (ArrayList<Cargo>) session.getAttribute("ListaCargo");
					String idRequestCargo = request.getParameter("id");
					Cargo ts = null;
					int idCargo= 0;
					try {
						idCargo = Integer.parseInt(idRequestCargo);
					} catch (Exception e) {
						idCargo= 0;
						e.printStackTrace();
						e.getMessage();
					}
					for (Cargo tempTS : listaCargo) {
						if (tempTS.getT_Cargo_ID() == idCargo) {
							ts = tempTS;
							break;
						}
					}
			%>
			 <input type="text" name="T_Cargo_ID" id="T_Cargo_ID" value="<%=ts.getT_Cargo_ID()%>" hidden="true">
              <div class="form-group col-sm-6 col-md-6">
                  <label class="control-label" for="cargo">Cargo:</label>
                  <input type="text" class="form-control" id="Nombre" name="Nombre" value="<%=ts.getNombre()%>" placeholder="Ingresar Cargo" required>
              </div>
                
                <div class="form-group row">
                    <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Editar</button></div>
                    <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-danger">Cancelar</button></div>
                </div>
          </form>
        </div>
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
          
        
      </div>
      <!-- /.container-fluid-->
      <!-- /.content-wrapper-->
      <footer class="sticky-footer">
        <div class="container">
          <div class="text-center">
            <small>Copyright � Your Website 2018</small>
          </div>
        </div>
      </footer>
      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
      </a>
      <!-- Logout Modal-->
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn btn-primary" href="login.html">Logout</a>
            </div>
          </div>
        </div>
      </div>
  
      
  
     <script>
      function setup(){
        $('#date-picker').datepicker({
          format: 'mm-dd-yyyy',
          startDate: '03-14-2013'
        });
      }
     </script>
  
      <!-- Bootstrap core JavaScript-->
      <script src="../../vendor/jquery/jquery.min.js"></script>
      <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- Core plugin JavaScript-->
      <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
      <!-- Page level plugin JavaScript-->
      <script src="../../vendor/datatables/jquery.dataTables.js"></script>
      <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
      <!-- Custom scripts for all pages-->
      <script src="../../js/sb-admin.min.js"></script>
      <!-- Custom scripts for this page-->
      <script src="../../js/tablesLanguage.js"></script>
      <script src="../../js/jquery.mask.js"></script>
      <script src="../../js/bootstrap-datepicker.js"></script>
    </div>
      </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright � Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
  </div>
</body>

</html>
