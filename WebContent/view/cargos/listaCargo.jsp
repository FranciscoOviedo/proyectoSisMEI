<%@page import="entidades.Cargo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTCargo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
   <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">






















      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Cargo</li>
      </ol>
      <div class="row">
        <div class="col-sm-9"><h1>Lista de cargos</h1></div>
        <div class="col-12 col-sm-3 text-secondary text-md-right">
          <a href="crearCargo.jsp" class="btn btn-primary btn-block" role="button"><i class="fa fa-fw fa-plus"></i>Nuevo</a> 
        </div>
    
      </div>
      <hr class="mt-2">
      
      <div class="table-responsive item_container card-body">
			<div class="form-check my-3">
				<label> <input type="checkbox" name="check" onchange="toggleInactivos(this)"> <span class="label-text unselectable">Mostrar Inactivos</span> </label>
			</div>
			<!-- Tabla de Activos -->
        	<div class="toggable">
	        	<table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0" >
	            <thead>
	                <tr class="thead-dark">
	                  <th scope="col">#</th>
	                  <th scope="col">Cargo</th>
	                  <th scope="col">Opciones</th>
	                </tr>
	              </thead>
	              <tbody>
	               <%
	               DTCargo dtcar = new DTCargo();
					ArrayList<Cargo> listaCargo = new ArrayList<Cargo>();
					listaCargo = dtcar.listarCargos();
	
					//Guardamos esa lista en una Sesión
					ArrayList<Cargo> listaCar = null;
					listaCar = dtcar.listarCargos();
					session.setAttribute("ListaCargo", listaCargo);
					int number = 1;
					
					for (Cargo car : listaCargo) {
				%>
	              <tr>
	                <td  width="50px;"><%=number%></td>
	                <td><%= car.getNombre() %></td>
					<td  width=class="text-right">
						<a class="btn btn-sm btn-light d-incline-block" href=<%out.print("editarCargo.jsp?id=" + car.getT_Cargo_ID());%>> <i class="fa fa-fw fa-pencil"></i></a>
						<a class="btn btn-sm btn-danger d-incline-block" href="" data-toggle="modal" data-target="#modalEliminar" data-cargoid=<%out.print(car.getT_Cargo_ID());%>> <i class="fa fa-fw fa-trash"></i></a>
					</td>
	              </tr>
	              <%
	              number = number + 1;
					}
				  %>
	              </tbody>
	        	</table>
      		</div>
      		
      		
      		<!-- Tabla de Inactivos -->
        	<div class="toggable" style="display: none;">
	        	<table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0" >
	            <thead>
	                <tr class="thead-dark">
	                  <th scope="col">#</th>
	                  <th scope="col">Cargo</th>
	                  <th scope="col">Opciones</th>
	                </tr>
	              </thead>
	              <tbody>
	               <%
					listaCargo = dtcar.listarCargosInacivos();
					number = 1;
					
					for (Cargo car : listaCargo) {
				%>
	              <tr>
	                <td  width="50px;"><%=number%></td>
	                <td><%= car.getNombre() %></td>
					<td width=class="text-right">
						<a class="btn btn-sm btn-light d-incline-block" href="" data-toggle="modal" data-target="#modalRestaurar" data-cargoid=<%out.print(car.getT_Cargo_ID());%>> <i class="fa fa-fw fa-arrow-up"></i></a>
					</td>
	              </tr>
	              <%
	              number = number + 1;
					}
				  %>
	              </tbody>
	        </table>
      	</div>
      </div>
      
        <!-- Modal Eliminar -->
		<div id="modalEliminar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Dar de Baja a Cargo</h4>
					</div>
					<div class="modal-body">
						<p>¿Esta seguro que desea dar de baja a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Dar de baja</button></a>
					</div>
				</div>
			</div>
		</div>
        
        
        <!-- Modal Restaurar -->
		<div id="modalRestaurar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Restaurar Cargo</h4>
					</div>
					<div class="modal-body">
						<p>¿Está seguro que desea restaurar a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Restaurar</button></a>
					</div>
				</div>
			</div>
		</div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Ã</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script>
    	$(document).ready(function () {
		   $('#modalEliminar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('cargoid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarCargo?id='+recipient);
		   });
		   $('#modalRestaurar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('cargoid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLrestaurarCargo?id='+recipient);
		   });
	    });
		function toggleInactivos(element)
		{
			$(".toggable").toggle();
			$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
		}
    </script>
  </div>
</body>

</html>
