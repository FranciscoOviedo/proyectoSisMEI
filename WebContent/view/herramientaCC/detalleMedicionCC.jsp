<%@page import="entidades.HerramientaCC"%>
<%@page import="vistas.V_Info_HerramientaCC"%>
<%@page import="java.util.ArrayList"%>
<%@page import="vistas.V_Info_ObjetivoCC"%>
<%@page import="datos.DTObjetivoCC"%>
<%@page import="datos.DTHerramientaCC"%>
<%@page import="entidades.MedicionCC"%>
<%@page import="datos.DTMedicionCC"%>
<%@page import="datos.DTDetalleMedicionCC"%>
<%@page import="vistas.V_Miembro_DetalleMedicionCC"%>
<%@page import="entidades.DetalleMedicionCC"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>SisMEI</title>
	<link rel="icon" href="../../img/LogoSisMEI-low-res.png">
	<!-- Bootstrap core CSS-->
	<link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Custom styles for this template-->
	<link href="../../css/sb-admin.css" rel="stylesheet">
	<link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
	<!-- Navigation-->
	<%@include file="../../navbar.jsp" %>
	<div class="content-wrapper">
		<div class="container-fluid">






















     	<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../../index.jsp">Dashboard</a></li>
			<li class="breadcrumb-item"><a href="listaHerramientaCC.jsp">Herramienta CC</a></li>
			<li class="breadcrumb-item"><a href="detalleHerramientaCC.jsp">Detalle Herramienta CC</a></li>
			<li class="breadcrumb-item active">Detalle Medicion CC</li>
		</ol>
		<div class="row">
			<div class="col">
				<h1>Herramienta CC</h1>
			</div>
		</div>
		<hr class="mt-2">

		<div class="item_container card-body">
			
			
			<%
				String idRequestMedicionCC = request.getParameter("idMedicion");
				V_Info_HerramientaCC hc = null;
				int idMedicionCC = 0;
				try {
					idMedicionCC = Integer.parseInt(idRequestMedicionCC);
				} catch (Exception e) {
					idMedicionCC = 0;
					e.printStackTrace();
					e.getMessage();
				}
				DTMedicionCC dmc = new DTMedicionCC();
				MedicionCC mc = dmc.obtenerMedicionCC(idMedicionCC);
				
				
			%>
			
			
			<!-- Formulario de Medicion CC -->
			<div>
				<form id="formMedicionCC" class="form formMedicionCC" method="POST" action="${pageContext.request.contextPath}/SLactualizarDetalleMedicionCC">
					<input hidden=true type="text" name="T_MedicionCC_ID" value="<%= idMedicionCC %>">
					<table class="table table-bordered table-hover table-responsive" width="100%" item-width="100%" cellspacing="0">
						<thead class="thead-light">
							<tr>
								<th scope="col"></th>
								<%
						           	DTObjetivoCC dtoc = new DTObjetivoCC();
									ArrayList<V_Info_ObjetivoCC> listaInfoObjetivoCC = new ArrayList<V_Info_ObjetivoCC>();
									listaInfoObjetivoCC = dtoc.listarInfoObjetivoCC(mc.getT_HerramientaCC_ID());
									
									for(int i=0; i<listaInfoObjetivoCC.size(); i++)
									{
										int categoria = listaInfoObjetivoCC.get(i).getT_Categoria_ID();
										int cantColspan = 1;
										boolean repite = true;
										while(i<listaInfoObjetivoCC.size()-1 && repite){
											if(categoria==listaInfoObjetivoCC.get(i+1).getT_Categoria_ID()){
												i++;
												cantColspan++;
											} else {
												repite = false;
											}
										}
								%>
								<th scope="col" colspan="<%=cantColspan%>"><%= listaInfoObjetivoCC.get(i).getNombreCategoria() %></th>
								<%
									}
								%>
							</tr>
							<tr>
								<th scope="col">Nombre Miembro</th>
								<%
									for(V_Info_ObjetivoCC ioc : listaInfoObjetivoCC)
									{
								%>
								<th scope="col" style="min-width:150px"><%= ioc.getNombre() %></th>
								<%
									}
								%>
							</tr>
						</thead>
						<tbody>
							<%
					           	DTDetalleMedicionCC dtmc = new DTDetalleMedicionCC();
								ArrayList<V_Miembro_DetalleMedicionCC> listaMiembroMedicionCC = new ArrayList<V_Miembro_DetalleMedicionCC>();
								listaMiembroMedicionCC = dtmc.listarMiembroMedicionCC(mc.getT_MedicionCC_ID(), 2, listaInfoObjetivoCC);
								
								for(V_Miembro_DetalleMedicionCC mdmc : listaMiembroMedicionCC)
								{
							%>
							<tr>
								<td  width="50px;"><%= mdmc.getNombre() %></td>
								<%
									for(DetalleMedicionCC dmcvalue : mdmc.getMediciones())
									{
								%>
								<td>
									<input class="form-control" type="number" min="0" max="5" step="1" name="Valor" value="<%= dmcvalue.getValor() %>">
									<input hidden=true type="text" name="DetalleMedicionCC_ID" value="<%= dmcvalue.getT_DetalleMedicionCC_ID() %>">
									<input hidden=true type="text" name="Miembro_ID" value="<%= dmcvalue.getT_Miembro_ID() %>">
									<input hidden=true type="text" name="ObjetivoCC_ID" value="<%= dmcvalue.getT_ObjetivoCC_ID() %>">
								</td>
								<%
									}
								%>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					<div class="form-group row">
						<div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Guardar cambios</button></div>
						<div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
					</div>
				</form>
			</div>
		</div>

		



















	</div>
    <%@include file="../../footer.jsp"%>
    
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLeftFixed.js"></script>
  </div>
</body>

</html>
