<%@page import="entidades.HerramientaCC"%>
<%@page import="vistas.V_Info_HerramientaCC"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTHerramientaCC"%>
<%@page import="entidades.ObjetivoCC"%>
<%@page import="datos.DTObjetivoCC"%>
<%@page import="entidades.Familia"%>
<%@page import="datos.DTFamilia"%>
<%@page import="entidades.MicroProyecto"%>
<%@page import="datos.DTMicroProyecto"%>
<%@page import="entidades.Categoria"%>
<%@page import="datos.DTCategoria"%>
<%@page import="vistas.V_Info_ObjetivoCC"%>

<%@page import="entidades.MedicionCC"%>
<%@page import="datos.DTMedicionCC"%>

<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>SisMEI</title>
<link rel="icon" href="../../img/LogoSisMEI-low-res.png">
<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="../../css/sb-admin.css" rel="stylesheet">
<link href="../../css/content.css" rel="stylesheet">
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaHerramientaCC.jsp">Herramienta CC</a>
        </li>
        <li class="breadcrumb-item active">Detalle Herramienta CC</li>
      </ol>
      
      <h1>Detalle Herramienta CC</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
          <%
			ArrayList<V_Info_HerramientaCC> listaHerramientaCC = (ArrayList<V_Info_HerramientaCC>) session.getAttribute("ListaHerramientaCC");
			String idRequestHerramientaCC = request.getParameter("id");
			V_Info_HerramientaCC hc = null;
			int idHerramientaCC = 0;
			try {
				idHerramientaCC = Integer.parseInt(idRequestHerramientaCC);
			} catch (Exception e) {
				idHerramientaCC = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (V_Info_HerramientaCC tempHC : listaHerramientaCC) {
				if (tempHC.getT_HerramientaCC_ID() == idHerramientaCC) {
					hc = tempHC;
					break;
				}
			}
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		%>
          
                <p><strong>Herramienta de Cambio Individual</strong></p>
				<p class="text-muted">Aplicado a la familia <strong class='text-nowrap'><%= hc.getNombreFamiliaKolping() %></strong> micro proyecto <strong class='text-nowrap'><%= hc.getNombreMicroProyecto() %></strong></p>
           		<p class="text-muted">Fecha de creacion <% out.print(format.format(hc.getFechaCreacion())); %> </p>
           
      </div>
        
        
        
        
        
        
        <!-- Tabla de Objetivos de Herramienta CC -->
        <div class="item_container card-body">
		<div class="row my-4">
			<h2 class="col mb-0">Objetivos:</h2>
			<button data-toggle="modal" data-target="#modalCrearObjetivoCC" class="col-auto mr-3 btn btn-success addnewrow"> <span class="fa fa-fw fa-plus"></span> </button>
		</div>

		<div class="table-responsive">
			<table class="table table-hover" id="tab_logic" cellspacing="0" width="100%">
				<thead>
					<tr class="thead">
						<th> <p>#</p> </th>
						<th> <p>Categoria</p> </th>
						<th> <div style="min-width:200px"> <p>Objetivo</p> </div> </th>
						<th> <p>Opciones</p> </th>
					</tr>
				</thead>
				<tbody>
				<%
		           	DTObjetivoCC dtoc = new DTObjetivoCC();
					ArrayList<V_Info_ObjetivoCC> listaInfoObjetivoCC = new ArrayList<V_Info_ObjetivoCC>();
					listaInfoObjetivoCC = dtoc.listarInfoObjetivoCC(idHerramientaCC);
					int number=1;
					
					for(V_Info_ObjetivoCC ioc : listaInfoObjetivoCC)
					{
				%>
					<tr>
						<td class="order"><% out.print(number); %></td>
						<td><span class="badge badge-secondary"><%= ioc.getNombreCategoria() %></span></td>
						<td><%= ioc.getNombre() %></td>
						<td width="100px">
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEditarObjetivoCC"
								data-objetivoccid="<%= ioc.getT_ObjetivoCC_ID() %>"
								data-categoriaccid="<%= ioc.getT_Categoria_ID() %>"
								data-nombre="<%= ioc.getNombre() %>"
								data-criterio1="<%= ioc.getCriterio1() %>"
								data-criterio2="<%= ioc.getCriterio2() %>"
								data-criterio3="<%= ioc.getCriterio3() %>"
								data-criterio4="<%= ioc.getCriterio4() %>"
								data-criterio5="<%= ioc.getCriterio5() %>">
								<i class="fa fa-fw fa-pencil"></i>
							</a>
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEliminarObjetivoCC" data-objetivoccid="<%= ioc.getT_ObjetivoCC_ID() %>"> <i class="fa fa-fw fa-minus"></i></a>
						</td>
					</tr>
				<%
						number++;
					}
					if(listaInfoObjetivoCC.size()==0){out.print("<tr><td colspan='4'>Aún no se han ingresado objetivos</td></tr>");}
				%>
				</tbody>
			</table>
		</div>
	  </div>
        
        
        
        <!-- Modal Crear ObjetivoCC -->
		<div id="modalCrearObjetivoCC" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Agregar un Objetivo CC</h4>
					</div>
					<form id="formObjetivoCC" class="form formObjetivoCC" method="POST" action="${pageContext.request.contextPath}/SLguardarObjetivoCC">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_Actividad_ID de la actividad que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_HerramientaCC_ID" id="T_HerramientaCC_ID" value="<%=idHerramientaCC%>" hidden="true">
							
							<div class="form-group">
								<label for="T_Categoria_ID" class="col-form-label">Categoria:</label>
								<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Categoria_ID" name="T_Categoria_ID">
								<%
									DTCategoria dtc = new DTCategoria();
									ArrayList<Categoria> listaCateg = dtc.listarCategorias();
									for(Categoria c: listaCateg)
									{
								%>
									<option value="<%= c.getT_Categoria_ID() %>"><%= c.getNombre() %></option>
								<%
									}
								%>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" for="Nombre">Objetivo:</label>
								<input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Ingrese la definición del objetivo">
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio1">Criterio 1:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio1" name="Criterio1" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 1 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio2">Criterio 2:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio2" name="Criterio2" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 2 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio3">Criterio 3:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio3" name="Criterio3" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 3 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio4">Criterio 4:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio4" name="Criterio4" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 4 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio5">Criterio 5:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio5" name="Criterio5" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 5 (Es opcional)"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Agregar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
        
        <!-- Modal Editar ObjetivoCC -->
		<div id="modalEditarObjetivoCC" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Editar un Objetivo CC</h4>
					</div>
					<form id="formEditarObjetivoCC" class="form formEditarObjetivoCC" method="POST" action="${pageContext.request.contextPath}/SLeditarObjetivoCC">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_HerramientaCC_ID de la Herramienta CC que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_ObjetivoCC_ID" id="T_ObjetivoCC_ID_MODIFICAR_OBJETIVO_CC" value="<%=idHerramientaCC%>" hidden="true">
							
							<!-- Este de aqui sirve para mandar el atributo T_HerramientaCC_ID de la Herramienta CC que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_HerramientaCC_ID" id="T_HerramientaCC_ID_MODIFICAR_OBJETIVO_CC" value="<%=idHerramientaCC%>" hidden="true">
							
							<div class="form-group">
								<label for="T_Categoria_ID" class="col-form-label">Categoria:</label>
								<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Categoria_ID_MODIFICAR_OBJETIVO_CC" name="T_Categoria_ID">
								<%
									for(Categoria c: listaCateg)
									{
								%>
									<option value="<%= c.getT_Categoria_ID() %>"><%= c.getNombre() %></option>
								<%
									}
								%>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label" for="Nombre_MODIFICAR_OBJETIVO_CC">Objetivo:</label>
								<input type="text" class="form-control" id="Nombre_MODIFICAR_OBJETIVO_CC" name="Nombre" placeholder="Ingrese la definición del objetivo">
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio1_MODIFICAR_OBJETIVO_CC">Criterio 1:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio1_MODIFICAR_OBJETIVO_CC" name="Criterio1" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 1 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio2_MODIFICAR_OBJETIVO_CC">Criterio 2:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio2_MODIFICAR_OBJETIVO_CC" name="Criterio2" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 2 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio3_MODIFICAR_OBJETIVO_CC">Criterio 3:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio3_MODIFICAR_OBJETIVO_CC" name="Criterio3" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 3 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio4_MODIFICAR_OBJETIVO_CC">Criterio 4:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio4_MODIFICAR_OBJETIVO_CC" name="Criterio4" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 4 (Es opcional)"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label " for="Criterio5_MODIFICAR_OBJETIVO_CC">Criterio 5:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="2" id="Criterio5_MODIFICAR_OBJETIVO_CC" name="Criterio5" maxlength="255" placeholder="Ingrese el criterio para poner un puntaje de 5 (Es opcional)"></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Editar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>
        
        
        
        
        
        <!-- Tabla de Medicion de Herramienta CC -->
        <div class="item_container card-body">
		<div class="row my-4">
			<h2 class="col mb-0">Mediciones:</h2>
			<button data-toggle="modal" data-target="#modalCrearMedicionCC" class="col-auto mr-3 btn btn-success"> <span class="fa fa-fw fa-plus"></span> </button>
		</div>

		<div class="table-responsive">
			<table class="table table-hover" id="tab_logic" cellspacing="0" width="100%">
				<thead>
					<tr class="thead">
						<th> <p>#</p> </th>
						<th> <p>Fecha de Medición</p> </th>
						<th> <p>Opciones</p> </th>
					</tr>
				</thead>
				<tbody>
				<%
		           	DTMedicionCC dtmc = new DTMedicionCC();
					ArrayList<MedicionCC> listaMedicionCC = new ArrayList<MedicionCC>();
					listaMedicionCC = dtmc.listarMedicionCC(idHerramientaCC);
					number=1;
					
					for(MedicionCC mc : listaMedicionCC)
					{
				%>
					<tr>
						<td class="order"><% out.print(number); %></td>
						<td><% out.print(format.format(mc.getFecha())); %></td>
						<td width="100px">
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEditarMedicionCC"
								data-medicionid="<%= mc.getT_MedicionCC_ID() %>"
								data-fecha="<%= mc.getFecha() %>">
								<i class="fa fa-fw fa-pencil"></i>
							</a>
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEliminarMedicionCC" data-objetivociid="<%= mc.getT_MedicionCC_ID() %>"> <i class="fa fa-fw fa-minus"></i></a>
						</td>
					</tr>
				<%
						number++;
					}
					if(listaMedicionCC.size()==0){out.print("<tr><td colspan='4'>Aún no se han ingresado objetivos</td></tr>");}
				%>
				</tbody>
			</table>
		</div>
	  </div>
        
        
        
        
		<!-- Modal Crear MedicionCC -->
		<div id="modalCrearMedicionCC" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Agregar una fecha de Medición CC</h4>
					</div>
					<form id="formMedicionCC" class="form formMedicionCC" method="POST" action="${pageContext.request.contextPath}/SLguardarMedicionCC">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_Herramienta_ID de la actividad que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_HerramientaCC_ID" id="T_HerramientaCC_ID" value="<%=idHerramientaCC%>" hidden="true">
							
							<div class="form-group">
								<label class="control-label" for="Fecha">Fecha de Realización:</label >
								<input type="text" class="form-control fechaMask" id="Fecha" name="Fecha" placeholder="Ingrese la fecha en que se realiza la medicion" required> 
							</div>
							
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Agregar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>
        
        
        
        
        
        
      
    </div>
    <%@include file="../../footer.jsp"%>

    
		<script src="../../vendor/jquery/jquery.min.js"></script>
		
		<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script><script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="../../js/jquery.chained.js"></script>
		<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap-select.js"></script>
		<script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
		<!-- Page level plugin JavaScript-->
		<script>
			//Para las Mascaras y el selectpicker
			$(document).ready(function() {
				$('.fechaMask').mask('00-00-0000');
				//Para los select de Familia y proyecto
				$("#T_MicroProyecto_ID").chained("#T_FamiliaKolping_ID");
				$('.selectpicker').selectpicker();
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				  $('.selectpicker').selectpicker('mobile');
				  $('.fechaMask').prop('readonly', true);
				}
			});
			//Para mostrar la cantidad de caracteres que faltan en los textarea
			var inputs = "textarea[maxlength]";
			$(document).on('keyup', "[maxlength]", function(e) {
				var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
				espan = este.prev('label').find('span');
				// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
				if (document.addEventListener
						&& !window.requestAnimationFrame) {
					if (remainingCharacters <= -1) {
						remainingCharacters = 0;
					}
				}
				espan.html(currentCharacters + "/" + maxlength);
			});
			//Para cambiar el idioma a español del date picker (Para seleccionar fechas)
			$.datepicker.regional['es'] = {
				closeText : 'Cerrar',
				prevText : '< Ant',
   				nextText: 'Sig >',
				currentText : 'Hoy',
				monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
				monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
				dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
				dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb' ],
				dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá' ],
				weekHeader : 'Sm',
				dateFormat : 'dd/mm/yy',
				firstDay : 1,
				isRTL : false,
				showMonthAfterYear : false,
				yearSuffix : ''
			};
			//Para que el datepicker funcione
			$.datepicker.setDefaults($.datepicker.regional['es']);
			$('#Fecha').datepicker({
				setDate : new Date(),
				dateFormat : 'dd-mm-yy',
				changeMonth : true,
				changeYear : true
			});
			//Esto es para cargar los datos del objetivo ci en el modal que se seleccione...
			$('#modalEditarObjetivoCC').on('show.bs.modal', function (event) {
				var button = $(event.relatedTarget)
				var objetivociid = button.data('objetivociid')
				var categoriaccid = button.data('categoriaccid')
				var nombre = button.data('nombre')
				var criterio1 = button.data('criterio1')
				var criterio2 = button.data('criterio2')
				var criterio3 = button.data('criterio3')
				var criterio4 = button.data('criterio4')
				var criterio5 = button.data('criterio5')
				var modal = $(this)
				modal.find('#T_ObjetivoCC_ID_MODIFICAR_OBJETIVO_CC').attr('value', objetivoccid);
				$('#T_Categoria_ID_MODIFICAR_OBJETIVO_CC').selectpicker('val', categoriaccid);	//Este es distinto por ser un selectpicker (El select que te permite buscar)
				modal.find('#Nombre_MODIFICAR_OBJETIVO_CC').attr('value', nombre);
				modal.find('#Criterio1_MODIFICAR_OBJETIVO_CC').text(criterio1); //Este es distinto por ser un textarea... el valor va entre los tags... no en el tag value
				modal.find('#Criterio2_MODIFICAR_OBJETIVO_CC').text(criterio2); //Este es distinto por ser un textarea... el valor va entre los tags... no en el tag value
				modal.find('#Criterio3_MODIFICAR_OBJETIVO_CC').text(criterio3); //Este es distinto por ser un textarea... el valor va entre los tags... no en el tag value
				modal.find('#Criterio4_MODIFICAR_OBJETIVO_CC').text(criterio4); //Este es distinto por ser un textarea... el valor va entre los tags... no en el tag value
				modal.find('#Criterio5_MODIFICAR_OBJETIVO_CC').text(criterio5); //Este es distinto por ser un textarea... el valor va entre los tags... no en el tag value
		   });
			
			//Esto es para resetear el formulario cuando se cierre el modal... antes si se escribia algo se cerraba y se volvia a abrir con otro... los datos seguian del anterior...
			$('#modalEditarObjetivoCC').on('hide.bs.modal', function () {
				$('#formEditarObjetivoCC').trigger("reset");  //Aqui poner el id del formulario de editar
			});
		</script>
	    <!-- Bootstrap core JavaScript-->
	    
	    
	    <!-- Core plugin JavaScript-->
	    
	    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
	    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
	    <!-- Custom scripts for all pages-->
	    <script src="../../js/sb-admin.min.js"></script>
	    <!-- Custom scripts for this page-->
	    <script src="../../js/tablesLanguage.js"></script>
	    <script src="../../js/jquery.mask.js"></script>
    
    
	</div>
</body>

</html>
