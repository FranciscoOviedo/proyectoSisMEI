<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Asistencia</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
 
 <!-- Navigation-->
  	<%@include file="../../navbar.jsp" %>
  	<!-- /Navigation-->
  	
  <!-- Page Content -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaReunion.jsp"> Reuni�n</a>
          </li>
        <li class="breadcrumb-item active">Asistencia</li>
      </ol>
       <!--/Breadcrumbs-->
        
      <div class="row">
        <div class="col"><h1>Lista de Asistencia</h1></div>
      </div>
      <hr class="mt-2">
      
      <div class="table-responsive item_container card-body">
        <table id="dataTable" class="table table-hover" cellspacing="0">
            <thead>
                <tr class="thead-dark">
                  <th scope="col">#</th>
                  <th scope="col">Nombre</th>
                  <th scope="col">C�dula</th>
                  <th scope="col">Edad</th>
                  <th scope="col">Sexo</th>
                  <th scope="col">Asistencia</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Francisco Juarez</td>
                  <td>001-040199-0000U</td>
                  <td>18</td>
                  <td>Masculino</td>
                  <td >
                      <label>
                        <input type="checkbox" name="check" checked> <span class="label-text unselectable"></span>
                      </label>
                   </td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>Francisco Juarez</td>
                    <td>001-040199-0000U</td>
                    <td>19</td>
                    <td>Masculino</td>
                    <td >
                        <label>
                          <input type="checkbox" name="check"><span class="label-text unselectable"></span>
                        </label>
                     </td>
                  </tr>
              </tbody>
        </table>
        <div class="form-group row mt-4">
          <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Guardar</button></div>
          <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-danger">Cancelar</button></div>
      </div>
      </div>
    </div>
     <!-- /Page Content -->
    
    <!-- footer-->
     <%@include file="../../footer.jsp" %>
    <!-- /footer-->
    
    <!-- Logout Modal-->
    <%@include file="../../logout.jsp" %>
    <!-- /Logout Modal-->
    
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
  </div>
</body>

</html>