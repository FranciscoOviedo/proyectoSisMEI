<%@page import="entidades.Reunion"%>
<%@page import="datos.DTReunion"%>
<%@page import="vistas.V_ReunionDetalle"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Descripci�n de Reuni�n</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  
    <!-- Navigation-->
  	<%@include file="../../navbar.jsp" %>
  	<!-- /Navigation-->
  	
  <!-- Page Content -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaReunion.jsp">Reuni�n</a>
        </li>
        <li class="breadcrumb-item active">Descripci�n Reuni�n</li>
      </ol>
      <!--/Breadcrumbs-->
      
      <div class="row">
        <div class="col"><h1>Descripcion de Reuni�n</h1></div>
      </div>
      <div class="item_container">
      
      <%
		ArrayList<V_ReunionDetalle> listaReunionesDetalle = (ArrayList<V_ReunionDetalle>) session.getAttribute("ListaReunionesDetalle");
		String idRequestReunion = request.getParameter("id");
		  V_ReunionDetalle rd = null;
		int idReunion = 0;
		try {
			idReunion = Integer.parseInt(idRequestReunion);
		} catch (Exception e) {
			idReunion = 0;
			e.printStackTrace();
			e.getMessage();
		}
		for (V_ReunionDetalle tempRD : listaReunionesDetalle) {
			if (tempRD.getT_Reunion_ID() == Integer.parseInt(idRequestReunion)) {
				rd = tempRD;
				break;
			}
		}
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	  %>
        <div class="card-body font-weight-bold text-truncate lead">
         <label><%=rd.getNombreFamilia() %></label> 
         <label class="inline-block float-right"><% out.print(rd.getNombreMunicipio()+", "+rd.getNombreDepartamento()); %></label> 
        </div>
        <hr class="my-0">
        <div class="card-body row">
            <div class="col-md-6 col-sm-6 card-body">
              <strong>Formador</strong>
              <hr class="my-0">
              <div class="text-secondary"><% out.print(rd.getNombreFormador()+" "+rd.getApellidoFormador()); %></div>
            </div>
            <div class="col-md-6 col-sm-6 card-body">
              <strong>Fecha:</strong>
              <hr class="my-0">
              <div class="text-secondary"><%= rd.getFecha() %></div>
            </div>
            <div class="col-md-auto col-sm-6 card-body">
              <strong>Tipo de Reuni�n:</strong>
              <hr class="my-0">
              <div class="text-secondary"><%= rd.getTipoReunion() %></div>
            </div>
            <div class="col-md-6 col-sm-6 card-body">
              <strong>Tema:</strong>
              <hr class="my-0">
              <div class="text-secondary"><%= rd.getTema() %></div>
            </div>
            <div class="col-md-6 col-sm-6 card-body">
              <strong>Descripci�n:</strong>
              <hr class="my-0">
              <div class="text-secondary"><%= rd.getDescripcion() %></div>
              
            </div>
            <div class="col-md-6 col-sm-6 card-body">
                <strong>Observaci�n:</strong>
                <hr class="my-0">
                <div class="text-secondary"><%= rd.getObservacion() %></div>
              </div>
          </div>
      </div>
    </div>
      <!-- /Page Content -->
    
    <!-- footer-->
    <%@include file="../../footer.jsp" %>
    <!-- /footer-->
    
    <!-- Logout Modal-->
    <%@include file="../../logout.jsp" %>
    <!-- /Logout Modal-->
   
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
  </div>
</body>
</html>