<%@page import="datos.DTFormador"%>
<%@page import="datos.DTTipoReunion"%>
<%@page import="datos.DTFamilia"%>
<%@page import="entidades.Formador"%>
<%@page import="entidades.TipoReunion"%>
<%@page import="entidades.Familia"%>
<%@page import="java.util.ArrayList"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  
  <title>Crear Reuni�n</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  
  <!--Sobreescribir -->
  <link href="../../css/content.css" rel="stylesheet"> 
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
  
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  
  
  	<!-- Navigation-->
  	<%@include file="../../navbar.jsp" %>
  	<!-- /Navigation-->
  
  <!-- Page Content -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaReunion.jsp">Reuni�n</a>
        </li>
        <li class="breadcrumb-item active">Crear Reuni�n</li>
      </ol>
       <!--/Breadcrumbs-->
       
      <h1>Crear Reuni�n</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form id="formReunion" class="form formReunion" method="POST" action="${pageContext.request.contextPath}/SLguardarReunion">
            <div class="row">
                <div class="form group col-sm-6 col-md-4">
                  <label class="control-label " for="formador">Formador:</label>
                  <select class="form-control selectpicker" data-live-search="true" id="T_Formador_ID" name="T_Formador_ID">
                <%
					DTFormador dtf = new DTFormador();
					ArrayList<Formador> listaFor = dtf.listarFormadores();
					for(Formador f: listaFor)
					{
				%>
						<option value="<%= f.getT_Formador_ID() %>"><% out.print(f.getNombre()+" "+f.getApellido()); %></option>
				<%
					}
				%>
                    
                  </select>
                </div>
              
            <div class="form-group col-sm-6 col-md-4">
              <label class="control-label" for="fechaReunion">Fecha de Reuni�n:</label>
              <input type="text" id="Fecha" class="form-control fechaMask" name="Fecha" title="La Fecha de Reuni�n es requerida" placeholder="Ingrese la fecha de Reuni�n" required> 
            </div>
            <div class="form-group col-sm-6 col-md-4">
                <label class="control-label" for="tipo">Tipo de Reuni�n:</label>
                <select class="form-control selectpicker" data-live-search="true" id="T_TipoReunion_ID" name="T_TipoReunion_ID">
                <%
					DTTipoReunion dtr = new DTTipoReunion();
					ArrayList<TipoReunion> listaTipoR = dtr.listarTipoReunion();
					for(TipoReunion tr: listaTipoR)
					{
				%>
						<option value="<%= tr.getT_TipoReunion_ID() %>"><%= tr.getNombre() %></option>
				<%
					}
				%>
                </select>
            </div>
            <div class="form-group col-sm-6 col-md-8">
                <label class="control-label " for="temaReunion">Tema:</label>
                <input type="text" class="form-control" id="Tema" name="Tema" placeholder="Ingresar tema" required>    
          </div>
            <div class="form-group col-sm-6 col-md-4">
               
                  <label class="control-label " for="familiaKolping">Familia Kolping:</label>
                  <select class="form-control selectpicker" data-live-search="true" id="T_FamiliaKolping_ID" name="T_FamiliaKolping_ID" >
	                <%
						DTFamilia dtFam = new DTFamilia();
						ArrayList<Familia> listaFam = dtFam.listarFamilias();
						for(Familia f: listaFam)
						{
					%>
							<option value="<%= f.getT_FamiliaKolping_ID() %>"><%= f.getNombre() %></option>
					<%
						}
					%>
                  </select>
               
              </div>
           
            <div class="form-group col-sm-6 col-md-6">
                
                    <label class="control-label " for="descripcionReunion">Descripci�n:</label>
                    <label class="d-inline-block float-right text-secondary" ><span id="contador"></span></label>
                    <textarea class="form-control resize_disabled" rows="4" id="Descripcion" name="Descripcion" maxlength="255" required></textarea>
             
            </div>
            <div class="form-group col-sm-6 col-md-6">       
                    <label class="control-label" for="observacionReunion">Observaci�n:</label>
                    <label class="d-inline-block float-right text-secondary " ><span id="contador2"></span></label>
                    <textarea  class="form-control resize_disabled" rows="4" id="Observacion" name="Observacion" maxlength="255" required></textarea>
            </div>
          </div>
            <div class="form-group row">
                <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Guardar</button></div>
                <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
            </div>
        </form>
      </div>
    </div> 
     <!-- /Page Content -->
    
    <!-- footer-->
     <%@include file="../../footer.jsp" %>
    <!-- /footer-->
    
    <!-- Logout Modal-->
    <%@include file="../../logout.jsp" %>
    <!-- /Logout Modal-->
  
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>  
    
	<script type="text/javascript" src="../../js/bootstrap-select.js"></script>
	
    <!-- Page level plugin JavaScript-->

      <script>
      $.datepicker.regional['es'] = {
				closeText : 'Cerrar',
				prevText : '< Ant',
 				nextText: 'Sig >',
				currentText : 'Hoy',
				monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
				monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
				dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado' ],
				dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mi�', 'Juv', 'Vie', 'S�b' ],
				dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'S�' ],
				weekHeader : 'Sm',
				dateFormat : 'dd/mm/yy',
				firstDay : 1,
				isRTL : false,
				showMonthAfterYear : false,
				yearSuffix : ''
			};
        $.datepicker.setDefaults($.datepicker.regional['es']);
        $('#Fecha').datepicker({
          setDate : new Date(),
          dateFormat : 'dd-mm-yy',
          changeMonth: true,
          changeYear : true,
          yearRange : "-200:c+nn",
          maxDate: "-0d"
        });
      </script>
      <script>
      var inputs = "textarea[maxlength]";
    $(document).on('input', "[maxlength]", function (e) {
        var este = $(this),
            maxlength = este.attr('maxlength'),
            maxlengthint = parseInt(maxlength),
            currentCharacters = este.val().length;
            espan = este.prev('label').find('span');            
            // Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
            if (document.addEventListener && !window.requestAnimationFrame) {
                if (remainingCharacters <= -1) {
                    remainingCharacters = 0;            
                }
            }
            espan.html(currentCharacters+"/"+maxlength);  
        });
      </script>
       <script>
       $(document).on('reset', function (e) {
    	 
    	$('#contador').text("");
    	$('#contador2').text("");
        });
      </script>
  </div>
</body>

</html>

