<%@page import= "vistas.V_Info_Miembro"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMiembro"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">

  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black">
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">






















      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../../index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="listaMiembro.jsp">Miembro</a>
        </li>
        <li class="breadcrumb-item active">Descripcion de Miembro</li>
      </ol>
      <h1>Descripcion de Miembro</h1>
      <hr class="mt-2">
      <div class="item_container">
       
		
	  <%
		ArrayList<V_Info_Miembro> listaMiembro = (ArrayList<V_Info_Miembro>) session.getAttribute("ListaMiembros");
		String idRequestMiembro = request.getParameter("id");
		  V_Info_Miembro m = null;
		int idMiembro = 0;
		try {
			idMiembro = Integer.parseInt(idRequestMiembro);
		} catch (Exception e) {
			idMiembro = 0;
			e.printStackTrace();
			e.getMessage();
		}
		for (V_Info_Miembro tempM : listaMiembro) {
			if (tempM.getT_Miembro_ID() == Integer.parseInt(idRequestMiembro)) {
				m = tempM;
				break;
			}
		}
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	  %>
	  
        <div class="card-body font-weight-bold text-truncate lead">
          <% out.print(m.getNombre()+" "+m.getApellido()); %>
        </div>
        <hr class="my-0">
        <div class="card-body row">
          <div class="col-md-6 col-sm-6" style="padding:10px">
            <strong>Cedula:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getCedula() %></div>
          </div>
          <div class="col-md-4 col-sm-6" style="padding:10px">
            <strong>Telefono:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getTelefono() %></div>
          </div>
          <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Fecha de Nacimiento:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getFechaNacimiento() %></div>
          </div>
          
          <div class="col-md-4 col-sm-6" style="padding:10px">
            <strong>Sexo:</strong>
            <hr class="my-0">
            <div class="text-secondary"><% if(m.getSexo()==1){out.print("Masculino");}else{out.print("Femenino");} %></div>

          </div>
          
          
          
          
        
           <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Fecha de Uni�n:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getFechaUnion() %></div>
          </div>
          
          <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Nivel Academico:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getNivelAcademico() %></div>
          </div>
          
           <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Cargo:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getNombreCargo() %></div>
          </div>
          
          <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Tipo de Socio:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getNombreTipoSocio() %></div>
          </div>
         
         <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Departamento:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getNombreDepartamento() %></div>
          </div>
          <div class="col-md-2 col-sm-6" style="padding:10px">
            <strong>Municipio:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getNombreMunicipio() %></div>
          </div> 
          <div class="col-md-3 col-sm-6" style="padding:10px">
            <strong>Familia:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= m.getNombreFamiliaKolping() %></div>
          </div>
         
         
         
        </div>
           
          
          



      </div>































    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright � Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
  </div>
</body>

</html>