<%@page import="entidades.Miembro"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMiembro"%>
<%@page import="vistas.V_Info_Miembro"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">






















      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Miembros</li>
      </ol>
      <div class="row">
        <div class="col"><h1>Lista de Miembros</h1></div>
        <div class="col-sm-auto">
          <a href="crearMiembro.jsp" class="btn btn-primary btn-block float-right" role="button"><i class="fa fa-fw fa-plus"></i>Nuevo</a> 
        </div>
    
      </div>
      <hr class="mt-2">
     <!-- activos -->
      <div class="item_container card-body">
			<div class="form-check my-3">
				<label> <input type="checkbox" name="check" onchange="toggleInactivos(this)"> <span class="label-text unselectable">Mostrar Inactivos</span> </label>
			</div>
			<div class="toggable">
				<table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0">
					<thead>
						<tr class="thead-dark">
							<th scope="col">#</th>
							<th scope="col">Nombre</th>
							<th scope="col">Apellido</th>
							<th scope="col">Nivel Académico</th>
							<th scope="col">Cargo</th>
							<th scope="col">Sexo</th>
							<th scope="col">Telefono</th>
							<th scope="col">Opciones</th>
						</tr>
					</thead>
					<tbody>
						<%
							DTMiembro dtm = new DTMiembro();
							ArrayList<V_Info_Miembro> listaMiembros = new ArrayList<V_Info_Miembro>();
							listaMiembros = dtm.listarInfoMiembros();
			
							//Guardamos esa lista en una Sesión
							session.setAttribute("ListaMiembros", listaMiembros);
							int number = 1;
			
							DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
							for (V_Info_Miembro m : listaMiembros) {
						%>
						<tr>
							<td><%=number%></td>
							<td><%=m.getNombre()%></td>
							<td><%=m.getApellido()%></td>
							<td><%=m.getNivelAcademico() %></td>
							<td><%=m.getNombreCargo() %></td>
							<td><% if (m.getSexo() == 1) { out.print("Masculino"); } else { out.print("Femenino"); } %></td>
							<td><%=m.getTelefono()%></td>
							<td>
								<a class="btn btn-sm btn-light	d-inline-block" href=<%out.print("verMiembro.jsp?id=" + m.getT_Miembro_ID());%>> <i class="fa fa-fw fa-eye"></i></a> 
								<a class="btn btn-sm btn-light	d-inline-block" href=<%out.print("editarMiembro.jsp?id=" + m.getT_Miembro_ID());%>> <i class="fa fa-fw fa-pencil"></i></a>
								<a class="btn btn-sm btn-danger	d-inline-block" href="" data-toggle="modal" data-target="#modalEliminar" data-miembroid=<%out.print(m.getT_Miembro_ID());%>> <i class="fa fa-fw fa-trash"></i></a>
							</td>
						</tr>
						<%
							number = number + 1;
							}
						%>
					</tbody>
				</table>
			</div>
			
			<!-- inactivos -->
			<div class="toggable" style="display: none;">
				<table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0">
					<thead>
						<tr class="thead-dark">
							<th scope="col">#</th>
							<th scope="col">Nombre</th>
							<th scope="col">Apellido</th>
							<th scope="col">Nivel Académico</th>
							<th scope="col">Cargo</th>
							<th scope="col">Sexo</th>
							<th scope="col">Telefono</th>
							<th scope="col">Opciones</th>
						</tr>
					</thead>
					<tbody>
						
						<%
							listaMiembros = dtm.listarInfoMiembrosInactivos();
							number = 1;
							for (V_Info_Miembro m : listaMiembros) {
						%>
						<tr>
							<td><%=number%></td>
							<td><%=m.getNombre()%></td>
							<td><%=m.getApellido()%></td>
							<td><%=m.getNivelAcademico() %></td>
							<td><%=m.getNombreCargo() %></td>
							<td><% if (m.getSexo() == 1) { out.print("Masculino"); } else { out.print("Femenino"); } %></td>
							<td nowrap><%=m.getTelefono()%></td>
							<td><a class="btn btn-sm btn-light d-incline-block" href="" data-toggle="modal" data-target="#modalRestaurar" data-miembroid=<%out.print(m.getT_Miembro_ID());%>> <i class="fa fa-fw fa-arrow-up"></i></a></td>
						</tr>
						<%
							number = number + 1;
							}
						%>
					</tbody>
				</table>
			</div>
      </div>
        
        <!-- Modal Eliminar -->
		<div id="modalEliminar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Dar de Baja a Miembro</h4>
					</div>
					<div class="modal-body">
						<p>¿Esta seguro que desea dar de baja a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Dar de baja</button></a>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal Restaurar -->
		<div id="modalRestaurar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Restaurar a Miembro</h4>
					</div>
					<div class="modal-body">
						<p>¿Está seguro que desea restaurar a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Restaurar</button></a>
					</div>
				</div>
			</div>
		</div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Ã</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    
    <script>
	    $(document).ready(function () {
		   $('#modalEliminar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('miembroid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarMiembro?id='+recipient);
		   });
		   $('#modalRestaurar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('miembroid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLrestaurarMiembro?id='+recipient);
		   });
	    });
	    function toggleInactivos(element)
	    {
	      $(".toggable").toggle();
	      $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
	    }
    </script>
  </div>
</body>

</html>
