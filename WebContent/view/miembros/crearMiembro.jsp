<%@page import="entidades.Miembro"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMiembro"%>
<%@page import="entidades.Departamento"%>
<%@page import="datos.DTDepartamento"%>
<%@page import="entidades.Municipio"%>
<%@page import="datos.DTMunicipio"%>
<%@page import="entidades.Cargo"%>
<%@page import="datos.DTCargo"%>
<%@page import="entidades.TipoSocio"%>
<%@page import="datos.DTTipoSocio"%>
<%@page import="entidades.Familia"%>
<%@page import="datos.DTFamilia"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaMiembro.jsp">Miembro</a>
        </li>
        <li class="breadcrumb-item active">Crear Miembro</li>
      </ol>
      
      <h1>Crear Miembro</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form id="formFormador" class="form formFormador" method="POST" action="${pageContext.request.contextPath}/SLguardarMiembro">
          <div class="row">
            <div class="form-group col-sm-6 col-md-6">
                <label class="control-label" for="Nombre">Nombres:</label>
                <input type="text" class="form-control" name="Nombre" id="Nombre" placeholder="Ingresar Nombres" required>
            </div>
            <div class="form-group col-sm-6 col-md-6">
              <label class="control-label" for="Apellido">Apellidos:</label>
              <input type="text" class="form-control" name="Apellido" id="Apellido" placeholder="Ingresar Apellidos" required>
            </div>
            <div class="form-group col-sm-6 col-md-4">
              <label class="control-label" for="Sexo">Sexo:</label>
                <select class="form-control" id="Sexo" name="Sexo">
                    <option value="1">Masculino</option>
                    <option value="0">Femenino</option>
                </select>
            </div>
            <div class="form-group col-sm-6 col-md-4">
              <label class="control-label" for="Cedula">C�dula:</label>
              <input type="text" class="form-control cedulaMask" name="Cedula" id="Cedula" placeholder="Ingresar C�dula" required>
            </div>
            <div class="form-group col-sm-6 col-md-4">
              <label class="control-label" for="FechaNacimiento">Fecha de Nacimiento:</label >
              <input type="text" id="FechaNacimiento" class="form-control fechaMask" name="FechaNacimiento" title="La Fecha de Nacimiento es requerida" placeholder="Ingrese la fecha de nacimiento" required> 
              <!--input type="text" class="form-control" value="02-16-2012"-->
            </div>
            <div class="form-group col-sm-6 col-md-4">
              <label class="control-label" for="FechaUnion">Fecha de Uni�n:</label >
              <input type="text" id="FechaUnion" class="form-control fechaMask" name="FechaUnion" title="La Fecha de Uni�n es requerida" placeholder="Ingrese la fecha de Uni�n" required> 
              <!--input type="text" class="form-control" value="02-16-2012"-->
            </div>
            
            <div class="form-group col-sm-6 col-md-3">
              <label class="control-label" for="T_Cargo_ID">Cargo:</label>
          		<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Cargo_ID" name="T_Cargo_ID">
				<%
					DTCargo dtcar = new DTCargo();
					ArrayList<Cargo> listaCar = dtcar.listarCargos();
					for(Cargo car: listaCar)
					{
				%>
						<option value="<%= car.getT_Cargo_ID() %>"><%= car.getNombre() %></option>
				<%
					}
				%>
	             </select>
           	</div>
            <div class="form-group col-sm-6 col-md-5">
              <label class="control-label" for="NivelAcademico">Nivel Acad�mico:</label>
              <select class="form-control" name="NivelAcademico" id="NivelAcademico">
                <option value="Preescolar">Preescolar</option>
                <option value="Primaria">Primaria</option>
                <option value="Secundaria">Secundaria</option>
                <option value="Universidad">Universidad</option>
                
              </select>
            </div>
            <div class="form-group col-sm-4 col-md-4">
              <label class="control-label" for="telefono">Tel�fono:</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">(505)</div>
                </div>
                <input type="tel" class="form-control telefonoMask" name="Telefono" id="Telefono" data-mask="0000-0000" placeholder="Ingresar Tel�fono" required>
              </div>
            </div>
            
            <div class="form-group col-sm-6 col-md-4">
                <label class="control-label" for="T_Departamento_ID">Departamento:</label>
          		<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Departamento_ID" name="T_Departamento_ID">
				<%
					DTDepartamento dtd = new DTDepartamento();
					ArrayList<Departamento> listaDep = dtd.listarDepartamentos();
					for(Departamento d: listaDep)
					{
				%>
						<option value="<%= d.getT_Departamento_ID() %>"><%= d.getNombre() %></option>
				<%
					}
				%>
	             </select>
           	</div>
            <div class="form-group col-sm-6 col-md-4">
				<label class="control-label" for="T_Municipio_ID">Municipio:</label>
				<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Municipio_ID" name="T_Municipio_ID">
				<%
					DTMunicipio dtm = new DTMunicipio();
					ArrayList<Municipio> listaMun = dtm.listarMunicipios();
					for(Municipio m: listaMun)
					{
				%>
						<option value="<%= m.getT_Municipio_ID() %>" data-chained="<%= m.getT_Departamento_ID() %>"><%= m.getNombre() %></option>
						
				<%
					}
				%>
				</select>
            </div>
            <div class="form-group col-sm-6 col-md-3">
              <label class="control-label" for="T_TipoSocio_ID">Tipo Socio:</label>
          		<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_TipoSocio_ID" name="T_TipoSocio_ID">
				<%
					DTTipoSocio dtTipo = new DTTipoSocio();
					ArrayList<TipoSocio> listaTipoSocio = dtTipo.listarSocios();
					for(TipoSocio tipo: listaTipoSocio)
					{
				%>
						<option value="<%= tipo.getT_TipoSocio_ID() %>"><%= tipo.getNombre() %></option>
						
				<%
					}
				%>
	             </select>
           	</div>
            <div class="form-group col-sm-6 col-md-3">
              <label class="control-label" for="T_Familia_ID">Familia:</label>
          		<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_FamiliaKolping_ID" name="T_FamiliaKolping_ID">
				<%
					DTFamilia dtFam = new DTFamilia();
					ArrayList<Familia> listaFamilia = dtFam.listarFamilias();
					for(Familia Fam: listaFamilia)
					{
				%>
						<option value="<%= Fam.getT_FamiliaKolping_ID() %>"><%= Fam.getNombre() %></option>
						
				<%
					}
				%>
	             </select>
           	</div>
            
            
          </div>

          <div class="form-group row">
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-danger">Cancelar</button></div>
          </div>
        </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright � Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    
	<script src="../../vendor/jquery/jquery.min.js"></script>
	<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
	<script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="../../js/jquery.chained.js"></script>
	<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script type="text/javascript" src="../../js/bootstrap-select.js"></script>
	<script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
  <!-- Page level plugin JavaScript-->
    <script>
      $(document).ready(function(){
        $('.cedulaMask').mask('000-000000-0000S', {'translation': {
          S: {pattern: /[A-Z]/}
        }});
        $('.fechaMask').mask('00-00-0000');
        $('.telefonoMask').mask('0000-0000');
        $('.selectpicker').selectpicker();
        $("#T_Municipio_ID").chained("#T_Departamento_ID");
      });
    </script>
    <script>
      $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi�','Juv','Vie','S�b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S�'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
      };
      $.datepicker.setDefaults($.datepicker.regional['es']);
      $('#FechaNacimiento').datepicker({
        setDate : new Date(),
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear : true,
        yearRange : "-200:c+nn",
        maxDate: "-0d"
      });
      $('#FechaUnion').datepicker({
        setDate : new Date(),
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear : true,
        yearRange : "-200:c+nn",
        maxDate: "-0d"
      });
    </script>
    <!-- Bootstrap core JavaScript-->
	    
	    
	    <!-- Core plugin JavaScript-->
	    
	    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
	    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
	    <!-- Custom scripts for all pages-->
	    <script src="../../js/sb-admin.min.js"></script>
	    <!-- Custom scripts for this page-->
	    <script src="../../js/tablesLanguage.js"></script>
	    <script src="../../js/jquery.mask.js"></script>
    
    
  </div>
</body>

</html>
