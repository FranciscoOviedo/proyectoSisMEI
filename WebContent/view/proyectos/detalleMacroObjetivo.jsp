<%@page import="entidades.MacroIndicador"%>
<%@page import="entidades.MacroObjetivo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMacroObjetivo"%>
<%@page import="datos.DTMacroIndicador"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">











      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="../../index.jsp">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
              <a href="detalleMacroObjetivo.jsp">Objetivos</a>
          </li>
          <li class="breadcrumb-item active">Indicadores</li>
      </ol>
      
      <%
			ArrayList<MacroObjetivo> listaObj = (ArrayList<MacroObjetivo>) session.getAttribute("ListaMacroObjetivo");
			String idRequestObj = request.getParameter("id");
		  	MacroObjetivo obj = null;
			int idObj = 0;
			try {
				idObj = Integer.parseInt(idRequestObj);
			} catch (Exception e) {
				idObj = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (MacroObjetivo tempO : listaObj) {
				if (tempO.getT_MacroProyecto_ID() == Integer.parseInt(idRequestObj)) {
					obj = tempO;
					break;
				}
			}
		%>
      
      <h1>Indicadores de: <%= obj.getDefinicion() %></h1>
      <hr class="mt-2">
                
            <div class="row">
                <div class="col-12">
                    <div class="item_container card-body">
   
              
		
		<div class="row my-4">
			<h2 class="col mb-0">Indicadores</h2>
			<button data-toggle="modal" data-target="#modalCrear" class="col-auto mr-3 btn btn-success addnewrow"> <span class="fa fa-fw fa-plus"></span> </button>
		</div>

		<div class="table-responsive">
			<table class="table table-hover" id="tab_logic" cellspacing="0" width="100%">
				<thead>
					<tr class="thead">
						<th> <p>#</p> </th>
						<th> <p>Definición</p> </th>
						<th> <p>Tipo de Medición</p> </th>
						<th> <p>Opciones</p> </th>
					</tr>
				</thead>
				<tbody>
				<%
		           	DTMacroIndicador dtmi = new DTMacroIndicador();
					ArrayList<MacroIndicador> listaMacroInd = new ArrayList<MacroIndicador>();
					listaMacroInd = dtmi.listarMacroIndicador(idObj);
					int number=1;
					
					for(MacroIndicador i : listaMacroInd)
					{
				%>
					<tr>
						<td class="order"><% out.print(number); %></td>
						<td><%= i.getDefinicion() %></td>
						<td><%= i.getTipoMedicion() %></td>
						<td>
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEditar"
								data-objetivoid="<% out.print( i.getT_MacroIndicador_ID()); %>"
								data-objetivoproyecto="<% out.print( i.getT_MacroObjetivo_ID()); %>"
								data-definicion="<% out.print( i.getDefinicion()); %>"
								data-tipoMedicion="<% out.print( i.getTipoMedicion()); %>"
								>
								<i class="fa fa-fw fa-pencil"></i>
							</a>
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEliminar" data-objetivoid=<% out.print(i.getT_MacroIndicador_ID()); %>> <i class="fa fa-fw fa-minus"></i></a>
						</td>
					</tr>
				<%
						number++;
					}
					if(listaMacroInd.size()==0){out.print("<tr><td colspan='5'>Aún no se han ingresado indicadores</td></tr>");}
				%>
				</tbody>
			</table>
		</div>
	  </div>
	</div>
	  
     <!-- Modal Crear Objetivo -->
		<div id="modalCrear" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Agregar un Indicador</h4>
					</div>
					<form id="formGasto" class="form formFormador" method="POST" action="${pageContext.request.contextPath}/SLguardarMacroIndicador">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_MacroProyecto_ID del objetivo que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_MacroProyecto_ID" id="T_MacroProyecto_ID" value="<%=idObj%>" hidden="true">
							
							<div class="form-group">
								<label for="Descripcion" class="col-form-label">Definición:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="4" id="Definicion" name="Definicion" maxlength="255" placeholder="Ingrese una breve descripcion de la actividad" required></textarea>
							</div>
							
							<div class="form-group">
								<label for="TipoMEDICION" class="col-form-label">Tipo de Medición:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<input type="text" class="form-control" id="TipoMedicion" name="TipoMedicion" title="El Tipo de Medición es requerido" placeholder="Ingrese el nombre del proyecto"
			                      required>
			                      </div>
						</div>
						<div class="modal-footer">
							 <button type="submit" class="btn btn-block btn-success">Agregar</button>
							 <button type="button" class="btn btn-block btn-danger" data-dismiss="modal">Cancelar</button>
						</div>
					</form>
				</div>
			</div>
		</div>


<!-- Modal Editar Objetivo -->
		<div id="modalEditar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Editar Indicador</h4>
					</div>
					<form id="formEditarObjetivo" class="form formEditar" method="POST" action="${pageContext.request.contextPath}/SLeditarMacroIndicador">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_Gasto_ID, es la llave primaria del gasto que se esta editando... debe estar hidden.. aun no tiene value porque se le asignara con javascript -->
							<input type="text" name="T_MacroIndicador_ID" id="T_MacroIndicador_ID_MODIFICAR_INDICADOR" hidden="true">
							
							<!-- Este de aqui sirve para mandar el atributo T_Actividad_ID de la actividad que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_MacroObjetivo_ID" id="T_MacroObjetivo_ID_MODIFICAR_INDICADOR" value="<%=idObj%>" hidden="true">
							
							<div class="form-group">
								<label for="Definición" class="col-form-label">Definición</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="4" id="Definicion_MODIFICAR" name="Definicion" maxlength="255" placeholder="Ingrese una breve Definicion del Objetivo" required></textarea>
							</div>
							
							<div class="form-group">
								<label for="TipoMedicion" class="col-form-label">Tipo de Medición:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<input type="text" class="form-control" id="TipoMedicion" name="TipoMedicion" title="El Tipo de Medición es requerido" placeholder="Ingrese el nombre del proyecto"
			                      required>
			                      </div>
							
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Editar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>


		<!-- Modal Eliminar -->
				<div id="modalEliminar" class="modal fade" role="dialog">
					<div class="modal-dialog">
						<!-- Modal content-->
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title">Dar de Baja Objetivos</h4>
							</div>
							<div class="modal-body">
								<p>¿Esta seguro que desea dar de baja a este elemento de la lista?.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Dar de baja</button></a>
							</div>
						</div>
					</div>
				</div>

		</div>
	</div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  <footer class="sticky-footer">
    <div class="container">
      <div class="text-center">
        <small>Copyright © Your Website 2018</small>
      </div>
    </div>
  </footer>
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
  <!-- Logout Modal-->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Page level plugin JavaScript-->
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>
  <!-- Custom scripts for this page-->
  <script src="../../js/tablesLanguage.js"></script>
  <script>
    $(document).ready(function () {
	   $('#modalEliminar').on('show.bs.modal', function (event) {
	   	  var button = $(event.relatedTarget)
	   	  var recipient = button.data('microproyectoid')
	   	  var modal = $(this)
	   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarMacroObjetivo?id='+recipient);
	   });
	   $('#modalRestaurar').on('show.bs.modal', function (event) {
	   	  var button = $(event.relatedTarget)
	   	  var recipient = button.data('microproyectoid')
	   	  var modal = $(this)
	   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLrestaurarMacroObjetivo?id='+recipient);
	   });
    });
    function toggleInactivos(element)
    {
      $(".toggable").toggle();
    }
    
  //Esto es para cargar los datos del gasto en el modal que se seleccione...
	$('#modalEditar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var indicadorid = button.data('indicadorid')
		var definicion = button.data('definicion')
		var tipoMedicion = button.data('tipoMedicion')
		var modal = $(this)
		modal.find('#T_MacroObjetivo_ID_MODIFICAR_OBJETIVO').attr('value', indicadorid);
		modal.find('#Definicion_MODIFICAR').text(definicion);
		modal.find('#TipoMedicion').text(tipoMedicion);
   });
	
	//Esto es para resetear el formulario cuando se cierre el modal... antes si se escribia algo se cerraba y se volvia a abrir con otro... los datos seguian del anterior...
	$('#modalEditar').on('hide.bs.modal', function () {
		$('#formEditar').trigger("reset");  //Aqui poner el id del formulario de editar
	});
	$(document).ready(function () {
		   $('#modalEliminar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('indicadorid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarMacroIndicador?id='+recipient);
		   });
   </script>



  </div>
</body>

</html>