<%@page import="entidades.MacroProyecto"%>
<%@page import="entidades.MacroObjetivo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMacroObjetivo"%>
<%@page import="datos.DTMacroProyecto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">











      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
          <li class="breadcrumb-item">
              <a href="../../index.jsp">Dashboard</a>
          </li>
          <li class="breadcrumb-item">
              <a href="listaMacroProyecto.jsp">Macro Proyectos</a>
          </li>
          <li class="breadcrumb-item active">Objetivos</li>
      </ol>
      
      <%
			ArrayList<MacroProyecto> listaMacro = (ArrayList<MacroProyecto>) session.getAttribute("ListaMacroProyecto");
			String idRequestMacro = request.getParameter("id");
			MacroProyecto macro = null;
			int idMacro = 0;
			try {
				idMacro = Integer.parseInt(idRequestMacro);
			} catch (Exception e) {
				idMacro = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (MacroProyecto tempM : listaMacro) {
				if (tempM.getT_MacroProyecto_ID() == Integer.parseInt(idRequestMacro)) {
					macro = tempM;
					break;
				}
			}
		%>
      
      <h1>Objetivos de: <%= macro.getNombre() %></h1>
      <hr class="mt-2">
                
            <div class="row">
                <div class="col-12">
                    <div class="item_container card-body">
   
              
		
		<div class="row my-4">
			<h2 class="col mb-0">Objetivos Especificos:</h2>
			<button data-toggle="modal" data-target="#modalCrearObjetivo" class="col-auto mr-3 btn btn-success addnewrow"> <span class="fa fa-fw fa-plus"></span> </button>
		</div>

		<div class="table-responsive">
			<table class="table table-hover" id="tab_logic" cellspacing="0" width="100%">
				<thead>
					<tr class="thead">
						<th> <p>#</p> </th>
						<th> <p>Definición</p> </th>
						<th> <p>Opciones</p> </th>
					</tr>
				</thead>
				<tbody>
				<%
		           	DTMacroObjetivo dtmo = new DTMacroObjetivo();
					ArrayList<MacroObjetivo> listaMacroObj = new ArrayList<MacroObjetivo>();
					listaMacroObj = dtmo.listarMacroObjetivo(idMacro);
					session.setAttribute("ListaMacroObjetivo", listaMacroObj);
					int number=1;
					
					for(MacroObjetivo o : listaMacroObj)
					{
				%>
					<tr>
						<td class="order"><% out.print(number); %></td>
						<td><%= o.getDefinicion() %></td>
						<td>
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEditarObjetivo"
								data-objetivoid="<% out.print( o.getT_MacroObjetivo_ID()); %>"
								data-definicion="<% out.print( o.getDefinicion()); %>" >
								<i class="fa fa-fw fa-pencil"></i>
							</a>
							<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalEliminar" data-objetivoid=<% out.print(o.getT_MacroObjetivo_ID()); %>> <i class="fa fa-fw fa-minus"></i></a>
							<a class="d-inline-block" href=<% out.print("detalleMacroObjetivo.jsp?id="+o.getT_MacroObjetivo_ID()); %>>
	                  		<i class="fa fa-fw fa-bullseye"></i></a>
						
						</td>
					</tr>
				<%
						number++;
					}
					if(listaMacroObj.size()==0){out.print("<tr><td colspan='5'>Aún no se han ingresado objetivos</td></tr>");}
				%>
				
					<tr data-toggle="collapse" data-target="#accordion" class="clickable">
						<td>Some Stuff</td>
						<td>Some more stuff</td>
						<td>And some more</td>
					</tr>
					<tr>
						<td colspan="3">
							<div id="accordion" class="collapse">Hidden by default</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	  </div>
	</div>
	  
     <!-- Modal Crear Objetivo -->
		<div id="modalCrearObjetivo" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Agregar un Objetivo Especifico</h4>
					</div>
					<form id="formGasto" class="form formFormador" method="POST" action="${pageContext.request.contextPath}/SLguardarMacroObjetivo">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_MacroProyecto_ID del objetivo que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_MacroProyecto_ID" id="T_MacroProyecto_ID" value="<%=idMacro%>" hidden="true">
							
							<div class="form-group">
								<label for="Descripcion" class="col-form-label">Definición:</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="4" id="Definicion" name="Definicion" maxlength="255" placeholder="Ingrese una breve descripcion de la actividad" required></textarea>
							</div>
						</div>
						<div class="modal-footer">
							 <button type="submit" class="btn btn-block btn-success">Agregar</button>
							 <button type="button" class="btn btn-block btn-danger" data-dismiss="modal">Cancelar</button>
						</div>
					</form>
				</div>
			</div>
		</div>


		<!-- Modal Editar Objetivo -->
		<div id="modalEditarObjetivo" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Editar Objetivo</h4>
					</div>
					<form id="formEditarObjetivo" class="form formEditarObjetivo" method="POST" action="${pageContext.request.contextPath}/SLeditarMacroObjetivo">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_Gasto_ID, es la llave primaria del gasto que se esta editando... debe estar hidden.. aun no tiene value porque se le asignara con javascript -->
							<input type="text" name="T_MacroObjetivo_ID" id="T_MacroObjetivo_ID_MODIFICAR_OBJETIVO" hidden="true">
							
							<!-- Este de aqui sirve para mandar el atributo T_Actividad_ID de la actividad que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_MacroProyecto_ID" id="T_MacroProyecto_ID_MODIFICAR_OBJETIVO" value="<%=idMacro%>" hidden="true">
							
							<div class="form-group">
								<label for="Definición" class="col-form-label">Definición</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="4" id="Definicion_MODIFICAR_OBJETIVO" name="Definicion" maxlength="255" placeholder="Ingrese una breve Definicion del Objetivo" required></textarea>
							</div>
							
							
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Editar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>


		<!-- Modal Eliminar -->
		<div id="modalEliminar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Dar de Baja Objetivos</h4>
					</div>
					<div class="modal-body">
						<p>¿Esta seguro que desea dar de baja a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<a><button type="button" class="btn btn-primary">Dar de baja</button></a>
					</div>
				</div>
			</div>
		</div>

		</div>
	</div>
  <!-- /.container-fluid-->
  <!-- /.content-wrapper-->
  <%@include file="../../footer.jsp" %>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <!-- Page level plugin JavaScript-->
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>
  <!-- Custom scripts for this page-->
  <script src="../../js/tablesLanguage.js"></script>
  <script>
	function toggleInactivos(element)
	{
		$(".toggable").toggle();
	}

	//Esto es para cargar los datos del gasto en el modal que se seleccione...
	$('#modalEditarObjetivo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var objetivoid = button.data('objetivoid')
		var definicion = button.data('definicion')
		var modal = $(this)
		modal.find('#T_MacroObjetivo_ID_MODIFICAR_OBJETIVO').attr('value', objetivoid);
		modal.find('#Definicion_MODIFICAR_OBJETIVO').text(definicion);	
	});

	//Esto es para resetear el formulario cuando se cierre el modal... antes si se escribia algo se cerraba y se volvia a abrir con otro... los datos seguian del anterior...
	$('#modalEditarObjetivo').on('hide.bs.modal', function () {
		$('#formEditarObjetivo').trigger("reset");  //Aqui poner el id del formulario de editar
	});
	
	$('#modalEliminar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var recipient = button.data('objetivoid')
		var modal = $(this)
		modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarMacroObjetivo?id='+recipient);
	});
	$('#modalRestaurar').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget)
		var recipient = button.data('objetivoid')
		var modal = $(this)
		modal.find('.modal-footer a').attr('href', '/SisMEI/SLrestaurarMacroObjetivo?id='+recipient);
	});
   </script>



  </div>
</body>

</html>