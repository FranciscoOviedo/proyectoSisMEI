<%@page import="entidades.MicroProyecto"%>
<%@page import="entidades.MacroProyecto"%>
<%@page import="entidades.Familia"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMicroProyecto"%>
<%@page import="datos.DTMacroProyecto"%>
<%@page import="datos.DTFamilia"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">




            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.jsp">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="listaMicroProyecto.jsp">Micro Proyectos</a>
                </li>
                <li class="breadcrumb-item active">Editar Micro Proyecto</li>
            </ol>
          <div class="row">
          <div class="col-12">
         <div class="item_container card-body">

                        <h2>Editar MicroProyectos</h2>
                        
                        <br>
          <form method="POST" action="./SLeditarMicroProyecto">
			
				<%
						ArrayList<MicroProyecto> ListaMicroProyecto = (ArrayList<MicroProyecto>) session.getAttribute("ListaMicroproyecto");
						String idRequestMicro = request.getParameter("id");
						MicroProyecto micro = null;
						int idMicro = 0;
						try {
							idMicro = Integer.parseInt(idRequestMicro);
						} catch (Exception e) {
							idMicro = 0;
							e.printStackTrace();
							e.getMessage();
						}
						for (MicroProyecto tempM : ListaMicroProyecto) {
							if (tempM.getT_MicroProyecto_ID()== Integer.parseInt(idRequestMicro)) {
								micro = tempM;
								break;
							}
						}
					%>		
				
				<div class="form-group">
	                       <label for="usr">Nombre del Proyecto:</label>
	                       <input type="text" class="form-control" value="<%= micro.getNombre() %>" name="Nombre" id="Nombre">
	                   </div>                        
	                   <div class="row">
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="usr">Fecha de Inicio:</label>
                                    <input type="text" class="form-control" value="<%= micro.getFechaInicio() %>" name="FechaInicio" id="FechaInicio">
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="usr">Fecha de Final:</label>
                                    <input type="text" class="form-control" value="<%= micro.getFechaFin() %>" name="FechaFin" id="FechaFin">
                                </div>                                   
                            </div>
                        </div>
                         <div class="form-group">
				             <label class="control-label" for="T_FamiliaKolping_ID">Familia Kolping:</label>
				             <select class="form-control selectpicker" data-live-search="true" data-live-search-normalize="true" id="T_FamiliaKolping_ID" name="T_FamiliaKolping_ID" required>
				             <!-- multiple -->
				             	<option value="">Debe seleccionar uno</option>
				               <%
								DTFamilia dtf = new DTFamilia();
								ArrayList<Familia> listaFor = dtf.listarFamilias();
								for(Familia f: listaFor)
								{
							%>
				               <option value="<%= f.getT_FamiliaKolping_ID() %>"<%{ out.print("selected"); } %>><%= f.getNombre() %></option>
				               <%
								}
				               %>

				             </select>
				           </div>

                        <div class="form-group">
                            <label class="control-label " for="ObjetivoGeneral">Objetivo General:</label>
                            <label class="d-inline-block float-right text-secondary"><span></span></label>
                            <textarea class="form-control resize_disabled"  name="ObjetivoGeneral" id="ObjetivoGeneral" rows="4" maxlength="255"><%= micro.getObjetivoGeneral() %></textarea>
                        </div>

                        <div class="form-group row">
                            <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Actualizar</button></div>
                            <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
                        </div>
				
			</form>
			<script src="plugins/jquery/jquery-3.3.1.js"></script>
			<script type="text/javascript">
			$(document).ready(function() {
				$('#nombre').val("<%=micro.getNombre()%>");
				$('#objetivo').val("<%=micro.getObjetivoGeneral() %>");
				$('#fechainicio').val("<%=micro.getFechaInicio() %>");
				$('#fechafin').val("<%=micro.getFechaFin() %>");
				$('#tipo').val("<%=micro.getTipo() %>");
			});
			</script>
						

           </div>
                    
                    











                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
        <%@include file="../../footer.jsp" %>
        <!-- Bootstrap core JavaScript-->
        <script src="../../vendor/jquery/jquery.min.js"></script>
		<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
		<script src="../../js/jquery.mask.js"></script>
		<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap-select.js"></script>
		<script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
       <script>
            $('.fechaMask').mask('00-00-0000');

            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '< Ant',
                nextText: 'Sig >',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };

            $.datepicker.setDefaults($.datepicker.regional['es']);

            $('#FechaInicio').datepicker({
                setDate: new Date(),
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            });

            $('#FechaFin').datepicker({
                setDate: new Date(),
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            });
            
            var inputs = "textarea[maxlength]";
    		$(document).on('keyup', "[maxlength]",function(e) {
    			var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
    			espan = este.prev('label').find('span');
    			// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
    			if (document.addEventListener && !window.requestAnimationFrame) {
    				if (remainingCharacters <= -1) {
    					remainingCharacters = 0;
    				}
    			}
    			espan.html(currentCharacters + "/" + maxlength);
    		});
        </script>
    </div>
</body>

</html>