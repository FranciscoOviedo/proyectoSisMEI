<%@page import="entidades.MicroProyecto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMicroProyecto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">











      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Micro Proyectos</li>
      </ol>
      <div class="row">
        <div class="col">
          <h1>Lista de MicroProyectos</h1>
        </div>
        <div class="col-sm-auto">
          <a href="crearMicroProyecto.jsp" class="btn btn-primary btn-block float-right" role="button"><i class="fa fa-fw fa-plus"></i>Nuevo</a>
        </div>
      </div>
      <hr class="mt-2">
         
          <div class="table-responsive item_container card-body">
          <div class="form-check my-3">
             <label>
             <input type="checkbox" name="check" onchange="toggleInactivos(this)"> <span class="label-text unselectable">Mostrar Inactivos</span>
           </label>
          </div>
          <div class="toggable">
            <table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0">
                <thead>
                    <tr class="thead-dark">
                      <th scope="col">#</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Objetivo General</th>
                      <th scope="col">Tipo</th>
                      <th scope="col">Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
              <%
				DTMicroProyecto dtmicro = new DTMicroProyecto();
				ArrayList<MicroProyecto> listaMicroProyecto = new ArrayList<MicroProyecto>();
		  		listaMicroProyecto = dtmicro.listaMicroProyecto();
				
				//Guardamos esa lista en una Sesión
				ArrayList<MicroProyecto> listaMicro = null;
				listaMicro = dtmicro.listaMicroProyecto();
				session.setAttribute("ListaMicroproyecto", listaMicro);
				int number=1; 
				
				for(MicroProyecto micro : listaMicroProyecto)
				{
			%>
              <tr>
                <td class="order"><% out.print(number); %></td>
                <td><%= micro.getNombre() %></td>
                <td><%= micro.getObjetivoGeneral() %></td>
                <td><%= micro.getTipo() %></td>
                <td>
                  <a class="mr-2 d-inline-block"  href=<% out.print("verMicroProyecto.jsp?id="+micro.getT_MicroProyecto_ID()); %>>
                  <i class="fa fa-fw fa-eye"></i></a>
                  <a class="mr-2 d-inline-block" href=<% out.print("editarMicroProyecto.jsp?id="+micro.getT_MicroProyecto_ID()); %>>
                  <i class="fa fa-fw fa-pencil"></i></a>
                  <a class="d-inline-block"  data-toggle="modal" data-target="#modalEliminar" data-microproyectoid=<% out.print(micro.getT_MicroProyecto_ID()); %>>
                  <i class="fa fa-fw fa-trash"></i></a>
                </td>
              </tr>
              <%
        		number=number+1;
			}
		  	%>
		  	</table>
          </div>
          
          
          <div class="toggable" style="display:none;">
            <table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0">
                <thead>
                    <tr class="thead-dark">
                      <th scope="col">#</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Objetivo General</th>
                      <th scope="col">Tipo</th>
                      <th scope="col">Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
		  <%
		    listaMicroProyecto = dtmicro.listaMicroProyectoInactivos();
		  	number=1;
			for(MicroProyecto micro : listaMicroProyecto)
			{
		  %>
             <tr>
             	<td class="order"><% out.print(number); %></td>
             	<td><%= micro.getNombre() %></td>
                <td><%= micro.getObjetivoGeneral() %></td>
                <td><%= micro.getTipo() %></td>
             	<td>
                	<a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalRestaurar" data-microproyectoid=<% out.print(micro.getT_MicroProyecto_ID()); %>>
                	<i class="fa fa-fw fa-arrow-up"></i></a>
               </td>
             </tr>
             <%
             	number=number+1;
			}
		  %>
                  </tbody>
            </table>
          </div>

			<!-- Modal Deshabilitar-->
			<div id="modalEliminar" class="modal fade" role="dialog">
				<div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Deshabilitar MicroProyecto</h4>
						</div>
						<div class="modal-body">
							<p>Esta seguro que desea deshabilitar este elemento de la lista?.</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<a><button type="button" class="btn btn-primary">Eliminar</button></a>
						</div>
					</div>

				</div>
			</div>

			<!-- Modal Restaurar -->
			<div id="modalRestaurar" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Restaurar MicroProyecto</h4>
						</div>
						<div class="modal-body">
							<p>¿Está seguro que desea restaurar a este elemento de la lista?.</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
							<a><button type="button" class="btn btn-primary">Restaurar</button></a>
						</div>
					</div>
				</div>
			</div>














		</div>
      </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <%@include file="../../footer.jsp" %>
    
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
     <script>
	    $(document).ready(function () {
		   $('#modalEliminar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('microproyectoid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarMicroProyecto?id='+recipient);
		   });
		   $('#modalRestaurar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('microproyectoid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLrestaurarMicroProyecto?id='+recipient);
		   });
	    });
	    function toggleInactivos(element)
	    {
	      $(".toggable").toggle();
	      $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
	    }
    </script>
</body>

</html>
