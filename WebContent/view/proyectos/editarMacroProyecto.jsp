<%@page import="entidades.MacroProyecto"%>
<%@page import="entidades.MacroObjetivo"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMacroProyecto"%>
<%@page import="datos.DTMacroObjetivo"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
   <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">




            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.jsp">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="listaMacroProyecto.jsp">MacroProyectos</a>
                </li>
                <li class="breadcrumb-item active">Editar MacroProyecto</li>
            </ol>

                <h1>Editar MacroProyecto</h1>
                <hr class="mt-2">
                
            <div class="row">
                <div class="col-12">

                    <div class="item_container card-body">
					<form method="POST" action="./SLeditarMacroProyecto">
                    	<%
							ArrayList<MacroProyecto> listaMacro = (ArrayList<MacroProyecto>) session.getAttribute("ListaMacroProyecto");
							String idRequestMacro = request.getParameter("id");
							MacroProyecto macro = null;
							int idMacro = 0;
							try {
								idMacro = Integer.parseInt(idRequestMacro);
							} catch (Exception e) {
								idMacro = 0;
								e.printStackTrace();
								e.getMessage();
							}
							for (MacroProyecto tempM : listaMacro) {
								if (tempM.getT_MacroProyecto_ID() == Integer.parseInt(idRequestMacro)) {
									macro = tempM;
									break;
								}
							}
							DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
						%>

                        <div class="form-group">
	                       <label for="usr">Nombre del Proyecto:</label>
	                       <input type="text" class="form-control" value="<%= macro.getNombre() %>" name="Nombre" id="Nombre">
	                   </div>                        
	                   <div class="row">
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="usr">Fecha de Inicio:</label>
                                    <input type="text" class="form-control" value="<%= macro.getFechaInicio() %>" name="FechaInicio" id="FechaInicio">
                                </div>
                            </div>
                            <div class="col-6 col-md-3">
                                <div class="form-group">
                                    <label for="usr">Fecha de Final:</label>
                                    <input type="text" class="form-control" value="<%= macro.getFechaFin() %>" name="FechaFin" id="FechaFin">
                                </div>                                   
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label " for="ObjetivoGeneral">Objetivo General:</label>
                            <label class="d-inline-block float-right text-secondary"><span></span></label>
                            <textarea class="form-control resize_disabled"  name="ObjetivoGeneral" id="ObjetivoGeneral" rows="4" maxlength="255"><%= macro.getObjetivoGeneral() %></textarea>
                        </div>

                        <div class="form-group row">
                            <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Actualizar</button></div>
                            <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
                        </div>
					</form>


                    </div>
                    
                     





		
		
		
				</div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
        <%@include file="../../footer.jsp" %>

        <!-- Bootstrap core JavaScript-->
        <script src="../../vendor/jquery/jquery.min.js"></script>
        <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
        <script src="../../js/jquery.mask.js"></script>
        
        <script>
            $('.fechaMask').mask('00-00-0000');

            $.datepicker.regional['es'] = {
				closeText: 'Cerrar',
				prevText: '< Ant',
				nextText: 'Sig >',
				currentText: 'Hoy',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
				dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
				weekHeader: 'Sm',
				dateFormat: 'dd/mm/yy',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''
            };

            $.datepicker.setDefaults($.datepicker.regional['es']);

            $('#FechaInicio').datepicker({
                setDate: new Date(),
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            });

            $('#FechaFin').datepicker({
                setDate: new Date(),
                dateFormat: 'dd-mm-yy',
                changeMonth: true,
                changeYear: true
            });
            
            var inputs = "textarea[maxlength]";
    		$(document).on('keyup', "[maxlength]",function(e) {
    			var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
    			espan = este.prev('label').find('span');
    			// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
    			if (document.addEventListener && !window.requestAnimationFrame) {
    				if (remainingCharacters <= -1) {
    					remainingCharacters = 0;
    				}
    			}
    			espan.html(currentCharacters + "/" + maxlength);
    		});
        </script>
    </div>
</body>

</html>