<%@page import="entidades.MicroProyecto"%>
<%@page import="entidades.MacroProyecto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTMacroProyecto"%>
<%@page import="datos.DTMicroProyecto"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">


        
        
          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="../../index.jsp">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="listaMicroProyecto.jsp">MicroProyectos</a>
                </li>
                <li class="breadcrumb-item active">Descripción Micro Proyecto</li>
            </ol>
            <h1>Descripción Micro Proyecto</h1>
            <hr class="mt-2">
            
            <%
							ArrayList<MicroProyecto> listaMicro = (ArrayList<MicroProyecto>) session.getAttribute("ListaMicroproyecto");
							String idRequestMicro = request.getParameter("id");
							MicroProyecto micro = null;
							int idMicro = 0;
							try {
								idMicro = Integer.parseInt(idRequestMicro);
							} catch (Exception e) {
								idMicro = 0;
								e.printStackTrace();
								e.getMessage();
							}
							for (MicroProyecto tempM : listaMicro) {
								if (tempM.getT_MicroProyecto_ID() == Integer.parseInt(idRequestMicro)) {
									micro = tempM;
									break;
								}
							}
							DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
						%>

                <div class="item_container">
                            <div class="card-body font-weight-bold text-truncate lead">
                               <%= micro.getNombre() %>
                            </div>
                            <hr class="my-0">
                            <div class="card-body">
                                <div class="row">
                                  <div class="col-auto text-md-right card-text" style="width:200px"><strong>Fecha de Inicio:</strong></div>
                                  <div class="col-sm card-text"><%= micro.getFechaInicio() %></div>
                                </div>
                                <div class="row">
                                  <div class="col-auto text-md-right card-text" style="width:200px"><strong>Fecha de Fin:</strong></div>
                                  <div class="col-sm card-text"><%= micro.getFechaFin() %></div>
                                </div>
                                <div class="row">
                                  <div class="col-auto text-md-right card-text" style="width:200px"><strong>Familia kolping:</strong></div>
                                  <div class="col-sm card-text">pendiente</div>
                                <div class="row">
                                  <div class="col-auto text-md-right card-text" style="width:200px"><strong>Objetivo General:</strong></div>
                                  <div class="col-sm card-text"><%= micro.getObjetivoGeneral() %></div>
                               <div class="row">
                                  <div class="col-auto text-md-right card-text" style="width:200px"><strong>Tipo:</strong></div>
                                  <div class="col-sm card-text"><%= micro.getTipo() %></div>
									
								</div>
                                </div>
                            </div>
                </div>
                       
                  
        
        
        
        











        
        
        
        
        
        
        
        
        
        
        
        
        
        
        </div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright Â© Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Ã</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

	<!-- Bootstrap core JavaScript-->
	<script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
         
  </div>
</body>

</html>
