<%@page import="entidades.HerramientaCI"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTHerramientaCI"%>
<%@page import="entidades.Familia"%>
<%@page import="datos.DTFamilia"%>
<%@page import="entidades.MicroProyecto"%>
<%@page import="datos.DTMicroProyecto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>SisMEI</title>
<link rel="icon" href="../../img/LogoSisMEI-low-res.png">
<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="../../css/sb-admin.css" rel="stylesheet">
<link href="../../css/content.css" rel="stylesheet">
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaHerramientaCI.jsp">Herramienta CI</a>
        </li>
        <li class="breadcrumb-item active">Crear Herramienta CI</li>
      </ol>
      
      <h1>Crear Herramienta CI</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form id="formFormador" class="form formFormador" method="POST" action="${pageContext.request.contextPath}/SLguardarHerramientaCI">
          <div class="row">
          
            <div class="form-group col-sm-6">
                <label class="control-label" for="T_FamiliaKolping_ID">Familia:</label>
          		<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_FamiliaKolping_ID" name="T_FamiliaKolping_ID">
				<%
					DTFamilia dtf = new DTFamilia();
					ArrayList<Familia> listaFam = dtf.listarFamilias();
					for(Familia f: listaFam)
					{
				%>
						<option value="<%= f.getT_FamiliaKolping_ID() %>"><%= f.getNombre() %></option>
				<%
					}
				%>
	             </select>
           	</div>
            <div class="form-group col-sm-6">
				<label class="control-label" for="T_MicroProyecto_ID">Micro Proyecto:</label>
				<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_MicroProyecto_ID" name="T_MicroProyecto_ID">
				<%
					DTMicroProyecto dtmp = new DTMicroProyecto();
					ArrayList<MicroProyecto> listaMicro = dtmp.listaMicroProyecto();
					for(MicroProyecto mp: listaMicro)
					{
				%>
						<option value="<%= mp.getT_MicroProyecto_ID() %>" data-chained="<%= mp.getT_FamiliaKolping_ID() %>"><%= mp.getNombre() %></option>
				<%
					}
				%>
				</select>
            </div>
            
            <div class="form-group col-sm-6">
              <label class="control-label" for="FechaCreacion">Fecha de Creación:</label >
              <input type="text" class="form-control fechaMask" id="FechaCreacion" name="FechaCreacion" placeholder="Ingrese la fecha de creación de la herramienta CI" required> 
            </div>
            
          </div>
          
          

          <div class="form-group row">
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
              <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
          </div>
        </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <%@include file="../../footer.jsp"%>

    
		<script src="../../vendor/jquery/jquery.min.js"></script>
		<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="../../js/jquery.chained.js"></script>
		<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap-select.js"></script>
		<script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
		<!-- Page level plugin JavaScript-->
		<script>
			//Para las Mascaras y el selectpicker
			$(document).ready(function() {
				$('.fechaMask').mask('00-00-0000');
				//Para los select de Familia y proyecto
				$("#T_MicroProyecto_ID").chained("#T_FamiliaKolping_ID");
				$('.selectpicker').selectpicker();
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				  $('.selectpicker').selectpicker('mobile');
				}
			});
			//Para cambiar el idioma a español del date picker (Para seleccionar fechas)
			$.datepicker.regional['es'] = {
				closeText : 'Cerrar',
				prevText : '< Ant',
   				nextText: 'Sig >',
				currentText : 'Hoy',
				monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
				monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
				dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
				dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb' ],
				dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá' ],
				weekHeader : 'Sm',
				dateFormat : 'dd/mm/yy',
				firstDay : 1,
				isRTL : false,
				showMonthAfterYear : false,
				yearSuffix : ''
			};
			//Para que el datepicker funcione
			$.datepicker.setDefaults($.datepicker.regional['es']);
			$('#FechaCreacion').datepicker({
				setDate : new Date(),
				dateFormat : 'dd-mm-yy',
				changeMonth : true,
				changeYear : true
			});
		</script>
	    <!-- Bootstrap core JavaScript-->
	    
	    
	    <!-- Core plugin JavaScript-->
	    
	    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
	    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
	    <!-- Custom scripts for all pages-->
	    <script src="../../js/sb-admin.min.js"></script>
	    <!-- Custom scripts for this page-->
	    <script src="../../js/tablesLanguage.js"></script>
	    <script src="../../js/jquery.mask.js"></script>
    
    
	</div>
</body>

</html>
