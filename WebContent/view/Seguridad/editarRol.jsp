<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Editar Rol</title>
  
 <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
   
  <!--Sobreescribir -->
  <link href="../../css/content.css" rel="stylesheet"> 
   
</head>


<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  
  
  	<!-- Navigation-->
  	<%@include file="../../navbar.jsp" %>
  	<!-- /Navigation-->
  
  <!-- Page Content -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaRol.jsp">Rol</a>
        </li>
        <li class="breadcrumb-item active">Editar Rol</li>
      </ol>
       <!--/Breadcrumbs-->
       
      <h1>Editar Rol</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form class="form" action="">
            <div class="form-group col-sm-6 col-md-8">
                <label class="control-label " for="temaReunion">Nombre del Rol:</label>
                <input type="text" class="form-control" id="rol" placeholder="Ingresar Rol" required value="Administrador">    
          </div>
          
          
             <br>
            <div class="form-group row">
                <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Guardar</button></div>
                <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
            </div>
        </form>
      </div>
    </div> 
     <!-- /Page Content -->
    
    <!-- footer-->
     <%@include file="../../footer.jsp" %>
    <!-- /footer-->
    
    <!-- Logout Modal-->
    <%@include file="../../logout.jsp" %>
    <!-- /Logout Modal-->
  
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
       
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>  
  </div>
</body>

</html>