<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Lista de Roles</title>
  
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet"> 
  
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  
  	<!-- Navigation-->
  	<%@include file="../../navbar.jsp" %>
  	<!-- /Navigation-->
  
  <!-- Page Content -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Rol</li>
      </ol>
      <!--/Breadcrumbs-->
      
      <div class="row">
        <div class="col"><h1>Lista de Roles</h1></div>
        <div class="col-sm-auto">
          <a href="crearRol.jsp" class="btn btn-primary btn-block float-right" role="button"><i class="fa fa-fw fa-plus"></i>Nuevo</a> 
        </div>
    
      </div>
      <hr class="mt-2">
      
      <div class="table-responsive item_container card-body">
        <table id="dataTable" class="table table-hover" cellspacing="0">
            <thead>
                <tr class="thead-dark">
                  <th scope="col">#</th>
                  <th scope="col">Rol</th>
                  <th scope="col">Opciones</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>Administrador</td>
                  
                 
                  <td>
                    <a class="mr-2 d-inline-block" href="editarRol.jsp">
                    <i class="fa fa-fw fa-pencil"></i></a>
                    <a class="mr-2 d-inline-block" data-toggle="modal" data-target="#eliminarModal">
                    <i class="fa fa-fw fa-trash"></i></a>
                    
                  </td>
                </tr>
              </tbody>
        </table>
      </div>
    </div>
    <!-- /Page Content -->
    
    <!-- footer-->
     <%@include file="../../footer.jsp" %>
    <!-- /footer-->
    
    <!-- Logout Modal-->
    <%@include file="../../logout.jsp" %>
    <!-- /Logout Modal-->
    
    <!-- Eliminar modal -->
    <div class="modal fade" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">�Seguro que desea eliminar este rol?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">�</span>
            </button>
          </div>
          <div class="modal-body">Seleccione "Eliminar" si desea eliminar el rol.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <button class="btn btn-primary" type="button" data-dismiss="modal">Eliminar</button>
            
          </div>
        </div>
      </div>
    </div>
    <!-- /eliminar modal -->
    
   
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    
  </div>
</body>
</html>