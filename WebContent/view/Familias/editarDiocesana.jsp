<%@page import="entidades.Diocesana"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTDiocesana"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listadoDiocesanas.jsp">Diocesana</a>
        </li>
        <li class="breadcrumb-item active">Editar Dicoesana</li>
      </ol>
      
      <h1>Editar Actividad</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form id="formDiocesana" class="form" method="POST" action="${pageContext.request.contextPath}/SLeditarDiocesana">
          <%
			ArrayList<Diocesana> listaDiocesanas= (ArrayList<Diocesana>) session.getAttribute("ListaDiocesanas");
			String idRequestDiocesana = request.getParameter("id");
			Diocesana dio = null;
			int idDiocesana = 0;
			try {
				idDiocesana = Integer.parseInt(idRequestDiocesana);
			} catch (Exception e) {
				idDiocesana = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (Diocesana tempDio: listaDiocesanas) {
				if (tempDio.getT_Diocesana_ID() == Integer.parseInt(idRequestDiocesana)) {
					dio = tempDio;
					break;
				}
			}
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		  %>
          <input type="text" name="T_Diocesana_ID" id="T_Diocesana_ID" value="<%=dio.getT_Diocesana_ID() %>" hidden="true">
          <div class="row">
          
            
            <div class="form-group col-sm-6 col-md-12">
                <label class="control-label" for="Nombre">Nombre:</label>
                <input type="text" class="form-control" id="Nombre" name="Nombre" value="<%=dio.getNombre()%>" placeholder="Ingresar nombre de la actividad" required>
            </div>
            
            </div>
           
		  <div class="form-group row">
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Actualizar</button></div>
              <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
          </div>
        </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Ã</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
  
  <script type="text/javascript" src="../../js/inputmask.js"></script>
  <script type="text/javascript" src="../../js/inputmask.extensions.js"></script>
  <script type="text/javascript" src="../../js/inputmask.numeric.extensions.js"></script>
  <script type="text/javascript" src="../../js/jquery.inputmask.js"></script>
  
  <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
  <script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
  <!-- Page level plugin JavaScript-->
    <script>
	var options = {
		onInvalid: function(val, e, f, invalid, options){
			var error = invalid[0];
			console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);
		}
	};
	$(document).ready(function(){
        $('.fechaMask').mask('00-00-0000');
        $('.dineromask').inputmask({alias: 'currency', 
            allowMinus: false,  
            digits: 2,
            prefix: "",
            showMaskOnFocus: true});
        $('.selectpicker').selectpicker();
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		  $('.selectpicker').selectpicker('mobile');
		  $('.fechaMask').prop('readonly', true);
		}
	});
	
	$.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
      };
      $.datepicker.setDefaults($.datepicker.regional['es']);
      $('#Fecha').datepicker({
        setDate : new Date(),
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear : true
      });
	 
	  var inputs = "textarea[maxlength]";
	  $(document).on('keyup', "[maxlength]", function(e) {
			var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
			espan = este.prev('label').find('span');
			// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
			if (document.addEventListener
					&& !window.requestAnimationFrame) {
				if (remainingCharacters <= -1) {
					remainingCharacters = 0;
				}
			}
			espan.html(currentCharacters + "/" + maxlength);
	   });
</script>
    <!-- Bootstrap core JavaScript-->
    
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script src="../../js/jquery.mask.js"></script>
    
    
  </div>
</body>

</html>