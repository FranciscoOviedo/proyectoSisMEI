<%@page import="entidades.Familia"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTFamilia"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="es">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>SisMEI</title>
	<link rel="icon" href="../../img/LogoSisMEI-low-res.png">
	<!-- Bootstrap core CSS-->
	<link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Custom styles for this template-->
	<link href="../../css/sb-admin.css" rel="stylesheet">
	<link href="../../css/content.css" rel="stylesheet"> 
  
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
	<!-- Navigation-->
	<%@include file="../../navbar.jsp" %>
	<div class="content-wrapper">
		<div class="container-fluid">






















     	<!-- Breadcrumbs-->
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="../../index.jsp">Dashboard</a></li>
			<li class="breadcrumb-item active">Familia</li>
		</ol>
		
		<div class="row">
			<div class="col">
				<h1>Lista de Familias</h1>
			</div>
			<div class="col-sm-auto">
				<a href="crearFamilia.jsp" class="btn btn-primary btn-block float-right" role="button"><i class="fa fa-fw fa-plus"></i>Nuevo</a>
			</div>
		</div>
		<hr class="mt-2">

		<div class="item_container card-body">
			<div class="form-check my-3">
				<label> <input type="checkbox" name="check" onchange="toggleInactivos(this)"> <span class="label-text unselectable">Mostrar Inactivos</span> </label>
			</div>
			<div class="toggable">
				<table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0">
					<thead>
						<tr class="thead-dark">
					    <th scope = "col"> # </ th>
                        <th scope = "col"> Nombre </ th>
                        <th scope = "col"> Distancia </ th>
                        <th scope = "col"> Tiempo Viaje </ th>
                        <th scope = "col"> Tipo </ th>
                         <th scope = "col"> Opciones </ th>

						</tr>
					</thead>
					<tbody>
						<%
							DTFamilia dtfa = new DTFamilia();
							ArrayList<Familia> listaFamilias = new ArrayList<Familia>();
							listaFamilias = dtfa.listarFamilias();
			
							//Guardamos esa lista en una Sesión
							ArrayList<Familia> listaFa = null;
							listaFa = dtfa.listarFamilias();
							session.setAttribute("ListaFamilias", listaFa);
							int number = 1;
			
							DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
							for (Familia fa : listaFamilias) {
						%>
						<tr>
							<td><%=number%></td>
							<td><%=fa.getNombre()%></td>
							<td><%= fa.getDistancia() %></td>
                            <td><%= fa.getTiempoViaje() %></td>
                            <td><%= fa.getTipo() %></td>

							<td>
							<a class="btn btn-sm btn-light	d-inline-block" href=<%out.print("listaFamilia.jsp?id=" + fa.getT_FamiliaKolping_ID());%>> <i class="fa fa-fw fa-eye"></i></a> 
							<a class="btn btn-sm btn-light	d-inline-block" href=<%out.print("editarFamilia.jsp?id=" + fa.getT_FamiliaKolping_ID());%>> <i class="fa fa-fw fa-pencil"></i></a>
							<a class="btn btn-sm btn-danger	d-inline-block" href="" data-toggle="modal" data-target="#modalEliminar" data-familiaid=<%out.print(fa.getT_FamiliaKolping_ID());%>> <i class="fa fa-fw fa-trash"></i></a>
							</td>
						</tr>
						<%
							number = number + 1;
							}
						%>
					</tbody>
				</table>
			</div>
			<div class="toggable" style="display: none;">
				<table class="table table-hover dataTable" width="100%" item-width="100%" cellspacing="0">
					<thead>
						<tr class="thead-dark">
							
                <th scope = "col"> # </ th>
                <th scope = "col"> Nombre </ th>
                <th scope = "col"> Distancia </ th>
                <th scope = "col"> Tiempo Viaje </ th>
                <th scope = "col"> Tipo </ th>
                <th scope = "col"> Opciones </ th>

						</tr>
					</thead>
					<tbody>
						
						<%
							listaFamilias = dtfa.listarFamiliasInactivas();
							number = 1;
							for (Familia fa : listaFamilias) {
						%>
						<tr>
						
							<td><%= number %></td>
                            <td><%= fa.getNombre() %></td>
                            <td><%= fa.getDistancia() %></td>
                            <td><%= fa.getTiempoViaje() %></td>
                           <td><%= fa.getTipo() %></td>
                            <td><a class="mr-2 d-inline-block" data-toggle="modal" data-target="#modalRestaurar" data-familiaid=<%out.print(fa.getT_FamiliaKolping_ID());%>> <i class="fa fa-fw fa-arrow-up"></i></a></td>
						
						</tr>
						<%
							number = number + 1;
							}
						%>
					</tbody>
				</table>
			</div>
		</div>

		<!-- Modal Eliminar -->
		<div id="modalEliminar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Dar de Baja a Familia</h4>
					</div>
					<div class="modal-body">
						<p>¿Esta seguro que desea dar de baja a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Dar de baja</button></a>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal Restaurar -->
		<div id="modalRestaurar" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Restaurar a Familia</h4>
					</div>
					<div class="modal-body">
						<p>¿Está seguro que desea restaurar a este elemento de la lista?.</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
						<a><button type="button" class="btn btn-primary">Restaurar</button></a>
					</div>
				</div>
			</div>
		</div>



















	</div>
    <%@include file="../../footer.jsp"%>
    
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script><!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script>
	    $(document).ready(function () {
		   $('#modalEliminar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('familiaid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLeliminarFamilia?id='+recipient);
		   });
		   $('#modalRestaurar').on('show.bs.modal', function (event) {
		   	  var button = $(event.relatedTarget)
		   	  var recipient = button.data('familiaid')
		   	  var modal = $(this)
		   	  modal.find('.modal-footer a').attr('href', '/SisMEI/SLrestaurarFamilia?id='+recipient);
		   });
	    });
	    function toggleInactivos(element)
	    {
	      $(".toggable").toggle();
	      $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
	    }
    </script>
    
  </div>
</body>

</html>
