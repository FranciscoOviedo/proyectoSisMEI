<%@page import="entidades.Familia"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTFamilia"%>
<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">

  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black">
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">






















      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="../../index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
          <a href="listadoFamilias.jsp">Familia</a>
        </li>
        <li class="breadcrumb-item active">Descripcion de Familia</li>
      </ol>
      <h1>Descripcion de Familia</h1>
      <hr class="mt-2">

    
        <hr class="my-0">
        <!--div class="card-body">
              <div class="row">
                <div class="col-auto text-md-right card-text" style="width:120px"><strong>Tipo:</strong></div>
                <div class="col-sm card-text">Voluntario</div>
              </div>
              <div class="row">
                <div class="col-auto text-md-right card-text" style="width:120px"><strong>Telefono:</strong></div>
                <div class="col-sm card-text">81125415</div>
              </div>
              <div class="row">
                <div class="col-auto text-md-right card-text" style="width:120px"><strong>Edad:</strong></div>
                <div class="col-sm card-text">22 años</div>
              </div>
              <div class="row">
                <div class="col-auto text-md-right card-text" style="width:120px"><strong>Sexo:</strong></div>
                <div class="col-sm card-text">Masculino</div>
              </div>
            </div-->


        <!--div class="card-body row">
              <div class="col-sm-12">
                <strong>Cedula:</strong>
                <div class="card-text">001-123195-0001B</div>
              </div>
              <div class="col-sm">
                <strong>Telefono:</strong>
                <div class="card-text">81125415</div>
              </div>
              <div class="col-sm-auto">
                <strong>Edad:</strong>
                <div class="card-text">22 años</div>
              </div>
              <div class="col-sm-12">
                <strong>Tipo:</strong>
                <div class="card-text">Voluntario</div>
              </div>
              <div class="col-sm-12">
                <strong>Sexo:</strong>
                <div class="card-text">Masculino</div>
              </div>
            </div-->
		
		<%
			ArrayList<Familia> listaFamilias = (ArrayList<Familia>) session.getAttribute("ListaFamilias");
			String idRequestFamilia = request.getParameter("id");
			Familia fa = null;
			int idFamilia = 0;
			try {
				idFamilia= Integer.parseInt(idRequestFamilia);
			} catch (Exception e) {
				idFamilia = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (Familia tempFa : listaFamilias) {
				if (tempFa.getT_FamiliaKolping_ID() == Integer.parseInt(idRequestFamilia)) {
					fa = tempFa;
					break;
				}
			}
			DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		%>
		
		 <div class="card-body font-weight-bold text-truncate lead">
          <% out.print(fa.getNombre()+" "); %>
        </div>
        <hr class="my-0">
        <div class="card-body row">
		
		
       <!-- <div class="card-body row">
          <div class="col-md-6 col-sm-6" style="padding:10px">
            <strong>Nombre:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= fa.getNombre() %></div>
          </div>
           </div-->
          
          <div class="col-md-5 col-sm-6" style="padding:10px">
            <strong>Distancia:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= fa.getDistancia() %></div>
          </div>
          <div class="col-md-5 col-sm-6" style="padding:10px">
            <strong>Tiempo de Viaje:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= fa.getTiempoViaje() %></div>
          </div>
          <div class="col-md-5 col-sm-6" style="padding:10px">
            <strong>Tipo:</strong>
            <hr class="my-0">
            <div class="text-secondary"><%= fa.getTipo() %></div>
          </div>
          
          </div>
        </div>



      </div>































    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="../../vendor/jquery/jquery.min.js"></script>
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
  </div>
</body>



</html>
