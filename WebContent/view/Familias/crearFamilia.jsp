<%@page import="entidades.Familia"%>
<%@page import="java.util.ArrayList"%>
<%@page import="entidades.Diocesana"%>
<%@page import="datos.DTDiocesana"%>
<%@page import="datos.DTFamilia"%>
<%@page import="entidades.Departamento"%>
<%@page import="datos.DTDepartamento"%>
<%@page import="entidades.Municipio"%>
<%@page import="datos.DTMunicipio"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>SisMEI</title>
<link rel="icon" href="../../img/LogoSisMEI-low-res.png">
<!-- Bootstrap core CSS-->
<link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="../../css/sb-admin.css" rel="stylesheet">
<link href="../../css/content.css" rel="stylesheet">
<link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
<link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
<link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">










      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listadoFamilias.jsp">Familia</a>
        </li>
        <li class="breadcrumb-item active">Crear Familia</li>
      </ol>
      
      <h1>Crear Familia</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
       <form id="formFormador" class="form formFormador" method="POST" action="${pageContext.request.contextPath}/SLguardarFamilia">
        <div class="row">
          
  	<div class="form-group col-sm-6 col-md-6">
              <label class="control-label" for="T_Diocesana_ID">Diocesana:</label>
              <select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" id="T_Diocesana_ID" name="T_Diocesana_ID">
                <option value="0">Ninguno</option>
                <%
					DTDiocesana dtdio = new DTDiocesana();
					ArrayList<Diocesana> listaDio = dtdio.listarDiocesanas();
					for(Diocesana dio: listaDio)
					{
				%>
                <option value="<%= dio.getT_Diocesana_ID() %>"><%= dio.getNombre() %></option>
                <%
					}
                %>
              </select>
            </div>

            <div class="form-group col-sm-6 col-md-5">
                <label class="control-label" for="Nombre">Nombres:</label>
                <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Ingrese los nombres" required>
            </div>
            <div class="form-group col-sm-6 col-md-6">
              <label class="control-label" for="Distancia">Distancia:</label>
              <div class="input-group">
	            <div class="input-group-prepend">
			    <div class="input-group-text">hr</div>
				 </div>
	             <input type="tel" class="form-control dinomask" id="Distancia" name="Distancia" placeholder="Ingrese la Distancia">
       		  </div>
            </div>
             
 
             <div class="form-group col-sm-6 col-md-5">
              <label class="control-label" for="TiempoViaje">Tiempo de Viaje</label>
              <!--input type="number" step="any" min="0" class="form-control" id="TiempoViaje" name="TiempoViaje" placeholder="Ingresar estimado del gasto"-->
              <div class="input-group">
	            <div class="input-group-prepend">
			    <div class="input-group-text">Km</div>
				 </div>
	             <input type="tel" class="form-control dineromask" id="TiempoViaje" name="TiempoViaje" placeholder="Ingresar estimado del gasto">
       		  </div>
            </div>
            
           
            <div class="form-group col-sm-6 col-md-4">
              <label class="control-label" for="Tipo">Tipo:</label>
              <select class="form-control" id="Tipo" name="Tipo">
                <option value="Grupo">Grupo</option>
                <option value="Familia">Familia</option>
              </select>
            </div>
            
            
             <div class="form-group col-sm-6 col-md-4">
                <label class="control-label" for="T_Departamento_ID">Departamento:</label>
          		<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Departamento_ID" name="T_Departamento_ID">
				<%
					DTDepartamento dtd = new DTDepartamento();
					ArrayList<Departamento> listaDep = dtd.listarDepartamentos();
					for(Departamento d: listaDep)
					{
				%>
						<option value="<%= d.getT_Departamento_ID() %>"><%= d.getNombre() %></option>
				<%
					}
				%>
	             </select>
           	</div>
           	
           	
           	
           <div class="form-group col-sm-6 col-md-4">
				<label class="control-label" for="T_Municipio_ID">Municipio:</label>
				<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_Municipio_ID" name="T_Municipio_ID">
				<%
					DTMunicipio dtm = new DTMunicipio();
					ArrayList<Municipio> listaMun = dtm.listarMunicipios();
					for(Municipio m: listaMun)
					{
				%>
						<option value="<%= m.getT_Municipio_ID() %>" data-chained="<%= m.getT_Departamento_ID() %>"><%= m.getNombre() %></option>
				<%
					}
				%>
				</select>
            </div>
            
            
          </div>
          
          
           
          <div class="form-group row">
          
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
              <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
          </div>
         
        </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
</div>
<%@include file="../../footer.jsp"%>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">Ã</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    
		<script src="../../vendor/jquery/jquery.min.js"></script>
		<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
		<script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="../../js/jquery.chained.js"></script>
		<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<script type="text/javascript" src="../../js/bootstrap-select.js"></script>
		<script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
		
 <script type="text/javascript" src="../../js/inputmask.js"></script>
  <script type="text/javascript" src="../../js/inputmask.extensions.js"></script>
  <script type="text/javascript" src="../../js/inputmask.numeric.extensions.js"></script>
  <script type="text/javascript" src="../../js/jquery.inputmask.js"></script>
  
  <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
  <script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
		
		
		
	<!-- Page level plugin JavaScript-->
    <script>
	var options = {
		onInvalid: function(val, e, f, invalid, options){
			var error = invalid[0];
			console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);
		}
	};
	$(document).ready(function(){
        $('.fechaMask').mask('00-00-0000');
        $('.dineromask').inputmask({alias: 'currency', 
            allowMinus: false,  
            digits: 2,
            prefix: "",
            showMaskOnFocus: true});
        
        $('.dinomask').inputmask({alias: 'currency', 
            allowMinus: false,  
            digits: 0,
            prefix: "",
            showMaskOnFocus: true});
        
        
        $('.selectpicker').selectpicker();
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		  $('.selectpicker').selectpicker('mobile');
		  $('.fechaMask').prop('readonly', true);
		}
	});
	
			//Para las Mascaras y el selectpicker
			
	
			$(document).ready(function() {
				$('.cedulaMask').mask('000-000000-0000S', {
					'translation' : {
						S : { pattern : /[A-Z]/ }
					}
				});
				$('.fechaMask').mask('00-00-0000');
				$('.telefonoMask').mask('0000-0000');
				
				
				//Para los select de municipio y departamento
				$("#T_Municipio_ID").chained("#T_Departamento_ID");
				$('.selectpicker').selectpicker();
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				  $('.selectpicker').selectpicker('mobile');
				  $('.fechaMask').prop('readonly', true);
				}
			});
			
			
			
			//Para cambiar el idioma a español del date picker (Para seleccionar fechas)
			$.datepicker.regional['es'] = {
				closeText : 'Cerrar',
				prevText : '< Ant',
   				nextText: 'Sig >',
				currentText : 'Hoy',
				monthNames : [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
				monthNamesShort : [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
				dayNames : [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
				dayNamesShort : [ 'Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb' ],
				dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá' ],
				weekHeader : 'Sm',
				dateFormat : 'dd/mm/yy',
				firstDay : 1,
				isRTL : false,
				showMonthAfterYear : false,
				yearSuffix : ''
			};
			//Para que el datepicker funcione
			$.datepicker.setDefaults($.datepicker.regional['es']);
			$('#FechaNacimiento').datepicker({
				setDate : new Date(),
				dateFormat : 'dd-mm-yy',
				changeMonth : true,
				changeYear : true,
				yearRange : "-200:c+nn",
				maxDate : "-0d"
			});
			$('#FechaUnion').datepicker({
				setDate : new Date(),
				dateFormat : 'dd-mm-yy',
				changeMonth : true,
				changeYear : true,
				yearRange : "-200:c+nn",
				maxDate : "-0d"
			});
		</script>
	    <!-- Bootstrap core JavaScript-->
	    
	    
	    <!-- Core plugin JavaScript-->
	    
	    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
	    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
	    <!-- Custom scripts for all pages-->
	    <script src="../../js/sb-admin.min.js"></script>
	    <!-- Custom scripts for this page-->
	    <script src="../../js/tablesLanguage.js"></script>
	    <script src="../../js/jquery.mask.js"></script>
    
    
	</div>
</body>

</html>
