<%@page import="entidades.TipoGasto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTTipoGasto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaTipoGasto.jsp">Tipo de Gasto</a>
        </li>
        <li class="breadcrumb-item active">Crear Tipo de Gasto</li>
      </ol>
      
      <h1>Crear Tipo de Gasto</h1>
      <hr class="mt-2">
      
      <div class="mx-auto col-sm-10">
      
	      <div class="item_container card-body">
	        <form id="formFormador" class="form" method="POST" action="${pageContext.request.contextPath}/SLguardarTipoGasto">
	          <div class="row">
	          	
	            <div class="form-group col-12">
	                <label class="control-label" for="Nombre">Nombre:</label>
	                <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Ingrese el nombre del tipo de gasto" required>
	            </div>
	            
			  </div>
			  <div class="form-group row">
	              <div class="col-6"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
	              <div class="col-6"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
	          </div>
	        </form>
	      </div>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <%@include file="../../footer.jsp"%>
    
	<script src="../../vendor/jquery/jquery.min.js"></script>
	<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../js/sb-admin.min.js"></script>
  </div>
</body>