<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SB Admin - Start Bootstrap Template</title>
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaCuota.jsp">Cuota</a>
        </li>
        <li class="breadcrumb-item active">Editar Cuota</li>
      </ol>
      
      <h1>Editar Cuota</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form id="formFormador" class="form formFormador" action="">
          <div class="row">
            <div class="form-group col-sm-6 col-md-5">
              <label class="control-label" for="cancelado">Cancelado:</label>
              <select class="form-control" id="cancelado">
                <option value="preescolar">Si</option>
                <option value="primaria">No</option>
                
              </select>
            </div>            
            
            
            
            <div class="form-group col-sm-6 col-md-6">
              <label class="control-label" for="fechaNacimiento">Fecha de Cuota:</label >
              <input type="text" id="fechaUni" class="form-control fechaMask" name="fechUni" title="La Fecha de Cuota es requerida" placeholder="12/03/18" required> 
              <!--input type="text" class="form-control" value="02-16-2012"-->
            </div>
            
            
            
            
           
            
            
            
            
            
          </div>

          <div class="form-group row">
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-danger">Cancelar</button></div>
          </div>
        </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2018</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
  <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
  <script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
  <!-- Page level plugin JavaScript-->
    
    
    <script>
      $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'],
        dayNamesShort: ['Dom','Lun','Mar','Mi�','Juv','Vie','S�b'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S�'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
      };
      $.datepicker.setDefaults($.datepicker.regional['es']);
      $('#fechanac').datepicker({
        setDate : new Date(),
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear : true,
        yearRange : "-200:c+nn",
        maxDate: "-0d"
      });
      $('#fechaUni').datepicker({
        setDate : new Date(),
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear : true,
        yearRange : "-200:c+nn",
        maxDate: "-0d"
      });
    </script>
    <!-- Bootstrap core JavaScript-->
    
    <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script src="../../js/jquery.mask.js"></script>
    
    
  </div>
</body>

</html>
