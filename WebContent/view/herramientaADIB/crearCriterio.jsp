<%@page import="entidades.CriterioADIB"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTCriterioADIB"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaCategoriaCICC.jsp">Criterio de Bienestar</a>
        </li>
        <li class="breadcrumb-item active">Crear Criterio</li>
      </ol>
      
      <h1>Crear Criterio de Bienestar</h1>
      <hr class="mt-2">
      
      <div class="mx-auto col-sm-10">
      
	      <div class="item_container card-body">
	        <form id="formFormador" class="form" method="POST" action="${pageContext.request.contextPath}/SLguardarCriterioBienestar">
	          <div class="row">
	          	
	            <div class="form-group col-12">
	                <label class="control-label" for="Nombre">Nombre:</label>
	                <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Ingrese el nombre del Criterio" required>
	            </div>
	            
	            <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Descripcion:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Descripcion" name="Descripcion" maxlength="255" placeholder="Ingrese la descripcion del Criterio" required></textarea>
	            </div>
	            
	             <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Categoria 1:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Categoria1" name="Categoria1" maxlength="255" placeholder="Ingrese la categoria 1" required></textarea>
	            </div>
	            
	            <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Categoria 2:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Categoria2" name="Categoria2" maxlength="255" placeholder="Ingrese la categoria 2" required></textarea>
	            </div>
	            
	            <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Categoria 3:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Categoria3" name="Categoria3" maxlength="255" placeholder="Ingrese la categoria 3" required></textarea>
	            </div>
	            
	            <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Categoria 4:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Categoria4" name="Categoria4" maxlength="255" placeholder="Ingrese la categoria 4" required></textarea>
	            </div>
	            
	            <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Categoria 5:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Categoria5" name="Categoria5" maxlength="255" placeholder="Ingrese la categoria 5" required></textarea>
	            </div>
	            
			  </div>
			  <div class="form-group row">
	              <div class="col-6"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
	              <div class="col-6"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
	          </div>
	        </form>
	      </div>
      </div>
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <%@include file="../../footer.jsp"%>
    
	<script src="../../vendor/jquery/jquery.min.js"></script>
	<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../js/sb-admin.min.js"></script>
    <script>
	var inputs = "textarea[maxlength]";
	$(document).on('keyup', "[maxlength]", function(e) {
		var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
		espan = este.prev('label').find('span');
		// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
		if (document.addEventListener
			&& !window.requestAnimationFrame) {
		if (remainingCharacters <= -1) {
			remainingCharacters = 0;
		}
	}
	espan.html(currentCharacters + "/" + maxlength);
	 });
    </script>
  </div>
