<%@page import="entidades.ActividadProyecto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTActividadProyecto"%>
<%@page import="entidades.Formador"%>
<%@page import="datos.DTFormador"%>
<%@page import="entidades.MicroProyecto"%>
<%@page import="datos.DTMicroProyecto"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaActividad.jsp">Actividad</a>
        </li>
        <li class="breadcrumb-item active">Crear Actividad</li>
      </ol>
      
      <h1>Crear Actividad</h1>
      <hr class="mt-2">
      
      <div class="item_container card-body">
        <form id="formFormador" class="form" method="POST" action="${pageContext.request.contextPath}/SLguardarActividad">
          <div class="row">
          	
            
          	<div class="form-group col-sm-6 col-md-6">
              <label class="control-label" for="T_MicroProyecto_ID">Micro Proyecto:</label>
              <select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" id="T_MicroProyecto_ID" name="T_MicroProyecto_ID">
                <option value="0">Ninguno</option>
                <%
					DTMicroProyecto dtmp = new DTMicroProyecto();
					ArrayList<MicroProyecto> listaMicro = dtmp.listaMicroProyecto();
					for(MicroProyecto mp: listaMicro)
					{
				%>
                <option value="<%= mp.getT_MicroProyecto_ID() %>"><%= mp.getNombre() %></option>
                <%
					}
                %>
              </select>
            </div>
          	
          	<div class="form-group col-sm-6 col-md-6">
              <label class="control-label" for="T_Formador_ID">Formador:</label>
              <select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" id="T_Formador_ID" name="T_Formador_ID" required>
              <!-- multiple -->
              	<option value="">Debe seleccionar uno</option>
                <%
					DTFormador dtf = new DTFormador();
					ArrayList<Formador> listaFor = dtf.listarFormadores();
					for(Formador f: listaFor)
					{
				%>
                <option value="<%= f.getT_Formador_ID() %>"><%= f.getNombre() %> <%= f.getApellido() %></option>
                <%
					}
                %>
              </select>
            </div>
            
            <div class="form-group col-sm-6 col-md-12">
                <label class="control-label" for="Nombre">Nombre:</label>
                <input type="text" class="form-control" id="Nombre" name="Nombre" placeholder="Ingrese el nombre de la actividad" required>
            </div>
            
            <div class="form-group col-sm-6 col-md-7">
              <label class="control-label" for="Estimado">Estimado:</label>
              <!--input type="number" step="any" min="0" class="form-control" id="Estimado" name="Estimado" placeholder="Ingresar estimado del gasto"-->
              <div class="input-group">
	             <div class="input-group-prepend">
				    <div class="input-group-text">C$</div>
				 </div>
	             <input type="tel" class="form-control dineromask" id="Estimado" name="Estimado" placeholder="Ingrese el estimado a gastar">
       		  </div>
            </div>
            
            <div class="form-group col-sm-6 col-md-5">
              <label class="control-label" for="Fecha">Fecha de Realización:</label >
              <input type="text" class="form-control fechaMask" id="Fecha" name="Fecha" placeholder="Ingrese la fecha en que se realizará la actividad" required> 
            </div>
            
            <div class="form-group col-sm-12">
				<label class="control-label " for="Descripcion">Descripción:</label>
				<label class="d-inline-block float-right text-secondary"><span></span></label>
				<textarea class="form-control resize_disabled" rows="4" id="Descripcion" name="Descripcion" maxlength="255" placeholder="Ingrese una breve descripción de la actividad" required></textarea>
            </div>
            
		  </div>
		  <div class="form-group row">
              <div class="col-6 col-md-3"><button type="submit" class="btn btn-block btn-success">Crear</button></div>
              <div class="col-6 col-md-3"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
          </div>
        </form>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <%@include file="../../footer.jsp"%>

    
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
  
  <script type="text/javascript" src="../../js/inputmask.js"></script>
  <script type="text/javascript" src="../../js/inputmask.extensions.js"></script>
  <script type="text/javascript" src="../../js/inputmask.numeric.extensions.js"></script>
  <script type="text/javascript" src="../../js/jquery.inputmask.js"></script>
  
  <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
  <script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
  <!-- Page level plugin JavaScript-->
    <script>
	var options = {
		onInvalid: function(val, e, f, invalid, options){
			var error = invalid[0];
			console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);
		}
	};
	$(document).ready(function(){
        $('.fechaMask').mask('00-00-0000');
        $('.dineromask').inputmask({alias: 'currency', 
            allowMinus: false,  
            digits: 2,
            prefix: "",
            showMaskOnFocus: true});
        $('.selectpicker').selectpicker();
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		  $('.selectpicker').selectpicker('mobile');
		  $('.fechaMask').prop('readonly', true);
		}
	});
	
	$.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
      };
      $.datepicker.setDefaults($.datepicker.regional['es']);
      $('#Fecha').datepicker({
        setDate : new Date(),
        dateFormat : 'dd-mm-yy',
        changeMonth: true,
        changeYear : true
      });
	 
	  var inputs = "textarea[maxlength]";
	  $(document).on('keyup', "[maxlength]", function(e) {
			var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
			espan = este.prev('label').find('span');
			// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
			if (document.addEventListener
					&& !window.requestAnimationFrame) {
				if (remainingCharacters <= -1) {
					remainingCharacters = 0;
				}
			}
			espan.html(currentCharacters + "/" + maxlength);
	   });
</script>

    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script src="../../js/jquery.mask.js"></script>
    
    
  </div>
</body>

</html>