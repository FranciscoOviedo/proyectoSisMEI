<%@page import="vistas.V_Info_ActividadProyecto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTActividadProyecto"%>
<%@page import="entidades.Gasto"%>
<%@page import="vistas.V_Gasto_TipoGasto"%>
<%@page import="datos.DTGasto"%>
<%@page import="entidades.TipoGasto"%>
<%@page import="datos.DTTipoGasto"%>

<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaActividad.jsp">Actividad</a>
        </li>
        <li class="breadcrumb-item active">Detalle Actividad</li>
      </ol>
      
      <h1>Detalle Actividad</h1>
      <hr class="mt-2">
      
      <div class="item_container">
          <%
			ArrayList<V_Info_ActividadProyecto> listaActividades= (ArrayList<V_Info_ActividadProyecto>) session.getAttribute("ListaActividades");
			String idRequestActividad = request.getParameter("id");
			V_Info_ActividadProyecto a = null;
			int idActividad = 0;
			try {
				idActividad = Integer.parseInt(idRequestActividad);
			} catch (Exception e) {
				idActividad = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (V_Info_ActividadProyecto tempA : listaActividades) {
				if (tempA.getT_Actividad_ID() == idActividad) {
					a = tempA;
					break;
				}
			}
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		  %>
          
          <div class="card-body font-weight-bold text-truncate lead">
            <%=a.getNombre()%>
          </div>
          <hr class="my-0">
          <div class="card-body">
            <p class="text-muted"><% if(a.getNombreMicroProyecto() != null){
                	out.print("Pertenece al micro proyecto <strong class='text-nowrap'>" + a.getNombreMicroProyecto() + "</strong>");
                }else{
                	out.print("No pertenece a un micro proyecto en especifico");
                }
                out.print(". El "+format.format(a.getFecha())); %></p>
            
           	<p><%=a.getDescripcion()%></p>
            
		  </div>
		  <div class="card-footer text-muted row">
			<div class="col-6 col-sm-8 text-muted"><% out.print("Ha cargo de <strong class='text-nowrap'>" + a.getNombreFormador() + " " + a.getApellidoFormador() + "</strong>"); %> </div>
            <div class="col-6 col-sm-4 text-right text-success"><% out.print("C$" + String.format("%,.2f", a.getEstimado())); %> </div>
		  </div>
        
        
        
        
      </div>
      
      
      <div class="item_container card-body">
		<div class="row my-4">
			<h2 class="col mb-0">Gastos:</h2>
			<button data-toggle="modal" data-target="#modalCrearGasto" class="col-auto mr-3 btn btn-success addnewrow"> <span class="fa fa-fw fa-plus"></span> </button>
		</div>

		<div class="table-responsive">
			<table class="table table-hover" id="tab_logic" cellspacing="0" width="100%">
				<thead>
					<tr class="thead">
						<th> <p>#</p> </th>
						<th> <p>Tipo Gasto</p> </th>
						<th> <div style="min-width: 400px"><p>Descripcion</p></div> </th>
						<th> <p>Cantidad</p> </th>
						<th> <p>Opciones</p> </th>
					</tr>
				</thead>
				<tbody>
				<%
		           	DTGasto dtg = new DTGasto();
					ArrayList<V_Gasto_TipoGasto> listaGastoDetalle = new ArrayList<V_Gasto_TipoGasto>();
					listaGastoDetalle = dtg.listarVistaGasto_TipoGasto(idActividad);
					int number=1;
					
					for(V_Gasto_TipoGasto g : listaGastoDetalle)
					{
				%>
					<tr>
						<td class="order"><% out.print(number); %></td>
						<td><span class="badge badge-secondary"><%= g.getNombreTipoGasto() %></span></td>
						<td><%= g.getDescripcionGasto() %></td>
						<td class="text-right"><% out.print("C$"+String.format("%,.2f",g.getCantidad() )); %></td>
						<td>
							<a class="btn btm-sm btn-light d-inline-block" href="" data-toggle="modal" data-target="#modalEditarGasto"
								data-gastoid="<% out.print(g.getT_Gasto_ID()); %>"
								data-gastotipo="<% out.print(g.getT_TipoGasto_ID()); %>"
								data-gastodescripcion="<% out.print(g.getDescripcionGasto()); %>"
								data-gastocantidad="<% out.print(g.getCantidad()); %>">
								<i class="fa fa-fw fa-pencil"></i>
							</a>
							<a class="btn btm-sm btn-danger d-inline-block" href="" data-toggle="modal" data-target="#modalEliminar" data-gastoid=<% out.print(g.getT_Gasto_ID()); %>> <i class="fa fa-fw fa-minus"></i></a>
						</td>
					</tr>
				<%
						number++;
					}
					if(listaGastoDetalle.size()==0){out.print("<tr><td colspan='5'>Aún no se han ingresado gastos</td></tr>");}
				%>
				</tbody>
			</table>
		</div>
	  </div>





		<!-- Modal Crear Gasto -->
		<div id="modalCrearGasto" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Agregar un Gasto</h4>
					</div>
					<form id="formGasto" class="form formFormador" method="POST" action="${pageContext.request.contextPath}/SLguardarGasto">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_Actividad_ID de la actividad que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_Actividad_ID" id="T_Actividad_ID" value="<%=idActividad%>" hidden="true">
							
							<div class="form-group">
								<label for="T_TipoGasto_ID" class="col-form-label">Tipo de Gasto:</label>
								<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_TipoGasto_ID" name="T_TipoGasto_ID">
								<%
									DTTipoGasto dttg = new DTTipoGasto();
									ArrayList<TipoGasto> listaGast = dttg.listarTipoGastos();
									for(TipoGasto tg: listaGast)
									{
								%>
									<option value="<%= tg.getT_TipoGasto_ID() %>"><%= tg.getNombre() %></option>
								<%
									}
								%>
								</select>
							</div>
							<div class="form-group">
								<label for="Descripcion" class="col-form-label">Descripcion</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="4" id="Descripcion" name="Descripcion" maxlength="255" placeholder="Ingrese una breve descripcion de la actividad" required></textarea>
							</div>
							<div class="form-group">
								<label class="control-label" for="Cantidad">Gasto</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">C$</div>
									</div>
									<input type="tel" class="form-control dineromask" id="Cantidad" name="Cantidad" placeholder="Ingresar cantidad gastada">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Agregar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>
		
		
		
		<!-- Modal Editar Gasto -->
		<div id="modalEditarGasto" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Agregar un Gasto</h4>
					</div>
					<form id="formEditarGasto" class="form formEditarGasto" method="POST" action="${pageContext.request.contextPath}/SLeditarGasto">
						<div class="modal-body">
							<!-- Este de aqui sirve para mandar el atributo T_Gasto_ID, es la llave primaria del gasto que se esta editando... debe estar hidden.. aun no tiene value porque se le asignara con javascript -->
							<input type="text" name="T_Gasto_ID" id="T_Gasto_ID_MODIFICAR_GASTO" hidden="true">
							
							<!-- Este de aqui sirve para mandar el atributo T_Actividad_ID de la actividad que se esta editando actualmente... debe estar hidden -->
							<input type="text" name="T_Actividad_ID" id="T_Actividad_ID_MODIFICAR_GASTO" value="<%=idActividad%>" hidden="true">
							
							<div class="form-group">
								<label for="T_TipoGasto_ID" class="col-form-label">Tipo de Gasto:</label>
								<select class="form-control selectpicker" data-container="body" data-live-search="true" data-live-search-normalize="true" data-size="5" id="T_TipoGasto_ID_MODIFICAR_GASTO" name="T_TipoGasto_ID">
								<%
									for(TipoGasto tg: listaGast)
									{
								%>
									<option value="<%= tg.getT_TipoGasto_ID() %>"><%= tg.getNombre() %></option>
								<%
									}
								%>
								</select>
							</div>
							<div class="form-group">
								<label for="Descripcion" class="col-form-label">Descripcion</label>
								<label class="d-inline-block float-right text-secondary"><span></span></label>
								<textarea class="form-control resize_disabled" rows="4" id="Descripcion_MODIFICAR_GASTO" name="Descripcion" maxlength="255" placeholder="Ingrese una breve descripcion de la actividad" required></textarea>
							</div>
							<div class="form-group">
								<label class="control-label" for="Cantidad">Gasto</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<div class="input-group-text">C$</div>
									</div>
									<input type="tel" class="form-control dineromask" id="Cantidad_MODIFICAR_GASTO" name="Cantidad" placeholder="Ingresar cantidad gastada">
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-6"><button type="submit" class="btn btn-block btn-success">Editar</button></div>
							<div class="col-6"><button data-dismiss="modal" class="btn btn-block btn-danger">Cancelar</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>


















    </div>
    <%@include file="../../footer.jsp"%>

    
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
  <script type="text/javascript" src="../../js/jquery-ui-timepicker-addon.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
  
  <script type="text/javascript" src="../../js/inputmask.js"></script>
  <script type="text/javascript" src="../../js/inputmask.extensions.js"></script>
  <script type="text/javascript" src="../../js/inputmask.numeric.extensions.js"></script>
  <script type="text/javascript" src="../../js/jquery.inputmask.js"></script>
  
  <script type="text/javascript" src="../../js/bootstrap-select.js"></script>
  <script type="text/javascript" src="../../js/defaults-es_ES.js"></script>
  <!-- Page level plugin JavaScript-->
    <script>
		$(document).ready(function(){
			$('.fechaMask').mask('00-00-0000');
			$('.dineromask').inputmask({alias: 'currency', 
			    allowMinus: false,  
			    digits: 2,
			    prefix: "",
			    showMaskOnFocus: true});
			$('.selectpicker').selectpicker();
			if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				$('.selectpicker').selectpicker('mobile');
				$('.fechaMask').prop('readonly', true);
			}
		});
		
		
		//Para cambiar el idioma a los selectores de fecha
		$.datepicker.regional['es'] = {
			closeText: 'Cerrar',
			prevText: '< Ant',
			nextText: 'Sig >',
			currentText: 'Hoy',
			monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
			monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
			dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
			dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
			dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
			weekHeader: 'Sm',
			dateFormat: 'dd/mm/yy',
			firstDay: 1,
			isRTL: false,
			showMonthAfterYear: false,
			yearSuffix: ''
		};
		$.datepicker.setDefaults($.datepicker.regional['es']);
		$('#Fecha').datepicker({
			setDate : new Date(),
			dateFormat : 'dd-mm-yy',
			changeMonth: true,
			changeYear : true
		});
		
		var inputs = "textarea[maxlength]";
		$(document).on('keyup', "[maxlength]", function(e) {
			var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
			espan = este.prev('label').find('span');
			// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
			if (document.addEventListener
					&& !window.requestAnimationFrame) {
				if (remainingCharacters <= -1) {
					remainingCharacters = 0;
				}
			}
			espan.html(currentCharacters + "/" + maxlength);
		});
		
		//Esto es para cargar los datos del gasto en el modal que se seleccione...
		$('#modalEditarGasto').on('show.bs.modal', function (event) {
			var button = $(event.relatedTarget)
			var gastoid = button.data('gastoid')
			var gastotipo = button.data('gastotipo')
			var gastodescripcion = button.data('gastodescripcion')
			var gastocantidad = button.data('gastocantidad')
			var modal = $(this)
			modal.find('#T_Gasto_ID_MODIFICAR_GASTO').attr('value', gastoid);
			$('#T_TipoGasto_ID_MODIFICAR_GASTO').selectpicker('val', gastotipo);	//Este es distinto por ser un selectpicker (El select que te permite buscar)
			modal.find('#Descripcion_MODIFICAR_GASTO').text(gastodescripcion);		//Este es distinto por ser un textarea... el valor va entre los tags... no en el tag value
			modal.find('#Cantidad_MODIFICAR_GASTO').val(gastocantidad);
	   });
		
		//Esto es para resetear el formulario cuando se cierre el modal... antes si se escribia algo se cerraba y se volvia a abrir con otro... los datos seguian del anterior...
		$('#modalEditarGasto').on('hide.bs.modal', function () {
			$('#formEditarGasto').trigger("reset");  //Aqui poner el id del formulario de editar
		});
	</script>
    <!-- Bootstrap core JavaScript-->
    
    <script src="../../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="../../js/tablesLanguage.js"></script>
    <script src="../../js/jquery.mask.js"></script>
    
    
  </div>
</body>

</html>