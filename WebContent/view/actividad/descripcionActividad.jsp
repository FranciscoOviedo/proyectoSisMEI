<%@page import="vistas.V_Info_ActividadProyecto"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTActividadProyecto"%>

<%@page import="java.text.DateFormat"%>
<%@page import= "java.text.SimpleDateFormat"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-1.10.4.custom.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/jquery-ui-timepicker-addon.css" />
  <link rel="stylesheet" media="all" type="text/css" href="../../css/bootstrap-select.css" />
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaActividad.jsp">Actividad</a>
        </li>
        <li class="breadcrumb-item active">Detalle Actividad</li>
      </ol>
      
      <h1>Detalle Actividad</h1>
      <hr class="mt-2">
      
      <div class="item_container">
          <%
			ArrayList<V_Info_ActividadProyecto> listaActividades= (ArrayList<V_Info_ActividadProyecto>) session.getAttribute("ListaActividades");
			String idRequestActividad = request.getParameter("id");
			V_Info_ActividadProyecto a = null;
			int idActividad = 0;
			try {
				idActividad = Integer.parseInt(idRequestActividad);
			} catch (Exception e) {
				idActividad = 0;
				e.printStackTrace();
				e.getMessage();
			}
			for (V_Info_ActividadProyecto tempA : listaActividades) {
				if (tempA.getT_Actividad_ID() == idActividad) {
					a = tempA;
					break;
				}
			}
			DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		  %>
          <!-- div class="card-body row">
          
          	<div class="col-sm-6">
                <p class="font-weight-bold">Micro Proyecto: </p>
                <% if(a.getNombreMicroProyecto() != null){ %>
                <p class="text-muted"><%=a.getNombreMicroProyecto()%></p>
                <%}else{%>
                <p class="text-muted">No pertenece a un micro proyecto en especifico</p>
                <%}%>
            </div>
            <div class="col-sm-6">
                <p class="font-weight-bold">Formador: </p>
                <p class="text-muted"><%=a.getNombreFormador()%></p>
            </div>
            <div class="col-sm-12">
                <p class="font-weight-bold">Nombre: </p>
                <p class="text-muted"><%=a.getNombre()%></p>
            </div>
            
            <div class="col-sm-6">
            	<p class="font-weight-bold">Estimado: </p>
            	<p class="text-muted">C$ <%=a.getEstimado()%></p>
            </div>
            
            <div class="col-sm-6">
              	<p class="font-weight-bold">Fecha de Realización: </p>
             	<p class="text-muted"><% out.print(format.format(a.getFecha())); %></p> 
            </div>
            
            <div class="col-sm-12">
            	<p class="font-weight-bold">Descripción: </p>
             	<p class="text-muted"><%=a.getDescripcion()%></p>
            </div>
		  </div>
		  <div class="card-footer text-muted">
			2 days ago
		  </div -->
        
        
          <div class="card-body font-weight-bold text-truncate lead">
            <%=a.getNombre()%>
          </div>
          <hr class="my-0">
          <div class="card-body">
            <p class="text-muted"><% if(a.getNombreMicroProyecto() != null){
                	out.print("Pertenece al micro proyecto <strong class='text-nowrap'>" + a.getNombreMicroProyecto() + "</strong>");
                }else{
                	out.print("No pertenece a un micro proyecto en especifico");
                }
                out.print(". El "+format.format(a.getFecha())); %></p>
            
           	<p><%=a.getDescripcion()%></p>
            
		  </div>
		  <div class="card-footer text-muted row">
			<div class="col-6 col-sm-8 text-muted"><% out.print("Ha cargo de <strong class='text-nowrap'>" + a.getNombreFormador() + " " + a.getApellidoFormador() + "</strong>"); %> </div>
            <div class="col-6 col-sm-4 text-right text-success"><% out.print("C$" + String.format("%,.2f", a.getEstimado())); %> </div>
		  </div>
      </div>
      
      















    </div>
    <%@include file="../../footer.jsp"%>

    
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  
    <!-- Custom scripts for all pages-->
    <script src="../../js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    
    
  </div>
</body>

</html>