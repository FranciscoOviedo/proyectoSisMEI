<%@page import="entidades.Categoria"%>
<%@page import="java.util.ArrayList"%>
<%@page import="datos.DTCategoria"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SisMEI</title>
  <link rel="icon" href="../../img/LogoSisMEI-low-res.png">
  <!-- Bootstrap core CSS-->
  <link href="../../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="../../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">
  <link href="../../css/content.css" rel="stylesheet">
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
  <!--link rel="stylesheet" media="all" type="text/css" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/-->
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top" link="black" vlink="orange" alink="black"  >
  <!-- Navigation-->
  <%@include file="../../navbar.jsp" %>
  <div class="content-wrapper">
    <div class="container-fluid">

















      




      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="../../index.jsp">Dashboard</a>
        </li>
        <li class="breadcrumb-item">
            <a href="listaCategoriaCICC.jsp">Categoria CI - CC</a>
        </li>
        <li class="breadcrumb-item active">Crear Categoria</li>
      </ol>
      
      <h1>Crear Categoria</h1>
      <hr class="mt-2">
      
      <div class="mx-auto col-sm-10">
      
	      <div class="item_container card-body">
	        <form id="formFormador" class="form" method="POST" action="${pageContext.request.contextPath}/SLeditarCategoria">
	        	<%
					ArrayList<Categoria> listaCategoria = (ArrayList<Categoria>) session.getAttribute("ListaCategorias");
					String idRequestCategoria = request.getParameter("id");
					Categoria c = null;
					int idCategoria = 0;
					try {
						idCategoria = Integer.parseInt(idRequestCategoria);
					} catch (Exception e) {
						idCategoria = 0;
						e.printStackTrace();
						e.getMessage();
					}
					for (Categoria tempC : listaCategoria) {
						if (tempC.getT_Categoria_ID() == idCategoria) {
							c = tempC;
							break;
						}
					}
				%>
	          <div class="row">
	          	<input type="text" name="T_Categoria_ID" id="T_Categoria_ID" value="<%=c.getT_Categoria_ID()%>" hidden="true">
	            <div class="form-group col-12">
	                <label class="control-label" for="Nombre">Nombre:</label>
	                <input type="text" class="form-control" id="Nombre" name="Nombre" value="<%=c.getNombre()%>" placeholder="Ingrese el nombre del tipo de gasto" required>
	            </div>
	            
	            <div class="form-group col-sm-12">
					<label class="control-label " for="Objetivo">Objetivo:</label>
					<label class="d-inline-block float-right text-secondary"><span></span></label>
					<textarea class="form-control resize_disabled" rows="4" id="Objetivo" name="Objetivo" maxlength="255" placeholder="Ingrese el objetivo de la categoria" required><%=c.getObjetivo()%></textarea>
	            </div>
	            
			  </div>
			  <div class="form-group row">
	              <div class="col-6"><button type="submit" class="btn btn-block btn-success">Editar</button></div>
	              <div class="col-6"><button type="reset" class="btn btn-block btn-danger">Cancelar</button></div>
	          </div>
	        </form>
	      </div>
      </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
    </div>
    <%@include file="../../footer.jsp"%>
    
	<script src="../../vendor/jquery/jquery.min.js"></script>
	<script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../../js/sb-admin.min.js"></script>
    <script>
	var inputs = "textarea[maxlength]";
	$(document).on('keyup', "[maxlength]", function(e) {
		var este = $(this), maxlength = este.attr('maxlength'), maxlengthint = parseInt(maxlength), currentCharacters = este.val().length;
		espan = este.prev('label').find('span');
		// Detectamos si es IE9 y si hemos llegado al final, convertir el -1 en 0 - bug ie9 porq. no coge directamente el atributo 'maxlength' de HTML5
		if (document.addEventListener
			&& !window.requestAnimationFrame) {
		if (remainingCharacters <= -1) {
			remainingCharacters = 0;
		}
	}
	espan.html(currentCharacters + "/" + maxlength);
	 });
    </script>
  </div>
